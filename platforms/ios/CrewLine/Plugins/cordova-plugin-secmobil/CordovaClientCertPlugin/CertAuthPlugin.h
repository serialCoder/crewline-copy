#import <Cordova/CDV.h>

@interface CertAuthPlugin : CDVPlugin

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *strongPassword;
@property (strong, nonatomic) NSString *duration;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *method;
@property (strong, nonatomic) NSString *requestData;
@property (assign, nonatomic) Boolean isAuthCommand;
@property (strong, nonatomic) NSDictionary *headersDictionary;

@property (strong, nonatomic) NSMutableDictionary * habileCredentialsFromCertificateDictionary;
@property(nonatomic, strong) NSString * jwtTokenString;
@property (strong, nonatomic) NSString *applicationId;
@property (strong, nonatomic) NSString *urlToken;


/*!
 * Mauvais pwd d'authentification pour récupération du certificat
 */
extern NSString *const CERTIFICATE_LOADED;

/*!
 * Mauvais pwd d'authentification pour récupération du certificat
 */
extern NSString *const BAD_PASSWORD;

/*!
 * Mauvais pwd d'authentification pour récupération du certificat
 */
extern NSString *const EXPIRED_CERTIFICATE;

/*!
 * Mauvais pwd d'authentification pour récupération du certificat
 */
extern NSString *const UNKNOWN_ERROR;

- (void) setAppGroup:(CDVInvokedUrlCommand*)command;

/*!
 *  Point entrée plugin secmobil
 *  \param command est un objet json qui doit contenir les paramètres suivants :
 *
 *  Pour récupérer un certificat :
 *      - userId :          matricule de l'utilisateur qui veut récupérer son certificat
 *      - strongPassword :  mot de passe "fort" (PIN + token)
 *      - duration :        durée de validité du certificat : "long" ou "short"
 */
- (void) getCertificate:(CDVInvokedUrlCommand*)command;

- (void) getJWTToken:(CDVInvokedUrlCommand*)command;

- (void) revokeCertificate:(CDVInvokedUrlCommand*)command;

- (void) hasCertificate:(CDVInvokedUrlCommand*)command;
/*!
 *  Point entrée plugin secmobil
 *  \param command est un objet json qui doit contenir les paramètres suivants :
 *
 *  Pour faire un appel REST :
 *      - url :             URL de la requête (sous la forme "http://monurl.airfrance.fr")
 *      - method :          méthode HTTP : "GET", "POST" ou "PUT"
 */
- (void) callRestService:(CDVInvokedUrlCommand*)command;

/*!
 * Initialisation du plugin : définit l'environnement d'exécution
 * \param env a 3 valeurs possibles : DEV, RCT ou PRD. Si valeur incorrecte alors PRD est positionnée,
 */
- (void) initPlugin:(CDVInvokedUrlCommand*)env;

@end
