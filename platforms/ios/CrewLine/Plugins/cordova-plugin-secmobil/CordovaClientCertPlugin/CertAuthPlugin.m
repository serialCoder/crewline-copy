#import "CertAuthPlugin.h"
#import <SecMobile-iOS/SecMobile.h>
#import <CommonCrypto/CommonCrypto.h>

@implementation CertAuthPlugin

/* RETURN CODES */
NSString *const CERTIFICATE_LOADED = @"secmobil.certificate.loaded";
NSString *const BAD_PASSWORD = @"secmobil.incorrect.credentials";
NSString *const EXPIRED_CERTIFICATE = @"secmobil.certificate.expired";
NSString *const UNKNOWN_ERROR = @"secmobil.unknown.error";


- (void)pluginInitialize {
    // Secmobil default init (PRD)
    [[AFIdentityManager sharedInstance] setSecMobileEnv:AF_ENV_PRD];
}

- (void) setAppGroup:(CDVInvokedUrlCommand*)command {

  NSString* company = [command.arguments objectAtIndex:0];
  SecMobileAppGroup appGroup;
  if ([company caseInsensitiveCompare: @"AF_GROUP"] == NSOrderedSame)
  {
      appGroup = AF_GROUP;
  } else if ([company caseInsensitiveCompare: @"KL_GROUP"] == NSOrderedSame)
  {
      appGroup = KL_GROUP;
  }

  [[AFIdentityManager sharedInstance] setSecMobileAppGroup:appGroup];
}

- (void)initPlugin:(CDVInvokedUrlCommand*)env {
    NSString* environment = [env.arguments objectAtIndex:0];

    AFSecMobileEnv secmobilEnv = AF_ENV_PRD;

    if ([environment caseInsensitiveCompare: @"dev"] == NSOrderedSame)
    {
        secmobilEnv = AF_ENV_DEV;
    } else if ([environment caseInsensitiveCompare: @"rct"] == NSOrderedSame)
    {
        secmobilEnv = AF_ENV_RCT;
    } else if ([environment caseInsensitiveCompare: @"prd"] == NSOrderedSame)
    {
        secmobilEnv = AF_ENV_PRD;
    }

    [[AFIdentityManager sharedInstance] setSecMobileEnv:secmobilEnv];
}


- (void) getCertificate:(CDVInvokedUrlCommand*)command {

    [self.commandDelegate runInBackground:^{
        [self extractUIParams:command];

        NSError * error = nil;
        [[AFIdentityManager sharedInstance] removeIdentity:&error];

        [self loadClientCert:command];
    }];
}

- (void) revokeCertificate:(CDVInvokedUrlCommand*)command {

    CDVPluginResult* result = nil;
    NSError * error = nil;

    [[AFIdentityManager sharedInstance] removeIdentity:&error];

    if(error) {
        NSLog(@"SecMobil certificate revoked successfully");
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:error.description];
    } else {
        NSLog(@"SecMobil certificate failed to delete");
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"secmobil.certificate.removed"];
    }

    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) hasCertificate:(CDVInvokedUrlCommand*)command {

    bool certLoaded = [[AFIdentityManager sharedInstance] isIdentityPresent];
    __block CDVPluginResult* result = nil;

    if (certLoaded) {

        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"secmobil.certificate.present"];
    } else {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"secmobil.certificate.nocertificate"];
    }


    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

// Log credentials from local certificate which is fetched with SecMobil
- (void) checkValidityOfCertificate:(CDVInvokedUrlCommand*)command {
    bool certLoaded = [[AFIdentityManager sharedInstance] isIdentityPresent];
    __block CDVPluginResult* result = nil;

    if(certLoaded){
        NSLog(@"Certificate = %@", @"Identity found");
    } else {
        NSLog(@"Certificate = %@", @"Identity not found");
    }

    NSError *error = nil;
    NSString *label = [[AFIdentityManager sharedInstance] secIdentityKeyChainAuthLabel];

    #if TARGET_IPHONE_SIMULATOR
        SecIdentityRef identity = [AFKeychain getIdentity:&error accessGroup:nil label:label];
    #else
        SecIdentityRef identity = [AFKeychain getIdentity:&error accessGroup:kAFSecAccessGroup label:label];
    #endif

    if (identity) {
        SecCertificateRef certi;
        SecIdentityCopyCertificate(identity, &certi);
        NSData *dataCerti = (__bridge_transfer NSData *) SecCertificateCopyData(certi);

        AFCertificate *certificate = [[AFCertificate alloc] initWithData:dataCerti];
        NSLog(@"Certificate = %@", certificate);
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"secmobil.certificate.present"];
    } else {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"secmobil.certificate.nocertificate"];
    }

    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}


// Authentication and loading of client certificat
-(void)loadClientCert:(CDVInvokedUrlCommand*)authParams {

    NSLog(@"user %@ asks for a certificate", self.userId);

    AFSecMobileDuration certDuration = [self.duration caseInsensitiveCompare: @"LONG"] == NSOrderedSame ? AF_LONG : AF_SHORT;
    [[AFIdentityManager sharedInstance] setSecMobileDuration: certDuration];

    __block CDVPluginResult* result = nil;

    [[AFIdentityManager sharedInstance] getIdentityWithUser:self.userId password: self.strongPassword completion:^(SecIdentityRef identityRef, NSError *error) {

            if (error) {
                NSString *errorMessage = nil;

                if ([[error localizedDescription] isEqualToString:@"cancelled"]) {
                    errorMessage = @"secmobil.incorrect.credentials";
                } else {
                    errorMessage = [NSString stringWithFormat:@"secmobil.unknown.error : %@",[error localizedDescription]];
                }

                NSLog(@"Certificate load fail : %@", errorMessage);
                result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errorMessage];

            } else {
                NSLog(@"Certificate loaded");

                result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"secmobil.certificate.loaded"];
            }

            [self.commandDelegate sendPluginResult:result callbackId:authParams.callbackId];
        }
     ];

}
- (void) callRestService:(CDVInvokedUrlCommand*)command {

    [self extractUIParams:command];

    bool certLoaded = [[AFIdentityManager sharedInstance] isIdentityPresent];

    __block CDVPluginResult* result = nil;
    if (certLoaded) {
        [self sendHttpRequest:command];
    } else
    {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"secmobil.nocertificate"];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
}


/* extraction des paramètres JS utilisateurs du plugins */
-(void) extractUIParams:(CDVInvokedUrlCommand*)uiParams
{
    NSDictionary* jsonParams = [[uiParams arguments] objectAtIndex:0];

    self.userId = [jsonParams objectForKey:@"userId"];
    self.strongPassword = [jsonParams objectForKey:@"strongPassword"];
    self.duration = [jsonParams objectForKey:@"duration"];
    self.url = [jsonParams objectForKey:@"url"];
    self.method = [jsonParams objectForKey:@"method"];
    self.requestData = [jsonParams objectForKey:@"jsonData"];
    self.headersDictionary = [jsonParams objectForKey:@"httpHeaders"];

    self.isAuthCommand = self.userId != nil && self.strongPassword != nil;
}


/* HTTP request */
- (void) sendHttpRequest:(CDVInvokedUrlCommand*)httpParams
{

    //[self.commandDelegate runInBackground:^{

    //AFCertificateURLSessionDelegate *urlDelegate = [[AFCertificateURLSessionDelegate alloc] init];


    NSURL *url = [NSURL URLWithString:self.url];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    [request setHTTPMethod:self.method];
    [request setHTTPBody:[self.requestData dataUsingEncoding:NSUTF8StringEncoding]];

    if (self.headersDictionary) {
        [request setAllHTTPHeaderFields:self.headersDictionary];
    }

    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];

    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self
                                                                 delegateQueue: [NSOperationQueue mainQueue]];


    __block CDVPluginResult* result = nil;


    NSURLSessionDataTask *dataTask = [delegateFreeSession dataTaskWithRequest:request
                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {



                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;

                                                                if(error) // choix arbitraire : on decide que tous les code au dela de 309 sont des erreurs !
                                                                {
                                                                    NSString * errorMessage = [NSString stringWithFormat:@"%@ (%ld) : %@",
                                                                      UNKNOWN_ERROR,
                                                                      [error code],
                                                                      [error localizedDescription]];

                                                                    NSLog(@"Error %@", errorMessage);

                                                                    if ([[[ error userInfo ] valueForKey: @"_kCFStreamErrorCodeKey"] isEqualToNumber: [NSNumber numberWithInt: -9828 ]])
                                                                    {
                                                                        errorMessage = EXPIRED_CERTIFICATE;
                                                                    }

                                                                    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errorMessage];

                                                                } else if (httpResponse.statusCode > 309) // choix arbitraire : on decide que tous les code au dela de 309 sont des erreurs !
                                                                {

                                                                    NSString *responseError = @"";

                                                                    if (data)
                                                                    {
                                                                      responseError= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                                    }

                                                                    NSString * errorMessage = [NSString stringWithFormat:@"HTTP Error %ld %@ : %@",
                                                                      (long)[httpResponse statusCode],
                                                                      [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode],
                                                                      responseError];

                                                                    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errorMessage];
                                                                    NSLog(@"Error %@", errorMessage);

                                                                } else
                                                                {
                                                                    NSString * contentType = [httpResponse.allHeaderFields objectForKey:@"Content-Type"];
                                                                    if ([contentType containsString: @"json"] || [contentType containsString: @"text"]) {
                                                                        NSString *resultString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                                        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:resultString];
                                                                    } else {
                                                                        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArrayBuffer:data];
                                                                    }
                                                                }

                                                                [self.commandDelegate sendPluginResult:result callbackId:httpParams.callbackId];

                                                            }];

    [dataTask resume];
    [delegateFreeSession finishTasksAndInvalidate];
}


-(void) getJWTToken:(CDVInvokedUrlCommand*)command  {

    SecIdentityRef certLoaded = [[AFIdentityManager sharedInstance] getIdentity];
    __block CDVPluginResult* result = nil;

    NSDictionary* jsonParams = [[command arguments] objectAtIndex:0];

    if (![jsonParams isEqual: [NSNull null]])
    {
        self.applicationId = [jsonParams objectForKey:@"clientAppId"];
        self.urlToken = [jsonParams objectForKey:@"urlToken"];
        if (self.urlToken == nil) {
            self.urlToken = @"https://www.klm.com/generic/oauth-b2e";
        }
    } else {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"parameter Client appId and urlToken are missing"];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        return ;
    }

    if (certLoaded != nil) {
        [self initJwtToken];
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:self.jwtTokenString];
    } else {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"secmobil.certificate.nocertificate"];
    }
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

#pragma mark NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler {


    if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
        OSStatus                err;
        NSURLProtectionSpace *  protectionSpace = [challenge protectionSpace];
        SecTrustRef             trust = [protectionSpace serverTrust];
        BOOL                    trusted;
        SecTrustResultType      trustResult;

        assert(protectionSpace != nil);
        assert(trust != NULL);

        // Evaluate the trust the standard way.

        err = SecTrustEvaluate(trust, &trustResult);
        trusted = (err == noErr) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));

        // If the standard policy says that it's trusted, allow it right now.
        // Otherwise do our custom magic.

        if (trusted) {

            NSURLCredential *credential = [NSURLCredential credentialForTrust:trust];
            completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
        }
    }


    if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodClientCertificate) {
        NSError *error = nil;

        SecIdentityRef identity = [AFKeychain getIdentity:&error label:[[AFIdentityManager sharedInstance] secIdentityKeyChainAuthLabel]];
        if (identity) {
            SecCertificateRef certificate;
            SecIdentityCopyCertificate(identity, &certificate);

            SecCertificateRef certArray[1] = { certificate };
            CFArrayRef myCerts = CFArrayCreate(NULL, (void *)certArray, 1, NULL);
            CFRelease(certificate);

            NSURLCredential *credential = [NSURLCredential credentialWithIdentity:identity certificates:(__bridge NSArray *)myCerts persistence:NSURLCredentialPersistenceNone];
            completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
        }

    }

}


#pragma mark Setting up a JWT Token

-(void) initJwtToken {

    // Check whether local certificate exists
    NSError *error = nil;
    NSString *label = [[AFIdentityManager sharedInstance] secIdentityKeyChainAuthLabel];
    SecIdentityRef identity = [AFKeychain getIdentity:&error accessGroup:kAFSecAccessGroup label:label];

    if (identity) {
        //fetch local certificate information as AFCertificate Object
        SecCertificateRef certi;
        SecIdentityCopyCertificate(identity, &certi);
        NSData *dataCerti = (__bridge_transfer NSData *) SecCertificateCopyData(certi);

        AFCertificate *certificate = [[AFCertificate alloc] initWithData:dataCerti];
        [self innerInitJWTWithSettings:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:certificate.subject.commonName,dataCerti, nil] forKeys:[NSArray arrayWithObjects:@"sub",@"x5c", nil]]];

    } else {
    }
}

-(id) innerInitJWTWithSettings:(NSDictionary *)userCredentials {
    if(self){
        //Create dictionary with Habile Certificate Credentials
        self.habileCredentialsFromCertificateDictionary = [[NSMutableDictionary alloc] initWithDictionary: userCredentials];
        //create a JSON Web Token
        [self encodeHabileJWTToken:self.habileCredentialsFromCertificateDictionary];
    }
    return self;
}

// create Habile Token
-(void) encodeHabileJWTToken:(NSDictionary *)habileDictionary {

    //create Jose Header with type, alg and x5c (FIRST PART OF BODY OF REQUEST)
    //BASE64 URL Encode the X5C Header from the Habile Certificate
    NSMutableDictionary * joseHeaderDictionary = [[NSMutableDictionary alloc] init];
    [joseHeaderDictionary setObject:[NSArray arrayWithObjects:[self base64URLSaveEncodeString:[self.habileCredentialsFromCertificateDictionary objectForKey:@"x5c"]], nil] forKey:@"x5c"];
    [joseHeaderDictionary setObject:@"JWT" forKey:@"type"];
    [joseHeaderDictionary setObject:@"RS256" forKey:@"alg"];

    //Create payload/claims dictionary (SECOND PART OF BODY OF REQUEST)
    NSDictionary * payLoadDictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:
                                                                            self.applicationId, // appID to change with params
                                                                            [self.habileCredentialsFromCertificateDictionary objectForKey:@"sub"],
                                                                            self.urlToken,
                                                                            [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970] + (500 * 60)],
                                                                            [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],
                                                                            @"INFLIGHT",
                                                                            nil]
                                                                   forKeys:[NSArray arrayWithObjects:
                                                                            @"iss",
                                                                            @"sub",
                                                                            @"aud",
                                                                            @"exp",
                                                                            @"iat",
                                                                            @"rolePatterns",
                                                                            nil]];

    //Final data object for JSON WEBTOKEN CLAIMS
    //1. deserialize dictionary
    //2. base64urlsave encode the string object
    //3. combine both JWT Header and JWT claims with '.' seperated
    NSString *headerAndPayloadAsCombinedString = [NSString stringWithFormat:@"%@.%@", [self base64URLSaveEncodeString:[self deserializeJSONObjectToDataObject:joseHeaderDictionary]], [self base64URLSaveEncodeString:[self deserializeJSONObjectToDataObject:payLoadDictionary]]];

    //Set Data Object from combined (JWT HEADER & Claims) JSON WEB TOKEN
    NSData *data = [headerAndPayloadAsCombinedString dataUsingEncoding:NSUTF8StringEncoding];
    data = [self signJWTWithPrivateKey:data];
    NSString * base64EncodedJsonDataSignedWithPrivateKey = [self base64URLSaveEncodeString:data];

    //Create final JSON Webtoken
    self.jwtTokenString = [NSString stringWithFormat:@"%@.%@", headerAndPayloadAsCombinedString,base64EncodedJsonDataSignedWithPrivateKey];
}

// sign NSData object with private key
- (NSData *)signJWTWithPrivateKey:(NSData *)data {

    SecKeyRef privateKey = self.privateKeyRef;
    size_t signedHashBytesSize = SecKeyGetBlockSize(privateKey);
    uint8_t* signedHashBytes = malloc(signedHashBytesSize);
    memset(signedHashBytes, 0x0, signedHashBytesSize);

    size_t hashBytesSize = CC_SHA256_DIGEST_LENGTH;
    uint8_t* hashBytes = malloc(hashBytesSize);
    if (!CC_SHA256([data bytes], (uint32_t)[data length], hashBytes)) {
        if (hashBytes)
            free(hashBytes);

        if (signedHashBytes)
            free(signedHashBytes);

        return nil;
    }

    SecKeyRawSign(privateKey,
                  kSecPaddingPKCS1SHA256,
                  hashBytes,
                  hashBytesSize,
                  signedHashBytes,
                  &signedHashBytesSize);

    NSData* signedHash = [NSData dataWithBytes:signedHashBytes length:(NSUInteger)signedHashBytesSize];

    if (hashBytes)
        free(hashBytes);

    if (signedHashBytes)
        free(signedHashBytes);

    return signedHash;
}

// Fetch private key
- (NSData *)privateKey {
    SecKeyRef privateKey = self.privateKeyRef;

    if (privateKey) {
        size_t keySize = SecKeyGetBlockSize(privateKey);
        return [NSData dataWithBytes:privateKey length:keySize];
    } else {
        return nil;
    }
}

// Fetch private key ref.
- (SecKeyRef)privateKeyRef {
    SecKeyRef privateKey;
    NSString *label = [[AFIdentityManager sharedInstance] secIdentityKeyChainAuthLabel];
    NSError *error = nil;
    SecIdentityRef identity = [AFKeychain getIdentity:&error accessGroup:kAFSecAccessGroup label:label];
    SecIdentityCopyPrivateKey(identity, &privateKey);
    CFRelease(identity);

    if (privateKey) {
        return privateKey;
    } else {
        return nil;
    }
}

// Deserialize nsdictionary to nsdata object
-(NSData *)deserializeJSONObjectToDataObject:(NSDictionary *)inputDictionary {
    NSError *error;
    NSData * deserializedJSONData = [NSJSONSerialization dataWithJSONObject:inputDictionary
                                                                    options:0
                                                                      error:&error];
    if (deserializedJSONData) {
        NSString *JSONString = [[NSString alloc] initWithBytes:[deserializedJSONData bytes] length:[deserializedJSONData length] encoding:NSUTF8StringEncoding];
        NSLog(@"BASE 64 URL SAVE ENCODED STRING: %@", JSONString);
    }
    return deserializedJSONData;
}

// Perform Base 64 URL Save Encoding
-(NSString *)base64URLSaveEncodeString:(NSData *)rawValue {
    NSString * base64URLSaveEncodedString = [rawValue base64EncodedStringWithOptions:0];
    base64URLSaveEncodedString = [base64URLSaveEncodedString stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    base64URLSaveEncodedString = [base64URLSaveEncodedString stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    base64URLSaveEncodedString = [base64URLSaveEncodedString stringByReplacingOccurrencesOfString:@"=" withString:@""];
    return base64URLSaveEncodedString;
}




@end
