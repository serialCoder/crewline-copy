//
//  NSError+AFError.h
//  SecMobile
//
//  Created by Laurent Gaches on 09/09/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (AFError)

/**
 Transform OSStatus for Security Framework in NSError
 
 @param status The status to translate.
 @return NSError corresponding.
 */
+ (NSError *)errorWithOSStatus:(OSStatus)status;

@end
