//
//  AFSecConfiguration.h
//  SecMobile
//
//  Created by Laurent Gaches on 14/10/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFIdentityManager.h"

@interface AFSecConfiguration : NSObject

@property (strong, nonatomic) NSString *host;
@property (strong, nonatomic) NSString *protocol;
@property (strong, nonatomic) NSString *port;
@property (strong, nonatomic) NSString *path;
@property (strong, nonatomic) NSString *pathShort;
@property (readonly, nonatomic) NSString *urlString;
@property (readonly, nonatomic) NSString *authIdentityLabel;
@property (readonly, nonatomic) NSBundle *secMobileBundle;
@property (readonly, nonatomic) AFSecMobileDuration secMobileDuration;


- (instancetype)initWithEnv:(AFSecMobileEnv)env;
- (void)setDuration:(AFSecMobileDuration)duration;


@end
