//
//  AFASN1Parser.h
//  SecMobile
//
//  Created by Laurent Gaches on 14/10/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <Foundation/Foundation.h>


/*!
  @enum AFASN1Type enum
  @abstract ASN1 Type
 */
typedef NS_ENUM(NSUInteger, AFASN1Type) {
	/*!
	 ASN1 type for EOC
	 */
	AFASN1TypeEOC = 0x00,
	
	/*!
	 ASN1 type for Boolean
	 */
	AFASN1TypeBoolean = 0x01,
	
	/*!
	 ASN1 type for integer numbers
	 */
	AFASN1TypeInteger = 0x02,
	
	/*!
	 ASN1 type for Bit Strings
	 */
	AFASN1TypeBitString = 0x03,
	
	/*!
	 ASN1 type for Octet Strings
	 */
	AFASN1TypeOctetString = 0x04,
	
	/*!
	 ASN1 type for NULL values
	 */
	AFASN1TypeNull = 0x05,
	
	/*!
	 ASN1 type for object identifiers
	 */
    AFASN1TypeObjectIdentifier = 0x06,
	
	/*!
	 ASN1 type for object descriptors
	 */
	AFASN1TypeObjectDescriptor = 0x07,
	
	/*!
     ASN1 type for external references
	 */
	AFASN1TypeExternal = 0x08,
	
	/*!
     ASN1 type for floating point numbers
	 */
	AFASN1TypeReal= 0x09,
	
	/*!
     ASN1 type for enumerated values
	 */
	AFASN1TypeEnumerated = 0x0a,
	
	/*!
     ASN1 type for embedded PDV values
	 */
	AFASN1TypeEmbeddedPDV = 0x0b,
	
	/*!
     ASN1 type for UTF8 strings
	 */
	AFASN1TypeUTF8String = 0x0c,
	
	/*!
     ASN1 type for sequences
	 */
	AFASN1TypeSequence = 0x10,
	
	/*!
     ASN1 type for sets
	 */
	AFASN1TypeSet = 0x11,
	
	/*!
     ASN1 type for numeric strings
	 */
	AFASN1TypeNumericString = 0x12,
	
	/*!
     ASN1 type for printable strings
	 */
	AFASN1TypePrintableString = 0x13,
	
	/*!
     ASN1 type for teletex strings
	 */
	AFASN1TypeTeletexString = 0x14,
	
	/*!
     ASN1 type for video text strings
	 */
	AFASN1TypeVideoTextString = 0x15,
	
	/*!
     ASN1 type for IA5 strings
	 */
	AFASN1TypeIA5String = 0x16,
	
	/*!
     ASN1 type for UTC times
	 */
	AFASN1TypeUTCTime = 0x17,
	
	/**
	 AFASN1 type for generalized times
	 */
	AFASN1TypeGeneralizedTime = 0x18,
	
	/*!
     ASN1 type for generalized time values
	 */
	AFASN1TypeGraphicString = 0x19,
    
	/*!
     ASN1 type for visible strings
	 */
	AFASN1TypeVisibleString = 0x1a,
	
	/*!
     ASN1 type for general strings
	 */
	AFASN1TypeGeneralString = 0x1b,
	
	/*!
     ASN1 type for universal strings
	 */
	AFASN1TypeUniversalString = 0x1c,
	
	/*!
     ASN1 type for bitmap strings
	 */
	AFASN1TypeBitmapString = 0x1e,
	
	/*!
     ASN1 value to signify that the value uses the long form
	 */
	AFASN1TypeUsesLongForm = 0x1f
};

@class AFASN1Parser;
@class AFASN1BitString;

@protocol AFASN1ParserDelegate <NSObject>

@optional

/**
 Sent by the parser object to the delegate when it begins parsing a document.
 
 @param parser A parser object.
 */
- (void)parserDidStartDocument:(AFASN1Parser *)parser;

/**
 Sent by the parser object to the delegate when it has successfully completed parsing
 
 @param parser A parser object.
 */
- (void)parserDidEndDocument:(AFASN1Parser *)parser;

/**
 Sent by a parser object to its delegate when it encounters the beginning of a constructed element.
 
 @param parser A parser object.
 @param type The tag type that contains the subsequent elements.
 */
- (void)parser:(AFASN1Parser *)parser didStartContainerWithType:(AFASN1Type)type;

/**
 Sent by a parser object to its delegate when it encounters the end of a constructed element.
 
 @param parser A parser object.
 @param type A string that is the name of an element (in its end tag).
 */
- (void)parser:(AFASN1Parser *)parser didEndContainerWithType:(AFASN1Type)type;

/**
 Sent by a parser object to its delegate when it encounters the beginning of a context-specific tag.
 
 @param parser A parser object.
 @param tag The tag value for the context that contains the subsequent elements.
 */
- (void)parser:(AFASN1Parser *)parser didStartContextWithTag:(NSUInteger)tag;

/**
 Sent by a parser object to its delegate when it encounters the end of a constructed element.
 
 @param parser A parser object.
 @param tag The tag value for the context that contained the previous elements.
 */
- (void)parser:(AFASN1Parser *)parser didEndContextWithTag:(NSUInteger)tag;

/**
 Sent by a parser object to its delegate when it encounters a fatal error.
 
 When this method is invoked, parsing is stopped. For further information about the error, you can query parseError or you can send the parser a parserError message. You can also send the parser lineNumber and columnNumber messages to further isolate where the error occurred. Typically you implement this method to display information about the error to the user.
 
 @param parser A parser object.
 @param parseError An `NSError` object describing the parsing error that occurred.
 */
- (void)parser:(AFASN1Parser *)parser parseErrorOccurred:(NSError *)parseError;

/**
 Sent by a parser object when a NULL element is encountered.
 
 @param parser A parser object.
 */
- (void)parserFoundNull:(AFASN1Parser *)parser;

/**
 Sent by a parser object to provide its delegate with the date encoded in the current element.
 
 All the ASN1 date types are provided via this method.
 
 @param parser A parser object.
 @param date A date representing the date encoded in the current element.
 */
- (void)parser:(AFASN1Parser *)parser foundDate:(NSDate *)date;

/**
 Sent by a parser object to provide its delegate with the object identifier encoded in the current element.
 
 @param parser A parser object.
 @param objIdentifier A string representing the object identifier encoded in the current element.
 */
- (void)parser:(AFASN1Parser *)parser foundObjectIdentifier:(NSString *)objIdentifier;

/**
 Sent by a parser object to provide its delegate with the string encoded in the current element.
 
 All the ASN1 string types are provided via this method.
 
 @param parser A parser object.
 @param string A string contained in the current element.
 */
- (void)parser:(AFASN1Parser *)parser foundString:(NSString *)string;

/**
 Sent by a parser object to provide its delegate with the octet string encoded in the current element.
 
 Integer data that is longer than 32 bits is also provided this way.
 
 @param parser A parser object.
 @param data A data object representing the contents of the current element.
 */
- (void)parser:(AFASN1Parser *)parser foundData:(NSData *)data;

/**
 Sent by a parser object to provide its delegate with the bit string encoded in the current element.
 
 @param parser A parser object.
 @param bitString A bit string object representing the contents of the current element.
 */
- (void)parser:(AFASN1Parser *)parser foundBitString:(AFASN1BitString *)bitString;

/**
 Sent by a parser object to provide its delegate with number values encoded in the current element.
 
 Note that number values that are longer than supported by the system are provided as Data instead.
 
 @param parser A parser object.
 @param number A number object representing the contents of the current element.
 */
- (void)parser:(AFASN1Parser *)parser foundNumber:(NSNumber *)number;


@end

@interface AFASN1Parser : NSObject


@property (nonatomic, weak) id<AFASN1ParserDelegate> delegate;
@property (nonatomic, readonly, strong) NSError *parserError;

/**
 Initializes the receiver with the ASN1 contents encapsulated in a given data object.
 
 @param data An `NSData` object containing ASN1 encoded data.
 @returns An initialized `DTASN1Parser` object or nil if an error occurs.
 */
- (instancetype)initWithData:(NSData *)data;

/**
 Starts the event-driven parsing operation.
 
 If you invoke this method, the delegate, if it implements parser:parseErrorOccurred:, is informed of the cancelled parsing operation.
 
 @returns `YES` if parsing is successful and `NO` in there is an error or if the parsing operation is aborted.
 */
- (BOOL)parse;

/**
 Stops the parser object.
 
 @see parse
 @see parserError
 */
- (void)abortParsing;


@end
