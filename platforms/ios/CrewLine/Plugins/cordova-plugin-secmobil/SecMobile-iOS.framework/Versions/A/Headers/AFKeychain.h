//
//  AFKeychain.h
//  SecMobile
//
//  Created by Laurent Gaches on 08/08/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 AFKeychain has static methods for managing the keychain
 */
@interface AFKeychain : NSObject 


/**---------------------------------------------------------------------------------------
 * @name Password
 *  ---------------------------------------------------------------------------------------
 */

/**

*/
+ (BOOL)addPassword:(NSString *)password forService:(NSString *)service account:(NSString *)account error:(NSError **)error;

/**
 
 */
+ (BOOL)addPassword:(NSString *)password forService:(NSString *)service account:(NSString *)account accessGroup:(NSString *)accessGroup error:(NSError **)error;


/**
 Returns a string containing the password
 @param service The service for which to return the corresponding password
 @param account The account for which to return the corresponding password
 @return Returns a string containing the password, or `nil` if the Keychain doesn't have a password.
 */
+ (NSString *)getPasswordForService:(NSString *)service account:(NSString *)account error:(NSError **)error;

/**
 Returns a string containing the password
 
 @param service The service for which to return the corresponding password
 @param account The account for which to return the corresponding password
 @param accessGroup The accessGroup for which to return the corresponding password
 @return Returns a string containing the password, or `nil` if the Keychain doesn't have a password.
 */
+ (NSString *)getPasswordForService:(NSString *)service account:(NSString *)account accessGroup:(NSString *)accessGroup error:(NSError **)error;

+ (BOOL)deletePasswordForService:(NSString *)service account:(NSString *)account error:(NSError **)error;
+ (BOOL)deletePasswordForService:(NSString *)service account:(NSString *)account accessGroup:(NSString *)accessGroup error:(NSError **)error;


/**
 Update a password from the Keychain.
 
 @param updatedPassword The new password to store.
 @param service The service for which to update the corresponding password.
 @param account The account for which to update the corresponding password.
 
 @warning : use update method instead of delete and add methods when you need to change a password.
 */
+ (BOOL)updatePassword:(NSString *)updatedPassword forService:(NSString *)service account:(NSString *)account error:(NSError **)error;

/**
 Update a password from the Keychain.
 
 @param updatedPassword The new password to store.
 @param service The service for which to update the corresponding password.
 @param account The account for which to update the corresponding password.
 @param accessGroup The Access Group for which to update the corresponding password.
 
 @warning : use update method instead of delete and add methods when you need to change a password.
 */

+ (BOOL)updatePassword:(NSString *)updatedPassword forService:(NSString *)service account:(NSString *)account accessGroup:(NSString *)accessGroup error:(NSError **)error;


/**---------------------------------------------------------------------------------------
 * @name Identity
 *  ---------------------------------------------------------------------------------------
 */

/**
 
 */
+ (BOOL)addIdentity:(SecIdentityRef)identity label:(NSString *)label error:(NSError **)error;

/**
 
 */
+ (BOOL)addIdentity:(SecIdentityRef)identity accessGroup:(NSString *)accessGroup label:(NSString *)label error:(NSError **)error;

/**
 
 */
+(SecIdentityRef) getIdentity:(NSError **)error label:(NSString *)label;

/**
 
 */
+(SecIdentityRef) getIdentity:(NSError **)error accessGroup:(NSString *)accessGroup label:(NSString *)label;

/**
 
 */
+ (BOOL)removeIdentityWithLabel:(NSString *)label error:(NSError **)error;

/**
 
 */
+ (BOOL)removeIdentityWithLabel:(NSString *)label accessGroup:(NSString *)accessGroup error:(NSError **)error;



@end
