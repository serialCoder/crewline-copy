//
//  UIDevice+AF.h
//  SecMobile
//
//  Created by Laurent Gaches on 03/10/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (AF)

- (NSString *) afPlatform;

@end
