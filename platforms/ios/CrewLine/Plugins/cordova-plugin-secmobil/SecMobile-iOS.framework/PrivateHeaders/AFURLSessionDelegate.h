//
//  AFURLSessionDelegate.h
//  SecMobile
//
//  Created by lehmann on 25/09/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AFURLSessionDelegate : NSObject <NSURLSessionTaskDelegate>

@property (strong, nonatomic) NSURLCredential *credential;

@end
