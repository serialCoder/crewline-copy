#import <Cordova/CDV.h>

@interface BrandKhorosMessenger : CDVPlugin

- (void)show:(CDVInvokedUrlCommand *)command;

- (void)destroy:(CDVInvokedUrlCommand *)command;

- (void)test:(CDVInvokedUrlCommand *)command;

- (void)init:(CDVInvokedUrlCommand *)command;

@end
