#import "BrandKhorosMessenger.h"
#import <BrandMessenger/BrandMessenger.h>
#import <objc/runtime.h>
#import <UserNotifications/UserNotifications.h>

static void (*didRegisterForRemoteNotificationsOriginalImpl)(id self, SEL _cmd, UIApplication* application, NSData* deviceToken);
static void didRegisterForRemoteNotificationsNewImpl(id self, SEL _cmd, UIApplication* application, NSData* deviceToken) {
    [BrandMessenger setPushToken:deviceToken];

    NSUInteger in_length = [deviceToken length];

    const unsigned char *in_bytes = [deviceToken bytes];

    NSMutableString *lc_string = [[NSMutableString alloc] initWithCapacity:(in_length * 2)];

     for(int i=0;i<in_length; i++){

          [lc_string appendFormat:@"%02x", in_bytes[i]];
    }
    NSLog(@"The device token: %@", lc_string);
    if(didRegisterForRemoteNotificationsOriginalImpl){

        didRegisterForRemoteNotificationsOriginalImpl(self, _cmd, application, deviceToken);
    }
}



@interface BrandMessengerNotificationCenterDelegate: NSObject <UNUserNotificationCenterDelegate>

+(id)sharedInstance;

@end

@implementation BrandMessengerNotificationCenterDelegate

+(id)sharedInstance {

    static id _sharedObject = nil;

    static dispatch_once_t pred=0;

    dispatch_once(&pred, ^{

        _sharedObject = [[self alloc] init];

    });
    return _sharedObject;

}

@end



@interface BrandKhorosMessenger() <KBMConversationDelegate>

@property (nonatomic, strong) NSString* callbackId;

@end;

@implementation BrandKhorosMessenger

 
- (void)init:(CDVInvokedUrlCommand *)command {
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(messagesReceived) name:KBMConversationDidReceiveMessagesNotification object:nil];
    
    __block CDVPluginResult *pluginResult;
    
       NSString *integrationAppId = [command.arguments objectAtIndex:0];
       NSString *region = [command.arguments objectAtIndex:1];
       NSString *jwtToken = [command.arguments objectAtIndex:2];
       NSString *registrationNumber = [command.arguments objectAtIndex:3];
       NSString *firstName = [command.arguments objectAtIndex:4];
       NSString *lastName = [command.arguments objectAtIndex:5];
       NSString *email = [command.arguments objectAtIndex:6];
       NSString *function = [command.arguments objectAtIndex:7];
       NSString *division = [command.arguments objectAtIndex:8];
       NSString *populationType = [command.arguments objectAtIndex:9];
       NSString *haulType = [command.arguments objectAtIndex:10];

    KBMSettings *settings = [KBMSettings settingsWithIntegrationId: integrationAppId];
    
    // enable offline mode
    settings.allowOfflineUsage = YES;
    settings.allowedMenuItems = @[KBMMenuItemCamera, KBMMenuItemGallery, KBMMenuItemDocument, KBMMenuItemLocation];
   
       // Defining region for eu (by default it's us => nothing needed for us)
       if ([region isEqualToString:@"eu-1"]) {

           settings.region = @"eu-1";
       }
    [BrandMessenger initWithSettings:settings completionHandler:^(NSError * _Nullable error, NSDictionary * _Nullable userInfo) {
        [BrandMessenger conversation].delegate = self;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name: KBMConversationDidReceiveMessagesNotification object:nil];
    if(error == nil) { 
        // after successfull initialization, the brand messenger login start.

       dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           
            [BrandMessenger login:registrationNumber jwt:jwtToken completionHandler:^(NSError * _Nullable error, NSDictionary * _Nullable userInfo) {
                NSMutableDictionary* properties = [[NSMutableDictionary alloc] init];
              
                [properties setObject:registrationNumber forKey:@"registrationNumber"];
                [properties setObject:function forKey:@"function"];
                [properties setObject:division forKey:@"division"];
                [properties setObject:populationType  forKey:@"populationType"];
                [properties setObject:haulType forKey:@"haulType"];
                [properties setObject:email forKey:@"email"];
               

                KBMUser* user = [KBMUser currentUser];

                [user setFirstName:firstName];
                [user setLastName:lastName];
                [user setEmail:email];
                [user addProperties:properties];
                
            }];
        });

           pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                        messageAsString:@"Brand messenger sdk initialisation has been done successfuly"];

    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                        messageAsString:@"Brand messenger sdk couldn't be initialized successfully"];

    }

    [self.commandDelegate sendPluginResult: pluginResult
    callbackId: command.callbackId];

    }];
}


- (void)show:(CDVInvokedUrlCommand *)command {
     UIViewController *conversationController = [BrandMessenger newConversationViewController];
     self.callbackId = command.callbackId;
     [BrandMessenger conversation].delegate = self;
     [self.viewController presentViewController: conversationController
                         animated:NO
                         completion:nil];

     CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];

     [self.commandDelegate sendPluginResult: pluginResult

                                callbackId: command.callbackId];

}

- (void)test:(CDVInvokedUrlCommand *)command {
    NSLog(@"Ce ci est un appel à la fonction de test depuis la sdk");
   
    [self.commandDelegate sendPluginResult: [CDVPluginResult resultWithStatus:CDVCommandStatus_OK]

                             callbackId: command.callbackId];

}
- (void)destroy:(CDVInvokedUrlCommand *)command {
    [BrandMessenger destroy];
    [self.commandDelegate sendPluginResult: [CDVPluginResult resultWithStatus:CDVCommandStatus_OK]

                             callbackId: command.callbackId];

}

- (void)conversation:(KBMConversation *)conversation willDismissViewController:(UIViewController *)viewController {
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                      messageAsString:@"conversation:willDismissViewController:"];

    [self.commandDelegate sendPluginResult: pluginResult
                                callbackId: self.callbackId];

    self.callbackId = nil;

}

+(void)load {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(applicationDidFinishLaunching:)
                                               name:UIApplicationDidFinishLaunchingNotification
                                               object:nil];

}

+(void)applicationDidFinishLaunching:(NSNotification*)notification {
    
    Class appDelegateClass = [[UIApplication sharedApplication].delegate class];
    [self replaceMethod:@selector(application:didRegisterForRemoteNotificationsWithDeviceToken:) ofClass:appDelegateClass with:(IMP)didRegisterForRemoteNotificationsNewImpl storeOriginalImplementation:(IMP *)&didRegisterForRemoteNotificationsOriginalImpl];
    [self registerForPushNotifications:[UIApplication sharedApplication]];

}



+(void)registerForPushNotifications:(UIApplication*)application {

    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later

        // For iOS 10 display notification (sent via APNS)

        [UNUserNotificationCenter currentNotificationCenter].delegate = [BrandMessengerNotificationCenterDelegate sharedInstance];

        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;

        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions

                                                                            completionHandler:^(BOOL granted, NSError * _Nullable error) {

            if(granted) {
                dispatch_async(dispatch_get_main_queue(), ^{

                    [application registerForRemoteNotifications];

                });
            }

        }];

    } else {

        // iOS 10 notifications aren’t available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];

        [application registerForRemoteNotifications];

    }

}

+(BOOL)replaceMethod:(SEL)selector ofClass:(Class)class with:(IMP)newImplementation storeOriginalImplementation:(IMP*)methodStore
{
    IMP imp = NULL;

    Method method = class_getInstanceMethod(class, selector);

    if (method) {

        const char *type = method_getTypeEncoding(method);

        imp = class_replaceMethod(class, selector, newImplementation, type);

        if (!imp) {

            imp = method_getImplementation(method);

        }

    }else{

        const char *type = @encode(Method);

        class_addMethod(class, selector, newImplementation, type);

    }

    if (imp && methodStore) { *methodStore = imp; }

    return (imp != NULL);

}

-(void)messagesReceived
{
    [BrandMessenger conversation].delegate = self;
}

-(BOOL)conversation:(KBMConversation*)conversation shouldShowInAppNotificationForMessage:(KBMConversation*)message {
    NSLog(@"shouldShowInAppNotificationForMessage called");
    return false;
}

@end
