(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-conditions-generales-conditions-generales-module"],{

/***/ "./src/app/pages/conditions-generales/conditions-generales.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/conditions-generales/conditions-generales.module.ts ***!
  \***************************************************************************/
/*! exports provided: ConditionsGeneralesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionsGeneralesPageModule", function() { return ConditionsGeneralesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _conditions_generales_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./conditions-generales.page */ "./src/app/pages/conditions-generales/conditions-generales.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _component_header_header_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../component/header/header.module */ "./src/app/component/header/header.module.ts");









var routes = [
    {
        path: '',
        component: _conditions_generales_page__WEBPACK_IMPORTED_MODULE_6__["ConditionsGeneralesPage"]
    }
];
var ConditionsGeneralesPageModule = /** @class */ (function () {
    function ConditionsGeneralesPageModule() {
    }
    ConditionsGeneralesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _component_header_header_module__WEBPACK_IMPORTED_MODULE_8__["HeaderModule"]
            ],
            declarations: [_conditions_generales_page__WEBPACK_IMPORTED_MODULE_6__["ConditionsGeneralesPage"]]
        })
    ], ConditionsGeneralesPageModule);
    return ConditionsGeneralesPageModule;
}());



/***/ }),

/***/ "./src/app/pages/conditions-generales/conditions-generales.page.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/conditions-generales/conditions-generales.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<ion-content id=\"content\" padding=\"true\">\r\n    <h1>Conditions générales d'utilisation de l'application CREWLINE</h1>\r\n    <br>\r\n    <h3>CONDITIONS GENERALES D'UTILISATION </h3>\r\n    <p>Les présentes conditions générales ont pour objet de définir les modalités d'accès des Mentions d’information GDPR pour un processus interne V.1.2 utilisateurs au système d'information d'Air France.</p>\r\n    <p>Le système d'information mis à disposition des utilisateurs est un outil professionnel. Il appartient à chacun d'adopter un comportement professionnel et responsable lors de son utilisation.</p>\r\n    <p>Air France est seul juge de la mise à disposition de son système d'information et se réserve le droit d'en restreindre ou d'en suspendre à tout moment l'accès.</p>\r\n    <p>L'ensemble des données, textes, informations, images, photographies ou tout autre contenu diffusé sur le site relèvent de la pleine et entière propriété d'Air France. Ces informations font l'objet d'une protection au titre du droit de la propriété intellectuelle. Par conséquent, les utilisateurs ne peuvent utiliser ces éléments qu'à des fins exclusivement privées (cercle familial).</p>\r\n    <p>A l'exception des dispositions ci-dessus, toute reproduction, représentation, utilisation ou adaptation, sous quelque forme que ce soit, de tout ou partie des éléments du site sans l'accord écrit d'Air France pourra donner lieu à des poursuites conformément au Code de la Propriété Intellectuelle.</p>\r\n    <p>La mise en place de tout moyen (automates, liens hypertextes, etc…) permettant un accès vers tout ou partie de l'Application CREW LINE est strictement interdite, sauf autorisation préalable et écrite d'Air France.</p>\r\n    <p>Les atteintes ou tentatives d'atteinte aux systèmes de traitement automatisé de données d'Air France sont passibles de sanctions pénales et/ou de peines complémentaires conformément aux articles 323-1 et suivants du code pénal.</p>\r\n    <p>Sont notamment interdits les actes suivants dès lors qu'ils ont été commis sciemment :</p>\r\n    <ul>\r\n        <li>le détournement des informations propres à Air France,</li>\r\n        <li>la collecte, l'enregistrement, l'archivage et la transmission à des tiers des données nominatives ou non sans autorisation préalable d'Air France,</li>\r\n        <li>L'utilisation du réseau dans le but de concurrence déloyale, notamment en jetant le discrédit sur les produits, les services et le savoir-faire d'Air France et de ses partenaires,</li>\r\n        <li>Le manquement à l'obligation de réserve en révélant des informations concernant tant Air France, que ses partenaires, agents, sous-traitants et clients,</li>\r\n        <li>L'atteinte au secret professionnel,</li>\r\n        <li>L'atteinte aux droits de propriété intellectuelle et droits de la personnalité, ainsi qu'à tout droit détenu ou concédé à Air France, et notamment en procédant à des copies, des modifications, des transmissions de logiciels, d'œuvres multimédias ou de toute autre création artistique ou industrielle, sans avoir obtenu les droits nécessaires de l'auteur ou du titulaire des droits,</li>\r\n        <li>L'atteinte aux droits d'auteur, qu'il s'agisse de création multimédia, de logiciels, de textes, de photos, d'images de toutes natures, étant précisé que toutes mentions relatives aux droits d'auteur ne pourront faire l'objet d’une suppression, d'une citation partielle ou d'une modification,</li>\r\n        <li>La destruction, la dégradation ou la détérioration des biens appartenant à Air France ou à des tiers à l'aide du réseau,</li>\r\n        <li>La réalisation ou la diffusion de fausses déclarations visant à falsifier les données de l'entreprise ou à tromper les destinataires ou les correspondants,</li>\r\n        <li>La suppression ou la modification de données au préjudice d'Air France,</li>\r\n        <li>Le fait de se maintenir frauduleusement dans tout ou partie des bases de données non autorisées,</li>\r\n        <li>Les actions conduisant à entraver, fausser, altérer, détourner ou modifier le fonctionnement ou la configuration d'un système.</li>\r\n    </ul>\r\n    <p>Dans des circonstances très exceptionnelles, Air France peut être légalement amenée à communiquer des informations confidentielles lorsque cette démarche est nécessaire à l'identification, à l'interpellation ou à la poursuite en justice de tout individu susceptible de porter préjudice ou atteinte, intentionnellement ou non :</p>\r\n    <ul>\r\n        <li>aux droits ou à la propriété d'Air France,</li>\r\n        <li>à d'autres Les présentes conditions générales ont pour objet de définir les modalités d'accès des utilisateurs au système d'information d'Air France. Utilisateurs du site d'Air France,</li>\r\n        <li>à toute autre personne qui pourrait être pénalisée par de telles activités.</li>\r\n    </ul>\r\n    <p>La communication de données personnelles peut ainsi exceptionnellement intervenir lorsqu’Air France est légalement tenue de le faire, suite au dépôt d'une plainte par exemple.</p>\r\n    <p>Air France pourra effectuer des mises à jour à tout moment des conditions générales d'utilisation définies aux présentes.</p>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/conditions-generales/conditions-generales.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/conditions-generales/conditions-generales.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content h1 {\n  text-align: center;\n  font-size: 4.2vw; }\n\nion-content h3 {\n  font-size: 3.5vw;\n  font-weight: bold; }\n\nion-content p, ion-content li {\n  font-size: 3vw; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29uZGl0aW9ucy1nZW5lcmFsZXMvQzpcXFVzZXJzXFxtNDI2MTU1XFxEZXNrdG9wXFxjcmV3bGluZS1jb3B5L3NyY1xcYXBwXFxwYWdlc1xcY29uZGl0aW9ucy1nZW5lcmFsZXNcXGNvbmRpdGlvbnMtZ2VuZXJhbGVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTs7QUFIcEI7RUFPSSxnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBUnJCO0VBWUksY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29uZGl0aW9ucy1nZW5lcmFsZXMvY29uZGl0aW9ucy1nZW5lcmFsZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIGgxIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogNC4ydnc7XHJcbiAgfVxyXG5cclxuICBoMyB7XHJcbiAgICBmb250LXNpemU6IDMuNXZ3O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgfVxyXG5cclxuICBwLCBsaSB7XHJcbiAgICBmb250LXNpemU6IDN2dztcclxuICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/pages/conditions-generales/conditions-generales.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/conditions-generales/conditions-generales.page.ts ***!
  \*************************************************************************/
/*! exports provided: ConditionsGeneralesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionsGeneralesPage", function() { return ConditionsGeneralesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var ConditionsGeneralesPage = /** @class */ (function () {
    function ConditionsGeneralesPage(menu) {
        this.menu = menu;
    }
    ConditionsGeneralesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-conditions-generales',
            template: __webpack_require__(/*! ./conditions-generales.page.html */ "./src/app/pages/conditions-generales/conditions-generales.page.html"),
            styles: [__webpack_require__(/*! ./conditions-generales.page.scss */ "./src/app/pages/conditions-generales/conditions-generales.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]])
    ], ConditionsGeneralesPage);
    return ConditionsGeneralesPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-conditions-generales-conditions-generales-module.js.map