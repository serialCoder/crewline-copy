cordova.define("cordova-plugin-secmobil.CertAuthPlugin", function(require, exports, module) {
function Plugin() {}

Plugin.secMobilGetCertificate = function(content, onSuccess, onFail) {
    cordova.exec(onSuccess, onFail, 'CertAuthPlugin', 'getCertificate', [content]);
};

Plugin.secMobilRevokeCertificate = function(content, onSuccess, onFail) {
    cordova.exec(onSuccess, onFail, 'CertAuthPlugin', 'revokeCertificate', [content]);
};

Plugin.secMobilHasCertificate = function(content, onSuccess, onFail) {
    cordova.exec(onSuccess, onFail, 'CertAuthPlugin', 'hasCertificate', [content]);
};

Plugin.secMobilCheckValidityOfCertificate = function(content, onSuccess, onFail) {
    cordova.exec(onSuccess, onFail, 'CertAuthPlugin', 'checkValidityOfCertificate', [content]);
};

Plugin.secMobilCallRestService = function(content, onSuccess, onFail) {
    cordova.exec(onSuccess, onFail, 'CertAuthPlugin', 'callRestService', [content]);
};

Plugin.initSecmobilHttp = function(content, onSuccess, onFail) {
    cordova.exec(onSuccess, onFail, 'CertAuthPlugin', 'initPlugin', [content]);
};

Plugin.secMobilSetAppGroup = function(content, onSuccess, onFail) {
    cordova.exec(onSuccess, onFail, 'CertAuthPlugin', 'setAppGroup', [content]);
};

Plugin.secMobilGetJWTToken = function(content, onSuccess, onFail) {
    cordova.exec(onSuccess, onFail, 'CertAuthPlugin', 'getJWTToken', [content]);
}

/*Plugin.secMobilGetOAuth2AccessToken = function(content, onSuccess, onFail) {
    cordova.exec(onSuccess, onFail, 'CertAuthPlugin', 'getOAuth2AccessToken', [content]);
}*/

module.exports = Plugin;

});
