cordova.define("cordova-plugin-brandkhoros.BrandKhorosMessenger", function(require, exports, module) {
const exec = require('cordova/exec'),
    nativePluginName = 'BrandKhorosMessenger';

function Plugin() {};

Plugin.show = function (arg0, success, error) {
    exec(success, error, nativePluginName, "show", [arg0]);
};

Plugin.destroy = function (arg0, success, error) {
    exec(success, error, nativePluginName, "destroy", [arg0]);
};


Plugin.init = function (khorosAppId, registrationNumber, khorosAppRegion, jwt, firstName, lastName, email, speciality, division, populationType, haultype, success, error) {
    exec(success, error, nativePluginName, "init", [khorosAppId, khorosAppRegion, jwt, registrationNumber, firstName, lastName, email, speciality, division, populationType, haultype]);
};

Plugin.test = function (arg0, success, error) {
    exec(success, error, nativePluginName, "test", [arg0]);
};


module.exports = Plugin;
});
