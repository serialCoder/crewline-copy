(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-mentions-legales-mentions-legales-module"],{

/***/ "./src/app/pages/mentions-legales/mentions-legales.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/mentions-legales/mentions-legales.module.ts ***!
  \*******************************************************************/
/*! exports provided: MentionsLegalesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MentionsLegalesPageModule", function() { return MentionsLegalesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _mentions_legales_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./mentions-legales.page */ "./src/app/pages/mentions-legales/mentions-legales.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _component_header_header_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../component/header/header.module */ "./src/app/component/header/header.module.ts");









var routes = [
    {
        path: '',
        component: _mentions_legales_page__WEBPACK_IMPORTED_MODULE_6__["MentionsLegalesPage"]
    }
];
var MentionsLegalesPageModule = /** @class */ (function () {
    function MentionsLegalesPageModule() {
    }
    MentionsLegalesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _component_header_header_module__WEBPACK_IMPORTED_MODULE_8__["HeaderModule"]
            ],
            declarations: [_mentions_legales_page__WEBPACK_IMPORTED_MODULE_6__["MentionsLegalesPage"]]
        })
    ], MentionsLegalesPageModule);
    return MentionsLegalesPageModule;
}());



/***/ }),

/***/ "./src/app/pages/mentions-legales/mentions-legales.page.html":
/*!*******************************************************************!*\
  !*** ./src/app/pages/mentions-legales/mentions-legales.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<ion-content id=\"content\" padding=\"true\">\r\n    <h1>Mentions d’information GDPR pour un processus interne (données « employés »)</h1>\r\n    <br>\r\n    <h3>Objet du traitement (finalité et base légale) :</h3>\r\n    <p>Conformément à la réglementation applicable en matière de protection des données à caractère personnel, notamment les dispositions de la loi « informatique et libertés » du 6 janvier 1978 modifiée et du Règlement Général sur la Protection des Données (Règlement UE 2016/679) ou « RGPD », Air France, en qualité de responsable de traitement, vous informe que les informations recueillies dans Crew Line font l'objet d'un traitement de données à caractère personnel.</p>\r\n    <p>Ces données sont collectées et traitées dans le cadre d’un dispositif d’assistance personnalisée aux personnels navigants.</p>\r\n    <p>La base légale du traitement est l’intérêt légitime d’Air France (cf. article 6.1.f du Règlement Général sur la Protection des Données ou RGPD).</p>\r\n    <h3>Catégories de données personnelles</h3>\r\n    <p>Les catégories de données personnelles qui sont collectées dans le cadre de ce dispositif sont les suivantes :</p>\r\n    <ul>\r\n        <li>Nom, prénom, matricule, grade</li>\r\n    </ul>\r\n    <p>Toutes les données recueillies sont nécessaires pour l’utilisation du service CREW LINE. En l’absence de recueil de ces données, le personnel ne sera pas en mesure de s’identifier au service CREW LINE et communiquer avec l’assistance personnelle.</p>\r\n    <h3>Durée de conservation des données :</h3>\r\n    <p>Les données sont conservées pendant toute la durée du contrat chez Air France du personnel navigant.</p>\r\n    <h3>Destinataires des données :</h3>\r\n    <p>Les données sont destinées au département OA/IS ainsi qu’aux sous-traitants auxquels AIR FRANCE fait appel pour la mise en œuvre du traitement soit la société KHOROS.</p>\r\n    <h3>Transfert de données hors UE :</h3>\r\n    <p>Certains des destinataires ou sous-traitants mentionnés ci-dessus sont susceptibles d'être établis en dehors de l’Espace Economique Européen et dans un pays dont le niveau de protection n’est pas considéré comme adéquat par la Commission Européenne. Les transferts correspondants sont réalisés en conformité avec les dispositions légales et règlementaires applicables, notamment au travers de la signature, au cas par cas, de clauses contractuelles basées sur le modèle de la commission européenne, ou tout autre mécanisme conforme RGPD.</p>\r\n    <h3>Droits des personnes :</h3>\r\n    <p>Conformément à la réglementation applicable en matière de protection des données à caractère personnel, vous disposez du droit de définir des directives relatives à la conservation, à l’effacement et à la communication de vos données à caractère personnel post mortem.</p>\r\n    <p>Pour exercer ces droits ou pour toute question sur le traitement de vos données à caractère personnel dans ce dispositif, vous pouvez adresser votre demande à votre RRH dont le contact est disponible dans les e-services RH dans l’onglet Organisation -->Mes contacts utiles.</p>\r\n    <p>Air France a désigné un Délégué à la Protection des Données, qui peut être contacté par e-mail à mail.data.protection@airfrance.fr. Vous pouvez également introduire une réclamation auprès de la Commission Nationale de l'Informatique et des Libertés (CNIL) ou de toute autre autorité compétente.</p>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/mentions-legales/mentions-legales.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/mentions-legales/mentions-legales.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content h1 {\n  text-align: center;\n  font-size: 4.2vw; }\n\nion-content h3 {\n  font-size: 3.5vw;\n  font-weight: bold; }\n\nion-content p, ion-content li {\n  font-size: 3vw; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudGlvbnMtbGVnYWxlcy9DOlxcVXNlcnNcXG00MjYxNTVcXERlc2t0b3BcXGNyZXdsaW5lLWNvcHkvc3JjXFxhcHBcXHBhZ2VzXFxtZW50aW9ucy1sZWdhbGVzXFxtZW50aW9ucy1sZWdhbGVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTs7QUFIcEI7RUFPSSxnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBUnJCO0VBWUksY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWVudGlvbnMtbGVnYWxlcy9tZW50aW9ucy1sZWdhbGVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICBoMSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDQuMnZ3O1xyXG4gIH1cclxuXHJcbiAgaDMge1xyXG4gICAgZm9udC1zaXplOiAzLjV2dztcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxuXHJcbiAgcCwgbGkge1xyXG4gICAgZm9udC1zaXplOiAzdnc7XHJcbiAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/mentions-legales/mentions-legales.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/mentions-legales/mentions-legales.page.ts ***!
  \*****************************************************************/
/*! exports provided: MentionsLegalesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MentionsLegalesPage", function() { return MentionsLegalesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var MentionsLegalesPage = /** @class */ (function () {
    function MentionsLegalesPage(menu) {
        this.menu = menu;
    }
    MentionsLegalesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mentions-legales',
            template: __webpack_require__(/*! ./mentions-legales.page.html */ "./src/app/pages/mentions-legales/mentions-legales.page.html"),
            styles: [__webpack_require__(/*! ./mentions-legales.page.scss */ "./src/app/pages/mentions-legales/mentions-legales.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]])
    ], MentionsLegalesPage);
    return MentionsLegalesPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-mentions-legales-mentions-legales-module.js.map