(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var _component_header_header_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../component/header/header.module */ "./src/app/component/header/header.module.ts");









var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_7__["HomePage"]
                    }
                ]),
                _component_header_header_module__WEBPACK_IMPORTED_MODULE_8__["HeaderModule"]
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_7__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/pages/home/home.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header [hideBackButton]=\"true\"></app-header>\r\n<ion-content id=\"content\">\r\n  <div id=\"impersonate-info\">\r\n    <b *ngIf=\"getImpersonatedUser()\">Impersonate :\r\n      {{getImpersonatedUser().registrationNumber}} {{getImpersonatedUser().firstName}}\r\n      {{getImpersonatedUser().lastName}}</b>\r\n  </div>\r\n  <div id=\"top-part\">\r\n    <div class=\"login-in-progress\" *ngIf=\"authenticationInProgress; else authenticationDoneBlock\">\r\n      {{ 'home.authent_in_progress' | translate }} <ion-spinner> </ion-spinner>\r\n    </div>\r\n    <ng-template #authenticationDoneBlock>\r\n      <h2 *ngIf=\"getActiveUser(); else logonErrorBlock\">\r\n        {{getActiveUser().firstName}}&nbsp;{{getActiveUser().lastName}}</h2>\r\n      <ng-template #logonErrorBlock>\r\n        <div class=\"no-access\">\r\n          <ion-icon name=\"warning\"></ion-icon><span>{{ 'home.error_retrieving_user_data' | translate }}</span>\r\n        </div>\r\n      </ng-template>\r\n    </ng-template>\r\n    <img class=\"ion-padding-bottom\" src=\"/assets/icon/logo-crewline.svg\" alt=\"crewline logo\" />\r\n    <h1>{{ 'home.welcome' | translate }}</h1>\r\n\r\n    <h3>{{ 'home.welcome_message' | translate }}</h3>\r\n  </div>\r\n  <div id=\"bottom-part\">\r\n    <ion-button expand=\"block\" fill=\"solid\" color=\"light\" (click)=\"openMessengerApp()\" *ngIf=\"getActiveUser()\">Messenger\r\n    </ion-button>\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --ion-background-color: #051039;\n  text-align: center; }\n  ion-content #impersonate-info {\n    color: white;\n    margin: 10px;\n    height: 5%; }\n  ion-content #top-part {\n    height: 70%;\n    padding: 7.5vh; }\n  @media screen and (max-width: 500px) {\n      ion-content #top-part {\n        padding: 3vh; } }\n  ion-content #top-part .login-in-progress {\n      color: white;\n      font-size: 2vh;\n      display: flex;\n      align-items: center;\n      justify-content: center; }\n  ion-content #top-part .login-in-progress ion-spinner {\n        color: white;\n        margin-left: 16px; }\n  ion-content #top-part img {\n      margin: 2vh;\n      margin-top: 4vh;\n      height: 15vh;\n      padding: 0; }\n  ion-content #top-part h1 {\n      font-size: 4vh;\n      font-family: \"Opens Sans\";\n      color: white;\n      font-weight: 400; }\n  ion-content #top-part h2 {\n      font-size: 3vh;\n      font-family: \"Opens Sans\";\n      color: white;\n      font-weight: 400; }\n  ion-content #top-part h3 {\n      font-size: 2vh;\n      padding: 2vh;\n      font-family: \"Opens Sans\";\n      color: white;\n      font-weight: 200; }\n  ion-content #bottom-part {\n    height: 20%; }\n  ion-content #bottom-part ion-button {\n      display: inline-block;\n      width: 27vh;\n      height: 7vh;\n      border-radius: 1000px;\n      font-family: \"Excellence In Motion Screen\";\n      font-size: 3vh; }\n  ion-content .no-access {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    background: white;\n    color: #051039;\n    padding: 16px;\n    font-size: 2.5vh; }\n  ion-content .no-access span {\n      text-align: left; }\n  ion-content .no-access ion-icon {\n      margin-right: 16px;\n      height: 80px;\n      font-size: 8rem; }\n  @media screen and (max-width: 500px) {\n        ion-content .no-access ion-icon {\n          height: 50px;\n          font-size: 5rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9DOlxcVXNlcnNcXG00MjYxNTVcXERlc2t0b3BcXGNyZXdsaW5lLWNvcHkvc3JjXFxhcHBcXHBhZ2VzXFxob21lXFxob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVFLCtCQUF1QjtFQUN2QixrQkFBa0IsRUFBQTtFQUhwQjtJQU1JLFlBQVk7SUFDWixZQUFZO0lBQ1osVUFBVSxFQUFBO0VBUmQ7SUFhSSxXQUFXO0lBQ1gsY0FBYyxFQUFBO0VBRWQ7TUFoQko7UUFpQk0sWUFBWSxFQUFBLEVBa0RmO0VBbkVIO01BcUJNLFlBQVk7TUFDWixjQUFjO01BQ2QsYUFBYTtNQUNiLG1CQUFtQjtNQUNuQix1QkFBdUIsRUFBQTtFQXpCN0I7UUE0QlEsWUFBWTtRQUNaLGlCQUFpQixFQUFBO0VBN0J6QjtNQWtDTSxXQUFXO01BQ1gsZUFBZTtNQUNmLFlBQVk7TUFDWixVQUFVLEVBQUE7RUFyQ2hCO01BeUNNLGNBQWM7TUFFZCx5QkFBeUI7TUFDekIsWUFBWTtNQUNaLGdCQUFnQixFQUFBO0VBN0N0QjtNQWlETSxjQUFjO01BRWQseUJBQXlCO01BQ3pCLFlBQVk7TUFDWixnQkFBZ0IsRUFBQTtFQXJEdEI7TUF5RE0sY0FBYztNQUNkLFlBQVk7TUFFWix5QkFBeUI7TUFDekIsWUFBWTtNQUNaLGdCQUFnQixFQUFBO0VBOUR0QjtJQXVFSSxXQUFXLEVBQUE7RUF2RWY7TUEwRU0scUJBQXFCO01BQ3JCLFdBQVc7TUFDWCxXQUFXO01BQ1gscUJBQXFCO01BRXJCLDBDQUEwQztNQUMxQyxjQUFjLEVBQUE7RUFoRnBCO0lBcUZJLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QsYUFBYTtJQUNiLGdCQUFnQixFQUFBO0VBM0ZwQjtNQThGTSxnQkFBZ0IsRUFBQTtFQTlGdEI7TUFrR00sa0JBQWtCO01BQ2xCLFlBQVk7TUFDWixlQUFlLEVBQUE7RUFFZjtRQXRHTjtVQXVHUSxZQUFZO1VBQ1osZUFBZSxFQUFBLEVBRWxCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuXHJcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzA1MTAzOTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICNpbXBlcnNvbmF0ZS1pbmZvIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIG1hcmdpbjogMTBweDtcclxuICAgIGhlaWdodDogNSU7XHJcbiAgfVxyXG5cclxuICAjdG9wLXBhcnQge1xyXG5cclxuICAgIGhlaWdodDogNzAlO1xyXG4gICAgcGFkZGluZzogNy41dmg7XHJcblxyXG4gICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcclxuICAgICAgcGFkZGluZzogM3ZoO1xyXG4gICAgfVxyXG5cclxuICAgIC5sb2dpbi1pbi1wcm9ncmVzcyB7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgZm9udC1zaXplOiAydmg7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxuICAgICAgaW9uLXNwaW5uZXIge1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTZweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGltZyB7XHJcbiAgICAgIG1hcmdpbjogMnZoO1xyXG4gICAgICBtYXJnaW4tdG9wOiA0dmg7XHJcbiAgICAgIGhlaWdodDogMTV2aDtcclxuICAgICAgcGFkZGluZzogMDtcclxuICAgIH1cclxuXHJcbiAgICBoMSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogNHZoO1xyXG5cclxuICAgICAgZm9udC1mYW1pbHk6IFwiT3BlbnMgU2Fuc1wiO1xyXG4gICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICB9XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6IDN2aDtcclxuXHJcbiAgICAgIGZvbnQtZmFtaWx5OiBcIk9wZW5zIFNhbnNcIjtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgfVxyXG5cclxuICAgIGgzIHtcclxuICAgICAgZm9udC1zaXplOiAydmg7XHJcbiAgICAgIHBhZGRpbmc6IDJ2aDtcclxuXHJcbiAgICAgIGZvbnQtZmFtaWx5OiBcIk9wZW5zIFNhbnNcIjtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICBmb250LXdlaWdodDogMjAwO1xyXG5cclxuICAgIH1cclxuXHJcblxyXG4gIH1cclxuXHJcbiAgI2JvdHRvbS1wYXJ0IHtcclxuXHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuXHJcbiAgICBpb24tYnV0dG9uIHtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICB3aWR0aDogMjd2aDtcclxuICAgICAgaGVpZ2h0OiA3dmg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwMDBweDtcclxuXHJcbiAgICAgIGZvbnQtZmFtaWx5OiBcIkV4Y2VsbGVuY2UgSW4gTW90aW9uIFNjcmVlblwiO1xyXG4gICAgICBmb250LXNpemU6IDN2aDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5uby1hY2Nlc3Mge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgY29sb3I6ICMwNTEwMzk7XHJcbiAgICBwYWRkaW5nOiAxNnB4O1xyXG4gICAgZm9udC1zaXplOiAyLjV2aDtcclxuXHJcbiAgICBzcGFuIHtcclxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIH1cclxuXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMTZweDtcclxuICAgICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgICBmb250LXNpemU6IDhyZW07XHJcblxyXG4gICAgICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xyXG4gICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICBmb250LXNpemU6IDVyZW07XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var src_app_service_brand_khoros_messenger_brand_khoros_messenger_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/brand-khoros-messenger/brand-khoros-messenger.service */ "./src/app/service/brand-khoros-messenger/brand-khoros-messenger.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/authentication/authentication.service */ "./src/app/service/authentication/authentication.service.ts");
/* harmony import */ var _service_session_session_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../service/session/session.service */ "./src/app/service/session/session.service.ts");








var HomePage = /** @class */ (function () {
    function HomePage(sessionService, authenticationService, router, ngZone, platform, loadingController, toastController, translateService, brandKhorosMessengerService) {
        var _this = this;
        this.sessionService = sessionService;
        this.authenticationService = authenticationService;
        this.router = router;
        this.ngZone = ngZone;
        this.platform = platform;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.translateService = translateService;
        this.brandKhorosMessengerService = brandKhorosMessengerService;
        this.authenticationInProgress = true;
        this.authenticateUser();
        this.platform.resume.subscribe(function () {
            _this.authenticateUser();
        });
    }
    /**
     * Authentifie l'utilisateur courant
     */
    HomePage.prototype.authenticateUser = function () {
        var _this = this;
        this.authenticationInProgress = true;
        this.authenticationService.getAuthenticatedUser().finally(function () {
            _this.authenticationInProgress = false;
        });
    };
    /**
     * Ouvre l'application de messagerie
     */
    HomePage.prototype.openMessengerApp = function () {
        var _this = this;
        this.loadingController.create({
            message: this.translateService.instant('home.opening_app')
        }).then(function (createdLoader) {
            _this.loader = createdLoader;
            _this.loader.present();
            _this.brandKhorosMessengerService.initNativeBrandMessenger()
                .then(function () { return _this.showMessenger(); }, function (error) {
                console.log('Error initializing Brand Messenger sdk: ', error);
            }).finally(function () {
                _this.loader.dismiss();
            });
        });
    };
    /**
     * Affiche la messagerie une fois l'init effectuée
     */
    HomePage.prototype.showMessenger = function () {
        var _this = this;
        this.brandKhorosMessengerService.nativeBrandKhorosMessenger.show('', function (callbackId) {
            _this.loader.dismiss();
            if (callbackId === 'conversation:willDismissViewController:') {
                _this.ngZone.run(function () { return _this.router.navigate(['/home']); });
            }
        }, function (error) {
            _this.loader.dismiss();
            console.log(error);
            // Toast the error message
            _this.toastController.create({
                message: 'Error : ' + error,
                duration: 3000,
                position: 'bottom'
            }).then(function (createdToast) {
                createdToast.present();
            });
        });
    };
    /**
     * Récupère l'utilisateur impersonnifié
     * @return l'utilisateur impersonifié
     */
    HomePage.prototype.getImpersonatedUser = function () {
        return this.sessionService.impersonatedUser;
    };
    /**
     * Récupère l'utilisateur actif
     * @return l'utilisateur actif
     */
    HomePage.prototype.getActiveUser = function () {
        return this.sessionService.getActiveUser();
    };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/pages/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_session_session_service__WEBPACK_IMPORTED_MODULE_7__["SessionService"],
            _service_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"],
            src_app_service_brand_khoros_messenger_brand_khoros_messenger_service__WEBPACK_IMPORTED_MODULE_1__["BrandKhorosMessengerService"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module.js.map