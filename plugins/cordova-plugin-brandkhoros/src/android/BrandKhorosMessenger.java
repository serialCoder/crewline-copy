package com.afklm.brand;

import com.lithium.smm.core.Lithium;
import com.lithium.smm.core.Settings;
import com.lithium.smm.core.LithiumCallback;
import com.lithium.smm.core.User;
import com.lithium.smm.ui.ConversationActivity;
import android.util.Log;
import android.app.Application;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import java.lang.Exception;
import java.util.HashMap;
import java.util.Map;

public class BrandKhorosMessenger extends CordovaPlugin {

    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if ("init".equals(action)) {
            messengerInit(args, callbackContext);
        } else if ("show".equals(action)) {
            show(callbackContext);
        } else if ("destroy".equals(action)) {
            destroy(callbackContext);
        } else {
            return false;
        }

        return true;
    }

    private void messengerInit(JSONArray args, final CallbackContext callbackContext) {
        try {
            JSONArray jsonArray = args;

            String khorosAppId = jsonArray.getString(0);
            String region = jsonArray.getString(1);
            String jwtToken = jsonArray.getString(2);
            String registrationNumber = jsonArray.getString(3);
            String firstName = jsonArray.getString(4);
            String lastName = jsonArray.getString(5);
            String email = jsonArray.getString(6);
            String function = jsonArray.getString(7);
            String division = jsonArray.getString(8);
            String populationType = jsonArray.getString(9);
            String haulType = jsonArray.getString(10);

            final Application application = this.cordova.getActivity().getApplication();
            Settings settings = new Settings(khorosAppId);

            // Defining region for eu (by default is us => nothing needed for us)
            if (region.equals("eu-1")) {
                settings.setRegion("eu-1");
            }

            Lithium.init(application, settings, new LithiumCallback() {
                @Override
                public void run(Response response) {
                    // Handle init result

                    Log.v("LithiumApp 1init status", Lithium.getInitializationStatus().toString());

                    System.out.println("error : " + response.getError());
                    System.out.println("status : " + response.getStatus());
                    System.out.println("data : " + response.getData());

                    if (response.getError() != null) {
                        callbackContext
                                .sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "cannot parse args"));
                        return;
                    }
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));

                    Lithium.login(registrationNumber, jwtToken, new LithiumCallback() {
                        @Override
                        public void run(Response response) {
                            Map<String, Object> userProperties = new HashMap<>();

                            userProperties.put("registrationNumber", registrationNumber);
                            userProperties.put("function", function);
                            userProperties.put("division", division);
                            userProperties.put("populationType", populationType);
                            userProperties.put("haulType", haulType);
                            userProperties.put("email", email);

                            User user = User.getCurrentUser();

                            user.setFirstName(firstName);
                            user.setLastName(lastName);
                            user.setEmail(email);
                            user.addProperties(userProperties);
                        }
                    });
                }
            });
        } catch (Exception exception) {
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, exception.toString()));
        }
    }

    private void show(final CallbackContext callbackContext) {
        System.out.println("trying to show something");
        MyConversationActivity.show(this.cordova.getActivity().getApplicationContext());
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
    }

    private void destroy(final CallbackContext callbackContext) {
        try {
            System.out.println("Brand Messenger destroy...");
            Lithium.destroy();
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
        } catch (Exception exception) {
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, exception.toString()));
        }
    }

    class MyConversationActivity extends ConversationActivity {
        public void onStop() {
            System.out.println(("Brand messenger stopping"));
            super.onStop();
        }
    }
}
