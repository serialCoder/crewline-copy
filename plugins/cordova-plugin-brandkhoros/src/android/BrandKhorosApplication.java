package com.afklm.brand;

import android.app.Application;
import com.lithium.smm.core.Lithium;
import com.lithium.smm.core.LithiumCallback;
import com.lithium.smm.core.Settings;

import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;

public class BrandKhorosApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Lithium.init(this);
    }
}
