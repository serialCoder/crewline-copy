//
//  AFCertificate.h
//  SecMobile
//
//  Created by Laurent Gaches on 14/10/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AFDN : NSObject

@property (nonatomic, strong) NSString *commonName;
@property (nonatomic, strong) NSString *organizationName;
@property (nonatomic, strong) NSString *countryName;
@property (nonatomic, strong) NSString *organizationalUnitName;
@property (nonatomic, strong) NSString *localityName;
@property (nonatomic, strong) NSString *streetAddress;
@property (nonatomic, strong) NSString *stateOrProvinceName;

@end


/**
 
 X.509 certificate encapsulation
 
 @warning This is an experimental implementation. Use this class only if you need to read certificate content in debug mode.
 */
@interface AFCertificate : NSObject


@property (nonatomic, readonly) NSNumber *version;

@property (nonatomic, readonly) AFDN *subject;
@property (nonatomic, readonly) AFDN *issuer;

@property (nonatomic, readonly) NSDate *notBefore;
@property (nonatomic, readonly) NSDate *notAfter;

@property (nonatomic, readonly) NSString *serial;


/**
 Init with AFCertificate with a SecCertificateRef
 
    SecCertificateRef certificate;
    SecIdentityCopyCertificate(identity, &certificate);
    NSData *data = (__bridge_transfer NSData *) SecCertificateCopyData(certificate);
 
    AFCertificate *certificate = [[AFCertificate alloc] initWithData:data];

 
 */
- (instancetype)initWithData:(NSData *)data;


@end
