//
//  AFIdentityManager.h
//  SecMobile
//
//  Created by lehmann on 24/09/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class AFSecConfiguration;

typedef NS_ENUM(NSInteger, AFSecMobileEnv) {
    AF_ENV_DEV,
    AF_ENV_RCT,
    AF_ENV_PRD
};

typedef NS_ENUM(NSInteger, AFSecMobileDuration) {
    AF_LONG,
    AF_SHORT
};

typedef NS_ENUM(NSInteger, SecMobileAppGroup) {
    AF_GROUP,
    KL_GROUP
};


/*!
 @typedef AFIdentityManagerCompletionHandler
 
 @param identityRef NULL if error occured.
 @param error error returned
 */
typedef void(^AFIdentityManagerCompletionHandler)(SecIdentityRef identityRef, NSError *error);


/** @const kAFSecAccessGroup The access group name */
extern NSString * const kAFSecAccessGroup;

/*!
 
 AFIdentityManager is the main object.
 It is a singleton to get the identity from the keychain
 */
@interface AFIdentityManager : NSObject

/**---------------------------------------------------------------------------------------
 * @name  Getting Shared Identity Manager
 *  ---------------------------------------------------------------------------------------
 */

/*!
 Returns the shared `AFIdentityManager` instance.
 
 @return The shared `AFIdentityManager` instance.
 */
+ (AFIdentityManager *)sharedInstance;



/**---------------------------------------------------------------------------------------
 * @name Getting Identity Info
 *  ---------------------------------------------------------------------------------------
 */

-(void)setSecMobileAppGroup:(SecMobileAppGroup)secMobileAppGroup;

@property (nonatomic) AFSecMobileEnv secMobileEnv;

@property (nonatomic) AFSecMobileDuration secMobileDuration;

@property (strong, nonatomic) AFSecConfiguration *secConfiguration;

/** The label to retrieve authentication identity from the keychain */
@property (nonatomic, readonly) NSString * secIdentityKeyChainAuthLabel;

/** 

 @return if identity is present 
 */
- (BOOL)isIdentityPresent;

/**---------------------------------------------------------------------------------------
 * @name Getting Identity
 *  ---------------------------------------------------------------------------------------
 */

/**
 Get the identity from the Keychain and call secmobil if needed
 Asynchronously calls a completion callback with identity and error
 
 Use this method if you want customize user interface.
 
 @param user the user name
 @param password the pin code + the token id
 @param completion the completion handler to call when IdentityRef is loaded.
 */
- (void)getIdentityWithUser:(NSString *)user password:(NSString *)password completion:(AFIdentityManagerCompletionHandler)completion;

/**
 Get the identity from the Keychain and call secmobil if needed
 Asynchronously calls a completion callback with identity and error
 
 Use this method if you want customize user interface.
 
 @param user the user name
 @param password the pin code + the token id
 @param duration of the certificat : AF_LONG(default)
 @param completion the completion handler to call when IdentityRef is loaded.
 */
- (void)getIdentityWithUser:(NSString *)user password:(NSString *)password duration:(AFSecMobileDuration)duration completion:(AFIdentityManagerCompletionHandler)completion;


/**
 get the identity from the keychain and call secmobil if needed
 Asynchronously calls a completion callback with identity and error
 If user and password are required an UIAlertView is showed.
 
 @param fromViewController the ViewController presenting the login popup.
 @param completion the completion handler to call when IdentityRef is loaded.
 @param duration of the certificat : AF_LONG(default)
 */
- (void)getIdentityFromViewController:(UIViewController *)fromViewController duration:(AFSecMobileDuration)duration completion:(AFIdentityManagerCompletionHandler)completion;

/**
 get the identity from the keychain and call secmobil if needed
 Asynchronously calls a completion callback with identity and error
 If user and password are required an UIAlertView is showed.
 
 @param fromViewController the ViewController presenting the login popup.
 @param completion the completion handler to call when IdentityRef is loaded.
 */
- (void)getIdentityFromViewController:(UIViewController *)fromViewController completion:(AFIdentityManagerCompletionHandler)completion;

/**
 get the identity from the keychain 
 
 @return the identity or NULL
 */
- (SecIdentityRef)getIdentity;


/**---------------------------------------------------------------------------------------
 * @name Removing Identity
 *  ---------------------------------------------------------------------------------------
 */


/** 
 Remove the identity from the Keychain
 */
- (BOOL)removeIdentity:(NSError **)error;




@end
