//
//  AFCrypto.h
//  SecMobile
//
//  Created by Laurent Gaches on 09/08/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFCrypto : NSObject

+(AFCrypto *)sharedManager;


- (BOOL)generateAsymmetricKeyPair:(NSError **)error;
- (NSData *)encryptWithPublicKey:(NSData *)data;
- (NSData *)decryptWithPrivateKey:(NSData *)dataToDecrypt;



//- (NSData *)decryptData:(NSData *)dataToDecrypt withSymmetricKey:(NSData *)symmetricKey;
//- (NSData *)encryptData:(NSData *)dataToDecrypt withSymmetricKey:(NSData *)symmetricKey;


@end
