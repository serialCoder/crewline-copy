//
//  AFCertificateURLSessionDelegate.h
//  SecMobile
//
//  Created by lehmann on 27/09/13.
//  Copyright (c) 2013 Air France. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 The AFCertificateURLSessionDelegate defines the method didReceiveChallenge that a delegate of an NSURLSession object should implement to handle mutual authentication
 */
@interface AFCertificateURLSessionDelegate : NSObject <NSURLSessionDelegate>

@end
