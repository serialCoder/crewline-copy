// using error to see if this shows up in AB
console.error("Running hook to add iOS Keychain Sharing entitlements");

var xcode = require('xcode'),
    fs = require('fs'),
    path = require('path'),
    plist = require('plist'),
    util = require('util');

module.exports = function (context) {
  var Q = context.requireCordovaModule('q');
  var deferral = new Q.defer();
console.log("***********************************************************");
  console.log(context.opts.plugin.pluginInfo);
  console.log("***********************************************************");

  if (context.opts.cordova.platforms.indexOf('ios') < 0) {
    throw new Error('This plugin expects the ios platform to exist.');
  }

  var iosFolder = context.opts.cordova.project ? context.opts.cordova.project.root : path.join(context.opts.projectRoot, 'platforms/ios/');
  console.log("iosFolder: " + iosFolder);

  fs.readdir(iosFolder, function (err, data) {

    if (err) {
      throw err;
    }

    var projFolder;
    var projName;

    // Find the project folder by looking for *.xcodeproj
    if (data && data.length) {
      data.forEach(function (folder) {
        if (folder.match(/\.xcodeproj$/)) {
          projFolder = path.join(iosFolder, folder);
          projName = path.basename(folder, '.xcodeproj');
        }
      });
    }

    if (!projFolder || !projName) {
      throw new Error("Could not find an .xcodeproj folder in: " + iosFolder);
    }

    var projectPlistPath = path.join(iosFolder, projName, util.format('%s-Info.plist', projName));
    var projectPlistJson = plist.parse(fs.readFileSync(projectPlistPath, 'utf8'));
    var secMobilAppGroup = projectPlistJson.secMobilAppGroup;

    var appGroup

    console.log("secMobilAppGroup passed in as variable: " + secMobilAppGroup);

    if (secMobilAppGroup == "KL_GROUP") {
      appGroup = "L2UZ78ZJAN.com.klm.mobile.secmobil"
    } else {
      appGroup = "6675WSKG6U.com.airfrance.mobile.inhouse.secmobil"
    }



    let fileArray = ['Entitlements-Release.plist', 'Entitlements-Debug.plist'];

    for (let fileName of fileArray) {

      var destFile = path.join(iosFolder, projName,fileName);

	     console.log("DestFile :  '" + destFile + "'");

       var entitlementsPlist = plist.parse(fs.readFileSync(destFile, 'utf8'));

       console.log(JSON.stringify(entitlementsPlist));

       var list = entitlementsPlist["keychain-access-groups"];

       if (list !== undefined) {

         console.log(JSON.stringify(list));

      } else {

        entitlementsPlist["keychain-access-groups"] = [appGroup];
        console.log(plist.build(entitlementsPlist));

        fs.writeFileSync(destFile, plist.build(entitlementsPlist));
      }
    }

    deferral.resolve();
    });
    deferral.resolve();
  return deferral.promise;
};
