package com.afklm.secmobil;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;

import com.airfrance.secmobil.IdentityManager;
import com.airfrance.secmobil.SecMobilDuration;
import com.airfrance.secmobil.SecMobilEnvironment;
import android.util.Base64;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Callback;
import okhttp3.Call;

public class CertAuthPlugin extends CordovaPlugin {

    private IdentityManager identityManager;
    private CallbackContext mCallbackContext = null;
    private X509Certificate cacheCertificate ;
    private SSLContext cacheSSLContext = null;
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {

        if ("getCertificate".equals(action)) {
            return getCertificate(args, callbackContext);
        } else if ("revokeCertificate".equals(action)) {
            return revokeCertificate(callbackContext);
        } else if ("hasCertificate".equals(action)) {
            return hasCertificate(args, callbackContext);
        } else if ("getJWTToken".equals(action)) {
            return getJWTToken(args, callbackContext);
        } else if ("callRestService".equals(action)) {
            initIdentityManager();

            if (this.cacheCertificate == null)
            {
                this.cacheCertificate = identityManager.getCertificate() ;
            }

            if (this.cacheCertificate == null) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "secmobil.nocertificate"));
            } else {
                String content = args.getString(0);
                JSONObject json = new JSONObject(content);

                String url = null;
                String method = null;
                String requestData = null;

                try {
                    url = json.getString("url");
                    method = json.getString("method");
                    requestData = json.getString("jsonData");
                } catch (JSONException jse) {

                }

                JSONObject headers = new JSONObject();
                try {
                    headers = json.getJSONObject("httpHeaders");
                } catch (JSONException jse) {

                }

                if (this.cacheSSLContext == null)
                {
                    this.cacheSSLContext = identityManager.getSSLContext();
                }
                // time out hardly set to 30 sec in android
                // OkHttpClient okHttpClient = new
                // OkHttpClient().newBuilder().sslSocketFactory(identityManager.getSSLContext().getSocketFactory()).build();
                OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                        .sslSocketFactory(this.cacheSSLContext.getSocketFactory())
                        .connectTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS).build();

                Request request = null;

                Request.Builder requestBuilder = new Request.Builder().url(url);

                String customContentType = "application/json" ;
                Iterator<String> headerItr = headers.keys();
                while (headerItr.hasNext()) {
                    String key = headerItr.next();
                    requestBuilder.addHeader(key, headers.getString(key));
                    if ("Content-Type".equals(key)) {
                        customContentType = headers.getString(key) ;
                    }
                }

                if ("GET".equalsIgnoreCase(method)) {
                    request = requestBuilder.get().build();
                } else if ("POST".equalsIgnoreCase(method) && requestData != null) {
                    request = requestBuilder.post(RequestBody.create(MediaType.parse(customContentType), requestData))
                            .build();
                } else if ("DELETE".equalsIgnoreCase(method)) {
                    if (requestData == null) {
                        request = requestBuilder.delete().build();
                    } else {
                        request = requestBuilder
                                .delete(RequestBody.create(MediaType.parse(customContentType), requestData)).build();
                    }
                } else if ("PUT".equalsIgnoreCase(method) && requestData != null) {
                    request = requestBuilder.put(RequestBody.create(MediaType.parse(customContentType), requestData))
                            .build();
                } else if ("PATCH".equalsIgnoreCase(method) && requestData != null) {
                    request = requestBuilder.patch(RequestBody.create(MediaType.parse(customContentType), requestData))
                            .build();

                }

                if (request != null) {
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            callbackContext.sendPluginResult(
                                    new PluginResult(PluginResult.Status.ERROR, e.getLocalizedMessage()));
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            callbackContext.sendPluginResult(
                                    new PluginResult(PluginResult.Status.OK, response.body().string()));
                        }
                    });
                } else {
                    PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, "malformed request");
                    callbackContext.sendPluginResult(pluginResult);
                    return true;
                }
            }

            return true;

        } else if ("initPlugin".equals(action)) {
            return initPlugin(args, callbackContext);
        }

        return true;
    }

    private void initIdentityManager() {
        if (identityManager == null) {
            Context context = cordova.getActivity().getApplicationContext();
            identityManager = new IdentityManager(context);
        }
    }







    private boolean getCertificate(JSONArray args, final CallbackContext callbackContext) throws JSONException {
        initIdentityManager();

        String content = args.getString(0);
        JSONObject json = new JSONObject(content);

        String userId = json.getString("userId");
        String password = json.getString("strongPassword");

        X509Certificate certificate = identityManager.getCertificate();
        if (certificate == null) {

            mCallbackContext = callbackContext;
            LoginTask loginTask = new LoginTask(userId, password);
            loginTask.execute((Void) null);

            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            return true;

        } else {
            this.cacheCertificate = certificate;
            PluginResult pluginResult;
            if (certificate != null) {
                pluginResult = new PluginResult(PluginResult.Status.OK, "secmobil.certificate.loaded");
            } else {
                pluginResult = new PluginResult(PluginResult.Status.ERROR, "secmobil.incorrect.credentials");
            }

            callbackContext.sendPluginResult(pluginResult);

            return true;
        }
    }

    private boolean hasCertificate(JSONArray args, final CallbackContext callbackContext) throws JSONException {
        initIdentityManager();
       
        PluginResult pluginResult;
       
        if (this.cacheCertificate == null) {
            this.cacheCertificate = identityManager.getCertificate();
        }

        if (this.cacheCertificate != null) {
            pluginResult = new PluginResult(PluginResult.Status.OK, "secmobil.certificate.present");
        } else {
            pluginResult = new PluginResult(PluginResult.Status.ERROR, "secmobil.certificate.nocertificate");
        }

        callbackContext.sendPluginResult(pluginResult);

        return true;
    }

    private boolean revokeCertificate(final CallbackContext callbackContext) {
        initIdentityManager();
        identityManager.removeCertificate();
        this.cacheCertificate = null;
        this.cacheSSLContext = null;
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "secmobil.certificate.removed"));
        return true;
    }

    private boolean initPlugin(JSONArray args, final CallbackContext callbackContext) throws JSONException {
        String env = args.getString(0);
        Context context = cordova.getActivity().getApplicationContext();

        switch (env) {
        case "dev":
            identityManager = new IdentityManager(context, SecMobilEnvironment.AF_ENV_DEV, SecMobilDuration.AF_LONG);
            break;
        case "rct":
            identityManager = new IdentityManager(context, SecMobilEnvironment.AF_ENV_RCT, SecMobilDuration.AF_LONG);
            break;
        case "prd":
            identityManager = new IdentityManager(context, SecMobilEnvironment.AF_ENV_PRD, SecMobilDuration.AF_LONG);
            break;
        default:
            callbackContext.error("Illegal environment");
            return true;
        }

        callbackContext.success();

        return true;
    }

    // Transcode certificate to JwtToken
    private String createJwtToken(X509Certificate certificate, String clientAppId, String url) {

        String jwtToken = null;

        if (certificate != null) {
            try {

                // Get base 64 string representation of certificate compatible with http
                // requests (has to remove equals chars)
                String base64Cert = Base64.encodeToString(certificate.getEncoded(), Base64.URL_SAFE | Base64.NO_WRAP)
                        .replaceAll("=", "");

                Log.d(CertAuthPlugin.class.getSimpleName(), "Base 64 cert : " + base64Cert);

                // Manually extract the subject CN from certificate
                String subject = certificate.getSubjectDN().getName();
                int index = subject.indexOf("=");
                subject = subject.substring(index + 1);
                index = subject.indexOf(",");
                subject = subject.substring(0, index);

                if (url == null || "".equals(url)) {
                    url = "https://www.klm.com/generic/oauth-b2e";
                }

                // Build header and payload
                JoseHeader joseHeader = new JoseHeader(Arrays.asList(base64Cert), "JWT", "RS256");
                Payload payload = new Payload(clientAppId, subject, url, System.currentTimeMillis() + (500 * 60),
                        System.currentTimeMillis(), "INFLIGHT");

                // Build json representation of header and payload
                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                String joseHeaderJson = gson.toJson(joseHeader, JoseHeader.class);
                String payloadJson = gson.toJson(payload, Payload.class);

                Log.d(CertAuthPlugin.class.getSimpleName(), joseHeaderJson);
                Log.d(CertAuthPlugin.class.getSimpleName(), payloadJson);

                // Generate base 64 string representation of both header and payload compatible
                // with http requests (has to remove equals char)
                String joseHeaderBase64 = Base64.encodeToString(joseHeaderJson.getBytes(StandardCharsets.UTF_8),
                        Base64.URL_SAFE | Base64.NO_WRAP).replaceAll("=", "");
                String payloadBase64 = Base64
                        .encodeToString(payloadJson.getBytes(StandardCharsets.UTF_8), Base64.URL_SAFE | Base64.NO_WRAP)
                        .replaceAll("=", "");

                // Combine header and paylod to build first part of jwt
                String headerAndPayloadBase64 = joseHeaderBase64 + "." + payloadBase64;

                // Obtain private key needed to sign combined header and payload
                KeyStore keyStore = identityManager.getKeyStore();
                String alias = keyStore.getCertificateAlias(certificate);
                PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias, null);

                // Sign header and payload with private key to build second part of jwt
                Signature signature = Signature.getInstance("SHA256withRSA");
                signature.initSign(privateKey);
                signature.update(headerAndPayloadBase64.getBytes(StandardCharsets.UTF_8));
                byte[] headerAndPayloadSignedBytes = signature.sign();

                // Generate base 64 string representation of signed header and payload
                // compatible with http requests (has to remove equals char)
                String headerAndPayloadSignedBase64 = Base64
                        .encodeToString(headerAndPayloadSignedBytes, Base64.URL_SAFE | Base64.NO_WRAP)
                        .replaceAll("=", "");

                // Combine first part and signed part to obtain final jwt token
                jwtToken = headerAndPayloadBase64 + "." + headerAndPayloadSignedBase64;

                Log.d(CertAuthPlugin.class.getSimpleName(), "Jwttoken built : " + jwtToken);

            } catch (Exception e) {
                Log.e(CertAuthPlugin.class.getSimpleName(), "Got error transcoding certificate : " + e.getMessage());
            }
        }

        return jwtToken;
    }

    private boolean getJWTToken(JSONArray args, final CallbackContext callbackContext) throws JSONException {

        initIdentityManager();
        X509Certificate certificate = identityManager.getCertificate();
        PluginResult pluginResult;
        String content = args.getString(0);
        JSONObject json = new JSONObject(content);

        String clientAppId = json.getString("clientAppId");
        String urlToken = json.getString("urlToken");

        if (certificate != null) {
            String tokenJWT = createJwtToken(certificate, clientAppId, urlToken);
            if (tokenJWT != null) {
                pluginResult = new PluginResult(PluginResult.Status.OK, tokenJWT);
            } else {
                pluginResult = new PluginResult(PluginResult.Status.ERROR, "secmobil.certificate.invalidjwttoken");

            }

        } else {
            pluginResult = new PluginResult(PluginResult.Status.ERROR, "secmobil.certificate.nocertificate");
        }

        callbackContext.sendPluginResult(pluginResult);

        return true;
    }

    public void onTaskDone(Boolean success) {

        PluginResult pluginResult;

        if (success) {
            pluginResult = new PluginResult(PluginResult.Status.OK, "secmobil.certificate.loaded");
        } else {
            pluginResult = new PluginResult(PluginResult.Status.ERROR, "secmobil.incorrect.credentials");
        }

        pluginResult.setKeepCallback(false);
        mCallbackContext.sendPluginResult(pluginResult);
    }

    private class LoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        LoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            X509Certificate certificate = identityManager.getCertificate(mEmail, mPassword);
            return certificate != null;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            onTaskDone(success);
        }

        @Override
        protected void onCancelled() {

        }
    }

    private class JoseHeader {

        public List<String> x5c;
        public String type;
        public String alg;

        public JoseHeader(List<String> x5c, String type, String alg) {
            this.x5c = x5c;
            this.type = type;
            this.alg = alg;
        }
    }

    private class Payload {

        public String iss;
        public String sub;
        public String aud;
        public long exp;
        public long iat;
        public String rolePatterns;

        public Payload(String iss, String sub, String aud, long exp, long iat, String rolePatterns) {
            this.iss = iss;
            this.sub = sub;
            this.aud = aud;
            this.exp = exp;
            this.iat = iat;
            this.rolePatterns = rolePatterns;
        }
    }

    private class TokenResponse {

        public String access_token;
        public String expires_in;
        public String token_type;
        public String scope;

    }

}

