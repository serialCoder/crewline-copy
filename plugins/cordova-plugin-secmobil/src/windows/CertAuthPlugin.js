var secMobilUrlDev = "https://secmobildev.airfrance.fr/secmobilWeb/services/authentication{0}Certificate";
var secMobilUrlRct = "https://secmobilrct.airfrance.fr/secmobilWeb/services/authentication{0}Certificate";
var secMobilUrlPrd = "https://secmobil.airfrance.fr/secmobilWeb/services/authentication{0}Certificate";

var currentSecMobilUrl = secMobilUrlPrd;
var sharedCertificatFolderName = "SecMobil";
var sharedCertificatFileName = "AFsecmobil.p12";
var sharedDeviceIdFileName = "sharedDeviceId.txt";
var secMobilCertificatName = "Air France SecMobil Certificat";

module.exports = {
    initPlugin: function (successCallback, errorCallback, args) {
        var env = args[0];

        switch (env) {
            case "dev":
                currentSecMobilUrl = secMobilUrlDev;
                break;
            case "rct":
                currentSecMobilUrl = secMobilUrlRct;
                break;
            default:
                currentSecMobilUrl = secMobilUrlPrd;
                break;
        }

        successCallback();
    },
    getCertificate: function (successCallback, errorCallback, args) {
        var params = args[0];

        getCertificate(params).then(() => { successCallback(); }, errorCallback);
    },
    hasCertificate: function (successCallback, errorCallback, args) {
        retrieveAppCertificate().then(() => { successCallback(); }, function (reason) {
            installSharedCertificat().then(retrieveAppCertificate, errorCallback).then(() => { successCallback(); }, errorCallback);
        });
    },
    revokeCertificate: function (successCallback, errorCallback, args) {
        deleteCertificate().then(deleteSharedCertificat, errorCallback).then(successCallback, errorCallback);
    },
    callRestService: function (successCallback, errorCallback, args) {
        var params = args[0];

        sendHttpRequest(params).then(successCallback, errorCallback);
    }
};

function sendHttpRequest(params) {
    return new WinJS.Promise(function (completeDispatch, errorDispatch) {
        retrieveAppCertificate().then(function (certificat) {
            var filter = new Windows.Web.Http.Filters.HttpBaseProtocolFilter();
            filter.clientCertificate = certificat;

            var method = getHttpMethod(params.method);
            var uri = new Windows.Foundation.Uri(params.url);
            var request = new Windows.Web.Http.HttpRequestMessage(method, uri);

            request.headers.insert("Cookie", "SMCHALLENGE=YES;pageAuthMixte=authrobuste");
            request.headers.insert("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko");

            var contentType = "application/json";

            if (!params.httpHeaders == false) {
                for (var header in params.httpHeaders) {
                    if (header == "Content-Type")
                        contentType = params.httpHeaders[header]
                    else
                        request.headers.insert(header, params.httpHeaders[header]);
                }
            }

            request.content = new Windows.Web.Http.HttpStringContent(params.jsonData, Windows.Storage.Streams.UnicodeEncoding.utf8, contentType);

            var client = new Windows.Web.Http.HttpClient(filter);
            client.defaultRequestHeaders
                .accept
                .parseAdd("*/*");

            client.sendRequestAsync(request).then(function (response) {
                if (response != null && response.statusCode === Windows.Web.Http.HttpStatusCode.ok) {
                    response.content.readAsStringAsync().then(completeDispatch, errorDispatch);
                }
                else {
                    errorDispatch("Impossible de récupèrer le certificat");
                }
            }, function (reason) {
                errorDispatch(reason);
            });
        }, errorDispatch);
    });
}

function getHttpMethod(method) {
    switch (method.toLowerCase()) {
        case "get":
            return Windows.Web.Http.HttpMethod.get;
        case "post":
            return Windows.Web.Http.HttpMethod.post;
        case "put":
            return Windows.Web.Http.HttpMethod.put;
        case "delete":
            return Windows.Web.Http.HttpMethod.delete;    
        case "patch":
            return Windows.Web.Http.HttpMethod.patch;

    }
}

function deleteCertificate() {
    return new WinJS.Promise(function (completeDispatch, errorDispatch) {
        retrieveAppCertificate().then(function (certificate) {
            var store = Windows.Security.Cryptography.Certificates.CertificateStores.getStoreByName("MY");

            if (store != null) {
                store.delete(certificate);
                completeDispatch();
            }
            else {
                errorDispatch("Impossible de supprimer le certificat");
            }
        }, function (reason) {
            errorDispatch(reason);
        });
    });
}

function deleteSharedCertificat() {
    return new WinJS.Promise(function (completeDispatch, errorDispatch) {
        var folder = Windows.Storage.ApplicationData.current.getPublisherCacheFolder(sharedCertificatFolderName);
        folder.getFileAsync(sharedCertificatFileName).then(function (file) {
            file.deleteAsync().then(completeDispatch, errorDispatch);
        }, function (reason) {
            errorDispatch(reason);
        });
    });
}

function getCertificate(params) {
    return new WinJS.Promise(function (completeDispatch, errorDispatch) {
        retrieveAppCertificate().then(completeDispatch, function (reason) {
            installSharedCertificat().then(retrieveAppCertificate).then(completeDispatch, function (reason) {
                downloadNewCertificat(params).then(installSharedCertificat).then(retrieveAppCertificate).then(completeDispatch, errorDispatch);
            });
        });
    });
}

function retrieveAppCertificate() {
    return new WinJS.Promise(function (completeDispatch, errorDispatch) {
        var certificateQuery = new Windows.Security.Cryptography.Certificates.CertificateQuery();
        certificateQuery.friendlyName = secMobilCertificatName;

        Windows.Security.Cryptography.Certificates.CertificateStores.findAllAsync(certificateQuery).then(function (certificates) {
            if (certificates.length > 0) {
                var certificat = certificates[0];
                var now = new Date();

                if (certificat.validTo > now) {
                    completeDispatch(certificates[0]);
                }
                else {
                    errorDispatch("Le certificat a expiré");
                }
            }
            else
                errorDispatch("Le certificat n'est pas installé");
        });
    });
}

function installSharedCertificat() {
    return new WinJS.Promise(function (completeDispatch, errorDispatch) {
        var folder = Windows.Storage.ApplicationData.current.getPublisherCacheFolder(sharedCertificatFolderName);
        folder.getFileAsync(sharedCertificatFileName).then(function (file) {

            Windows.Storage.FileIO.readBufferAsync(file).then(function (certificateBuffer) {

                var encodedCertificate = Windows.Security.Cryptography.CryptographicBuffer.encodeToBase64String(certificateBuffer);

                getPassword().then(function (password) {
                    Windows.Security.Cryptography.Certificates.CertificateEnrollmentManager.importPfxDataAsync(encodedCertificate, password, Windows.Security.Cryptography.Certificates.ExportOption.notExportable, Windows.Security.Cryptography.Certificates.KeyProtectionLevel.noConsent, Windows.Security.Cryptography.Certificates.InstallOptions.deleteExpired, secMobilCertificatName).then(function () {
                        completeDispatch();
                    }, function (reason) {
                        errorDispatch(reason);
                    });
                }, function (reason) {
                    errorDispatch(reason);
                });
            }, function (reason) {
                errorDispatch(reason);
            });
        }, function (reason) {
            errorDispatch(reason);
        });
    });
}

function downloadNewCertificat(params) {
    return new WinJS.Promise(function (completeDispatch, errorDispatch) {
        // var m_BPF = new Windows.Web.Http.HttpBaseProtocolFilter();
        var m_BPF = new Windows.Web.Http.Filters.HttpBaseProtocolFilter();
		m_BPF.AllowUI = false;
		var client = new Windows.Web.Http.HttpClient(m_BPF);
        client.defaultRequestHeaders
            .accept
            .parseAdd("*/*");

        var uri = new Windows.Foundation.Uri(getSecMobilUri(params));
        var request = new Windows.Web.Http.HttpRequestMessage(Windows.Web.Http.HttpMethod.post, uri);

        request.headers.insert("Cookie", "SMCHALLENGE=YES;pageAuthMixte=authrobuste");
        request.headers.insert("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko");
        request.headers.insert("Authorization", "Basic " + btoa(params.userId + ":" + params.strongPassword));

        var content = {
            model: getModel()
        };

        getUid().then(function (uid) {
            content.Uid = uid;

            getPassword().then(function (password) {
                content.password = password;

                request.content = new Windows.Web.Http.HttpStringContent(formatParams(content), Windows.Storage.Streams.UnicodeEncoding.utf8, "application/x-www-form-urlencoded");

                client.sendRequestAsync(request).then(function (response) {
                    if (response != null && response.statusCode === Windows.Web.Http.HttpStatusCode.ok) {
                        var folder = Windows.Storage.ApplicationData.current.getPublisherCacheFolder(sharedCertificatFolderName);
                        folder.createFileAsync(sharedCertificatFileName, Windows.Storage.CreationCollisionOption.replaceExisting).then(function (file) {
                            file.openAsync(Windows.Storage.FileAccessMode.readWrite).then(function (stream) {
                                response.content.writeToStreamAsync(stream).then(function () {
                                    stream.close();
                                    completeDispatch();
                                }, function (reason) {
                                    stream.close();
                                    errorDispatch(reason);
                                });
                            }, function (reason) {
                                errorDispatch(reason);
                            });
                        }, function (reason) {
                            errorDispatch(reason);
                        });
                    }
                    else {
                        errorDispatch("Impossible de récupèrer le certificat");
                    }
                }, function (reason) {
                    errorDispatch(reason);
                });
            }, function (reason) {
                errorDispatch(reason);
            });
        }, function (reason) {
            errorDispatch(reason);
        });
    });
}

function formatParams(data) {
    var out = new Array();

    for (key in data) {
        out.push(key + '=' + encodeURIComponent(data[key]));
    }

    return out.join('&');
}

function getModel() {
    var deviceInfos = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();

    var model = deviceInfos.systemSku;

    if (!model)
        model = deviceInfos.systemManufacturer + " " + deviceInfos.systemProductName;

    return model;
}

function getUid() {
    return new WinJS.Promise(function (completeDispatch, errorDispatch) {
        var folder = Windows.Storage.ApplicationData.current.getPublisherCacheFolder(sharedCertificatFolderName);
        folder.getFileAsync(sharedDeviceIdFileName).then(function (file) {

            Windows.Storage.FileIO.readTextAsync(file).then(function (uid) {
                completeDispatch(uid);
            }, function (reason) {
                errorDispatch(reason);
            });
        }, function (reason) {
            folder.createFileAsync(sharedCertificatFileName, Windows.Storage.CreationCollisionOption.replaceExisting).then(function (file) {

                var token = Windows.System.Profile.HardwareIdentification.getPackageSpecificToken(null);
                var uid = Windows.Security.Cryptography.CryptographicBuffer.encodeToBase64String(token.id);

                uid = uid.replace(/\W/g, '');

                Windows.Storage.FileIO.writeTextAsync(file, uid).then(function () {
                    completeDispatch(uid);
                }, function (reason) {
                    errorDispatch(reason);
                });
            }, function (reason) {
                errorDispatch(reason);
            });
        });
    });
}

function getPassword() {
    return new WinJS.Promise(function (completeDispatch, errorDispatch) {
        getUid().then(function (uid) {
            var model = getModel();

            var inputpwd = uid + model;
            var inputBin = Windows.Security.Cryptography.CryptographicBuffer.convertStringToBinary(inputpwd, Windows.Security.Cryptography.BinaryStringEncoding.utf8)

            var hasher = Windows.Security.Cryptography.Core.HashAlgorithmProvider.openAlgorithm("SHA256");
            var hashed = hasher.hashData(inputBin);

            var result = Windows.Security.Cryptography.CryptographicBuffer.encodeToBase64String(hashed);

            result = result.replace(/\W/g, '');

            completeDispatch(result);
        }, errorDispatch);
    });
}

function getSecMobilUri(params) {
    var result;

    if (params.duration.toLowerCase() === "long") {
        result = currentSecMobilUrl.replace("{0}", "");
    } else {
        result = currentSecMobilUrl.replace("{0}", "Short");
    }

    return result;
}

require("cordova/exec/proxy").add("CertAuthPlugin", module.exports);
