![iOS ready](https://img.shields.io/badge/iOS-ready-green.svg)
![android soon](https://img.shields.io/badge/Android-ready-green.svg)
![Windows soon](https://img.shields.io/badge/Windows-soon-orange.svg)


# Secmobil cordova plugin
AF-KLM cordova plugin for certificate based authentication.

# Supported Platforms

The plugin is available for iOS and android. Windows versions will be available soon.

# Files tree
  ```
www/plugin.js : contains the plugin common API

scr/[plateform]/* : native source files to implement the common API on the platform
  ```


# Project prerequisite

## Enabled KeyChain sharing

* **gulp/cordova (FrontEnd forge)**<br>

  In your gulpfile.js / android task, add : !!! not always working..

  ```
  	gulp.task("cordova-android-update-androidmanifest-xml", "Modify android .cordova generated folder to match specific requirements.", [], function() {
    return gulp.src("./.cordova/platforms/android/AndroidManifest.xml")
      .pipe($.injectString.after("<manifest", " android:sharedUserId=\"com.airfrance.app.shared\" "))
      .pipe(gulp.dest("./.cordova/platforms/android"));
  	});

  ```
  
for android project, you can add in your config.xml to persist certificat
  ```
<platform name="android”>[…]<preference name="android-manifest/@android:sharedUserId" value="com.airfrance.app.shared" />
  ```                                                                                                                 


# Install cordova-plugin-secmobil plugin. Multiples solutions:

* **with gulp (FrontEnd forge)**<br>

  In your gulpfile.js / ios task, add :
  ```
  .pipe($.cordovaPlugin('cordova-plugin-secmobil'))
  ```

* **pure cordova**<br>

  ```
  npm config set registry http://nexus.airfrance.fr/nexus/repository/npm/
  ```
  ```
  cordova plugin add cordova-plugin-secmobil --variable APP_GROUP='KL_GROUP'
  // or
  cordova plugin add cordova-plugin-secmobil --variable APP_GROUP='AF_GROUP'
  ```
* **MS Visual Studio**</br>

Because Visual Studio can handle Cordova project it is easy to add Cordova plugin to your project.

To do it, you juste have to open the config.xml file and go to the Plugins tabs.

Here select Custom and browse to the place were the plugin is stored.

Then click Add button and wait the end of the installation process.

<img src="readmeImg/VisualStudioAddPlugin.png">


# How to use cordova-plugin-secmobil plugin

The Cordova Secmobile plugin offer a common API for iOS, Android and Windows platforms :

* **initialize**<br>
```
initSecmobilHttp : use this function to initialize your Secmobile environment.
  - Parameters : - environment (dev, rct or prd. Default is prd)
```

* **Set app Group**<br>
```
secMobilSetAppGroup : use this function to initialize your app Group for SSO.
  - Parameters : - appGroup (AF_GROUP, KL_GROUP)
```



* **Get Certificate**<br>
```
secMobilGetCertificate : use this function to get a certificate for a user.
  - Parameters : - userID : user ID
                 - strongPassword : User Password
                 - duration : certificate durartion ("LONG" or "SHORT")
  - Returns :  - CDVCommandStatus_OK
                   - "secmobil.certificate.loaded",  everything went ok
               - CDVCommandStatus_ERROR
                   - "secmobil.incorrect.credentials", your credentials are wrong
                   - "secmobil.unknown.error", otherwise

```

* **Revoke Certificate**<br>

```
secMobilRevokeCertificate : use this function to revoke thd certificate of a user.
  - Parameters : none
  - Returns :  - CDVCommandStatus_OK
                   - "secmobil.certificate.removed",  certificate has been removed

```

* **Is a certificate present on device ?**<br>

```
secMobilHasCertificate : use this function to test if a certifiacte is present on device.
  - Parameters : none
  - Returns :  - CDVCommandStatus_OK
                   - "secmobil.certificate.present",  there is a certificate on device
               - CDVCommandStatus_ERROR
                   - "secmobil.certificate.nocertificate", there is no certificate on device

```

* **Call a REST service using current certificate**<br>

```
secMobilCallRestService : use this function call a rest service.
  - Parameters : - url : service URL
                 - method : GET or POST
                 - requestData : Request body
                 - headersDictionary (optional) : Request headers
  - Returns :  - CDVCommandStatus_OK
                   - "YOUR_DATA",  your data as a string
               - CDVCommandStatus_ERROR
                   - "secmobil.nocertificate", there is no certificate on device
                   - "secmobil.certificate.expired", your certificate has expired
                   - "Network error code : XXX ", any HTTP error

```


* **Get JWT Token for specified app Id**<br>

```
secMobilGetJWTToken : use this function to get jwt token.
  - Parameters : - clientAppId : appId
                 - urlToken : get token from a specified url. default : "https://www.klm.com/generic/oauth-b2e"
  - Returns :  - CDVCommandStatus_OK
                   - "YOUR_JWT_TOKEN",  your data as a string
               - CDVCommandStatus_ERROR
                   - "secmobil.nocertificate", there is no certificate on device
                   - "secmobil.certificate.expired", your certificate has expired
                   - "Network error code : XXX ", any HTTP error
```


* **Get OAuth2 access Token from Web Authority**<br>

```
getOAuth2AccessToken : use this function to get jwt token.
  - Parameters : - clientId : client Id
  - Returns :  - CDVCommandStatus_OK
                  - "YOUR_JWT_TOKEN",  your data as a string
               - CDVCommandStatus_ERROR
                  - "secmobil.nocertificate", No JWT Token available, certificate doesn't exists
```



## How to use this plugin in a IONIC application

- [Confluence documentation](https://confluence.devnet.klm.com/x/EyDMC#id-5.2ManageRestcall&HabileAuth-MobileauthentificationviaSecMobil)
- [Snippet sample in Bitbucket](https://bitbucket.devnet.klm.com/projects/TECCSE/repos/hybrid-snippet-authentication/browse)


# How to maintain and test cordova-plugin-secmobil plugin

* **Testing Sample**<br>

You can use this Cordova SecMobile sample project to test your modifications :
https://stash.eden.klm.com/projects/SECMOBIL/repos/cordova-plugin-secmobil-example/browse


* **Testing Servers**<br>
- RCT : https://secmobil-apirct.airfrance.fr/secmobilTestWeb/user
- PRD : https://secmobil-api.airfrance.fr/secmobilTestWeb/user
