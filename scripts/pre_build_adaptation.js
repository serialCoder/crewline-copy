#!/usr/bin/env node

write('start of the script',true);

var path  = require('path');
var fs = require('fs-extra');
var fs_base = require('fs');
// used for update config.xml file (with appName and appId)
var DOMParser = require('xmldom').DOMParser;
var XMLSerializer = require('xmldom').XMLSerializer;

/* load config file with env, config file, app name and other params  */
var envName = 'default';

var CONFIG = {};
 // check if sourceFile exists
 if (fs_base.existsSync('./adaptation.json')) {
     CONFIG = JSON.parse(fs_base.readFileSync('./adaptation.json', 'utf8'));
 } else {
    write('adaptation.json is not present now search here in ./../');
    CONFIG = JSON.parse(fs_base.readFileSync(path.resolve(__dirname, './../adaptation.json'), 'utf8'));
 }

var env = process.argv.find(function(r) { return r.startsWith ('-target:'); })
if (env != null) {
    envName = env.split(':')[1];
}
write('selected env : '+envName);

var buildNumber = process.argv.find(function(r) { return r.startsWith ('-buildNumber:'); })
if (buildNumber != null) {
    buildNumber = buildNumber.split(':')[1];
} else {
    buildNumber = '';
}

var sourceFile = '';
var appName = '';
var appId = '';
var version = '';
// BEGIN IOS
var provisioningProfile = '';
var developmentTeam = '';
var packageType = '';
// END IOS
var destFile = CONFIG.currentConfigFile;
if (env == null){
    envName = CONFIG.defaultEnvironment ;
}

var configFounded = false;

CONFIG.environments.forEach(function (e) {
    if (e.name === envName ){
        sourceFile =e.configFile;
        appName = e.appName;
        appId = e.appId;
        version = e.version;
        // BEGIN IOS
        if (e.provisioningProfile !== undefined) {
            provisioningProfile = e.provisioningProfile;
            write("provisioning profile from adapation file: " + provisioningProfile);
        }
        if (e.developmentTeam !== undefined) {
            developmentTeam = e.developmentTeam;
            write("development team from adaptation file: " + developmentTeam);
        }
        if (e.packageType !== undefined) {
            packageType = e.packageType;
            write("package type from adaptation file : " + packageType);
        }
        if (e.buildNumber !== undefined && buildNumber == '') {
            buildNumber = e.buildNumber;
            write("package build number from adaptation file : " + buildNumber);
        }
        // END IOS
        configFounded = true ;

        // Save in env_variable the current name of the APP
        process.env.current_app_name = e.appName;
    }
})

if (!configFounded) {
    // serious problem, show warning in console even if not in verbose mode
    write('ERROR: env name '+envName+ " doesn't exists in adaptation.json file",true );
    return -1;
} else {

    write('envName : '+envName);

    // region change Config ressources
    write('Change configuration file');
    write('sourceFile : '+sourceFile);
    write('destFile : '+destFile);
    // check if sourceFile exists
    if (fs_base.existsSync(sourceFile)) {
        fs.copySync(sourceFile, destFile);
    } else {
        write("ERROR:source file doesn't exists " + sourceFile,true);
    }

    // region change AppName and appId in config xml file
    write('Change App name / App id');

    fs.readFile('./config.xml', 'utf-8', function (err,data) 
    {
        var xml = new DOMParser().parseFromString(data);
        var widget = xml.getElementsByTagName('widget')[0];

        if (appId !== '') {
            widget.setAttribute('id', appId);
            write('change configxml AppId attribute to:' + appId);
        }
        if (appName !== '') {
            widget.getElementsByTagName("name")[0].childNodes[0].data= appName;
            write('change configxml app name to:' + appName);
        }
        if (version !== '') {
            widget.setAttribute('version', version);
            write('change configxml version attribute to:' + version);
        }

        // BEGIN IOS
        if (buildNumber !== '') {
            widget.setAttribute('ios-CFBundleVersion', buildNumber);
            write('change configxml ios-CFBundleVersion attribute to:' + buildNumber);
        }
        // END IOS

        // BEGIN Android
        if (buildNumber !== '') {
            widget.setAttribute('android-versionCode', buildNumber);
            write('change configxml android-versionCode attribute to:' + buildNumber);
        }
        // END Android

        // utf8 by default
        write('write new config file');
        fs_base.writeFileSync('./config.xml', new XMLSerializer().serializeToString(xml));

    });

   // BEGIN IOS
    if (fs_base.existsSync('./build.json')) {
        // var file = require (fileName);
        var file = JSON.parse(fs_base.readFileSync('./build.json', 'utf8'));
        // console.log(file);

        if (provisioningProfile !== '') {
            file.ios.release.provisioningProfile = provisioningProfile;
            write('Build.json Provisioning profile modified with value : ' + provisioningProfile);
        }
        if (developmentTeam !== '') {
            file.ios.release.developmentTeam = developmentTeam;
            write('Build.json Development Team modified with value : ' + developmentTeam);
        }
        if (packageType !== '') {
            file.ios.release.packageType = packageType;
            write('Build.json Package Type modified with value : ' + packageType);
        }

        fs_base.writeFile('./build.json', JSON.stringify(file), function (err) {

            if (err) return console.console.log(err);

        });
    }
   // END IOS

}

write('end of pre_build_adaptation.js ##');

function write(elem, force=false) {
    if (force || process.argv.find(function(r) { return r === '-v'; }) != undefined) {
        console.log('pre_build_adaptation.js:'+elem);
    }
}

