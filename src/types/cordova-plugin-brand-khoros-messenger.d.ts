interface BrandKhorosMessenger {
    init(
        integrationAppId: string,
        registrationNumber: string,
        region: string,
        jwt: string,
        firstName: string,
        lastName: string,
        email: string,
        userFunction: string,
        division: string,
        populationType: string,
        haulType: string,
        onSuccess,
        onFail): void;

    show(content, onSuccess: ((callbackId: string) => void), onFail): void;
    destroy(content, onSuccess, onFail): void;
    test(content, onSuccess, onFail): void;
}