interface CertAuthPlugin {
    secMobilGetCertificate(content, onSuccess, onFail): void;

    secMobilRevokeCertificate(content, onSuccess, onFail): void;

    secMobilHasCertificate(content, onSuccess, onFail): void;

    secMobilCheckValidityOfCertificate(content, onSuccess, onFail): void;

    secMobilCallRestService(content, onSuccess, onFail): void;

    initSecmobilHttp(content, onSuccess?, onFail?): void;

    secMobilSetAppGroup(content, onSuccess, onFail): void;

    secMobilGetJWTToken(content, onSuccess, onFail): void;

    secMobilGetOAuth2AccessToken(content, onSuccess, onFail): void;
}
