import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentionsLegalesPage } from './mentions-legales.page';

describe('MentionsLegalesPage', () => {
  let component: MentionsLegalesPage;
  let fixture: ComponentFixture<MentionsLegalesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentionsLegalesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentionsLegalesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
