import {Component} from '@angular/core';
import {MenuController} from '@ionic/angular';

@Component({
  selector: 'app-mentions-legales',
  templateUrl: './mentions-legales.page.html',
  styleUrls: ['./mentions-legales.page.scss'],
})
export class MentionsLegalesPage {
  constructor(public menu: MenuController) { }
}
