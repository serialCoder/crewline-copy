import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MentionsLegalesPage } from './mentions-legales.page';
import { TranslateModule } from '@ngx-translate/core';
import {HeaderModule} from '../../component/header/header.module';

const routes: Routes = [
  {
    path: '',
    component: MentionsLegalesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes),
    HeaderModule
  ],
  declarations: [MentionsLegalesPage]
})
export class MentionsLegalesPageModule {}
