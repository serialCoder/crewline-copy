import {
    BrandKhorosMessengerService
} from 'src/app/service/brand-khoros-messenger/brand-khoros-messenger.service';

import { Component, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { User } from '../../models/user';
import { AuthenticationService } from '../../service/authentication/authentication.service';
import { SessionService } from '../../service/session/session.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private loader: any;

  authenticationInProgress = true;

  constructor(
    private sessionService: SessionService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private ngZone: NgZone,
    private platform: Platform,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private translateService: TranslateService,
    private brandKhorosMessengerService: BrandKhorosMessengerService) {
    this.authenticateUser();

    this.platform.resume.subscribe(() => {
      this.authenticateUser();
    });
  }

  /**
   * Authentifie l'utilisateur courant
   */
  authenticateUser() {
    this.authenticationInProgress = true;
    this.authenticationService.getAuthenticatedUser().finally(() => {
      this.authenticationInProgress = false;
    });
  }

  /**
   * Ouvre l'application de messagerie
   */
  openMessengerApp() {
    this.loadingController.create({
      message: this.translateService.instant('home.opening_app')
    }).then((createdLoader) => {

      this.loader = createdLoader;
      this.loader.present();

      this.brandKhorosMessengerService.initNativeBrandMessenger()
        .then(() => this.showMessenger(),
          (error) => {
            console.log('Error initializing Brand Messenger sdk: ', error);
          }).finally(() => {
            this.loader.dismiss();
          });
    });
  }

  /**
   * Affiche la messagerie une fois l'init effectuée
   */
  private showMessenger() {
    this.brandKhorosMessengerService.nativeBrandKhorosMessenger.show('',
      (callbackId: string) => {

        this.loader.dismiss();
        if (callbackId === 'conversation:willDismissViewController:') {
          this.ngZone.run(() => this.router.navigate(['/home']));
        }

      }, (error) => {
        this.loader.dismiss();
        console.log(error);

        // Toast the error message
        this.toastController.create({
          message: 'Error : ' + error,
          duration: 3000,
          position: 'bottom'
        }).then((createdToast) => {
          createdToast.present();
        });
      });
  }

  /**
   * Récupère l'utilisateur impersonnifié
   * @return l'utilisateur impersonifié
   */
  getImpersonatedUser(): User {
    return this.sessionService.impersonatedUser;
  }

  /**
   * Récupère l'utilisateur actif
   * @return l'utilisateur actif
   */
  getActiveUser(): User {
    return this.sessionService.getActiveUser();
  }

}
