import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConditionsGeneralesPage } from './conditions-generales.page';
import { TranslateModule } from '@ngx-translate/core';
import {HeaderModule} from '../../component/header/header.module';

const routes: Routes = [
  {
    path: '',
    component: ConditionsGeneralesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes),
    HeaderModule
  ],
  declarations: [ConditionsGeneralesPage]
})
export class ConditionsGeneralesPageModule {}
