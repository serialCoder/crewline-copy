import {Component} from '@angular/core';
import {MenuController} from '@ionic/angular';

@Component({
  selector: 'app-conditions-generales',
  templateUrl: './conditions-generales.page.html',
  styleUrls: ['./conditions-generales.page.scss'],
})
export class ConditionsGeneralesPage {
  constructor(public menu: MenuController) { }
}
