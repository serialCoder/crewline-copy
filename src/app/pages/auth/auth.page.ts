import { Component, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { SecMobilService } from '../../service/http/secMobil.service';
import { TokenValidator, UsernameValidator } from '../../validators/Validators';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage {

  public loginForm: FormGroup;
  private user: any;


  constructor(private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public zone: NgZone,
    private secMobilService: SecMobilService,
    private translateService: TranslateService,
    private router: Router,
    private loadingController: LoadingController,
    private toastController: ToastController) {

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, UsernameValidator.isValid])],
      password: ['', Validators.compose([Validators.required, TokenValidator.isValid])],
      checked: [false, Validators.requiredTrue]
    });
  }

  login(): void {

    this.loadingController.create({
      message: 'Connexion en cours...'
    }).then((createdLoader) => {

      createdLoader.present();

      this.user = {
        username: this.loginForm.value.username.toLowerCase(),
        password: this.loginForm.value.password
      };

      this.secMobilService.authenticate(this.user.username, this.user.password)
        .then(() => {
          createdLoader.dismiss();
          this.resetUI();
          this.router.navigate(['/home'], { replaceUrl: true });
        }).catch((error) => {

          // Screen reset
          createdLoader.dismiss();
          this.resetUI();

          // Determine the error message
          let errorMsg = '';

          if (error === 'secmobil.incorrect.credentials') {
            errorMsg = this.translateService.instant('secmobil.authent.error-incorrect-credentials');
          } else if (error === 'secmobil.unknown.error : errSecDefault') {
            errorMsg = this.translateService.instant('secmobil.authent.error-contact-server');
          } else {
            errorMsg = this.translateService.instant('secmobil.authent.error-contact-server');
          }

          // Toast the error message
          this.toastController.create({
            message: errorMsg,
            duration: 3000,
            position: 'bottom'
          }).then((createdToast) => {
            createdToast.present();
          });
        });
    });

  }

  private resetUI() {
    this.loginForm.reset();
  }
}
