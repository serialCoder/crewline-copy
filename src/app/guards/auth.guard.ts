import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Platform } from '@ionic/angular';

import { User } from '../models/user';
import { DeviceService } from '../service/device/device.service';
import { SecMobilService } from '../service/http/secMobil.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    public currentUser: User = null;
    public impersonateUser: string;

    constructor(
        private router: Router,
        private platform: Platform,
        private secMobilService: SecMobilService,
        private deviceService: DeviceService) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.platform.ready()
            .then(() => {
                if (this.deviceService.isBrowser()) {
                    return true;
                } else {
                    return this.secMobilService.isAuthenticated().then(isLoggedIn => {
                        if (!isLoggedIn) {
                            this.router.navigate(['/login']);
                            return false;
                        }
                        return true;
                    });
                }
            }).catch((error) => {
                this.router.navigate(['/login']);
                return false;
            });
    }


}
