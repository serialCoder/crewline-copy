import { Injectable } from '@angular/core';

import { User } from '../../models/user';
import { AuthenticationService } from '../authentication/authentication.service';
import { SessionService } from '../session/session.service';

@Injectable({ providedIn: 'root' })
export class ImpersonateService {

  constructor(
    private sessionService: SessionService,
    private authenticationService: AuthenticationService,
  ) {
  }

  /**
   * Impersonnifie un utilisateur
   * @param registrationNumber le matricule de la personne qu'on souhaite impersonnifier
   * @return une promesse contenant le user connecté
   */
  impersonate(registrationNumber: string): Promise<User> {
    this.sessionService.impersonatedUser = new User(registrationNumber);
    return this.authenticationService.getAuthenticatedUser().then(impersonatedUser => {
      this.sessionService.impersonatedUser = impersonatedUser;
      return impersonatedUser;
    });
  }

  /**
   * Réinitialise l'impersonnification afin de récupérer son identité d'origine
   */
  clearImpersonate() {
    this.sessionService.impersonatedUser = undefined;
  }

}
