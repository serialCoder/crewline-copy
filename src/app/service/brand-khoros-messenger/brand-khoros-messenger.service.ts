import * as CryptoJS from 'crypto-js';

import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

import { Config } from '../../../configuration/environment-variables/config';
import { PluginsService } from '../plugin/plugins.service';
import { SessionService } from '../session/session.service';

@Injectable({
    providedIn: 'root'
})
export class BrandKhorosMessengerService {

    public brandMessengerInitialized = false;


    constructor(private config: Config,
        private pluginService: PluginsService,
        private platform: Platform,
        private sessionService: SessionService) { }

    public get nativeBrandKhorosMessenger() {
        return this.pluginService.brand_khoros_native_messenger;
    }

    public initNativeBrandMessenger(): Promise<void> {
        return new Promise((resolve, reject) => {

            console.log('initNativeLithium');

            if (!this.brandMessengerInitialized) {
                console.log('initNativeLithium...');

                const id = this.platform.is('ios') ? this.config.integrationId : this.config.khorosId;
                const user = this.sessionService.getActiveUser();
                this.nativeBrandKhorosMessenger.init(
                    id,
                    user.registrationNumber,
                    this.config.khorosRegion,
                    this.encodeJWT(),
                    user.firstName,
                    user.lastName,
                    user.email,
                    user.function,
                    user.division,
                    user.populationType,
                    user.haulType,
                    () => {
                        this.brandMessengerInitialized = true;
                        return resolve();
                    },
                    (err: any) => {
                        return reject(new Error(err));
                    }
                );

            } else {
                console.log('initNativeLithium already done');
                return resolve();
            }
        });
    }

    public destroyBrandKhorosMessenger(): Promise<void> {
        return new Promise((resolve, reject) => {
            console.log('destroy Brand khoros Messenger');

            if (this.brandMessengerInitialized) {
                console.log('destroyBrandKhorosMessenger...');
                this.nativeBrandKhorosMessenger.destroy('', () => {
                    this.brandMessengerInitialized = false;
                    resolve();
                },
                    (err: any) => {
                        console.log('error while trying to destroy Brand Messenger: ', err);
                        reject(new Error(err));
                    });
            } else {
                console.log('destroyLithium has not been initialized no need to destroy');
                return resolve();
            }
        });
    }

    private encodeJWT(): string {
        const userId = this.sessionService.getActiveUser().registrationNumber;

        // Build Header
        const header = {
            alg: 'HS256',
            kid: this.config.khorosKeyId,
            typ: 'JWT'
        };

        // Build Data
        const data = {
            userId: userId,
            scope: 'appUser',
            platform: this.platform.is('ios') ? 'ios' : 'android'
        };

        // Encode Header
        const stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
        const encodedHeader = this.base64url(stringifiedHeader);

        // Encode Data
        const stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
        const encodedData = this.base64url(stringifiedData);

        // Build Token from encoded Data
        const token = encodedHeader + '.' + encodedData;

        const secret = this.config.khorosSecret;
        const signature = this.base64url(CryptoJS.HmacSHA256(token, secret));

        return token + '.' + signature;
    }

    private base64url(source) {
        // Encode in classical base64
        let encodedSource = CryptoJS.enc.Base64.stringify(source);

        // Remove padding equal characters
        encodedSource = encodedSource.replace(/=+$/, '');

        // Replace characters according to base64url specifications
        encodedSource = encodedSource.replace(/\+/g, '-');
        encodedSource = encodedSource.replace(/\//g, '_');

        return encodedSource;
    }

}
