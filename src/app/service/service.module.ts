import { NgModule } from '@angular/core';

import { AuthenticationService } from './authentication/authentication.service';
import { DeviceService } from './device/device.service';
import { ImpersonateService } from './impersonate/impersonate.service';
import { SessionService } from './session/session.service';

@NgModule({
    declarations: [
    ],
    imports: [
    ],
    providers: [
        SessionService,
        DeviceService,
        AuthenticationService,
        ImpersonateService
    ]
})
export class ServiceModule { }

