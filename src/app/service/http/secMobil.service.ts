import { Injectable } from '@angular/core';
import { CrewTypeEnum } from 'src/app/enums/crew-type.enum';
import { DivisionEnum } from 'src/app/enums/division.enum';
import { User } from 'src/app/models/user';

import { Config } from '../../../configuration/environment-variables/config';
import { DeviceService } from '../device/device.service';
import { RestRequest } from './rest/rest-request';

declare var window: any;

@Injectable({ providedIn: 'root' })
export class SecMobilService {

    constructor(
        private config: Config,
        private deviceService: DeviceService
    ) {
    }

    public init() {
        if (this.secMobile) {
            this.secMobile.initSecmobilHttp(this.config.secmobileEnv);
            console.log('init plugin with ' + this.config.secmobileEnv + ' env');
            this.secMobile.secMobilSetAppGroup('AF_GROUP');
        }
    }

    get secMobile(): any {
        if (!this.deviceService.isBrowser() && window.cordova.plugins && window.cordova.plugins.CertAuthPlugin) {
            return window.cordova.plugins.CertAuthPlugin;
        } else {
            return null;
        }
    }

    public secMobilRevokeCertificate(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.secMobile) {
                this.secMobile.secMobilRevokeCertificate('',
                    (success) => {
                        resolve(success);
                    },
                    (err) => {
                        console.error('AuthentService:secMobilRevokeCertificate failure : ' + err);
                        reject(err);
                    }
                );
            } else {
                resolve('ok');
            }
        });
    }

    /**
     * Authenticate 'login' user.
     */
    public authenticate(login: string, password: string): Promise<any> {
        const authentParam = {
            duration: 'long',
            userId: login.toLowerCase(),
            strongPassword: password
        };
        return Promise.resolve();
        /*
        return new Promise((resolve, reject) => {
            if (this.secMobile) {
                this.secMobile.secMobilGetCertificate(authentParam,
                    (success) => {
                        resolve(success);
                    },
                    (err) => {
                        console.error('isAuthenticated:authenticate failure : ' + err);
                        reject(err);
                    }
                );
            } else {
                resolve('ok');
            }
        });
        */
    }

    /**
     * Check if certificate is present and valid.
     */
    public isAuthenticated(): Promise<any> {
        return Promise.resolve(true);
        /*  return new Promise((resolve, reject) => {
              if (this.secMobile) {
                  this.secMobile.secMobilHasCertificate('',
                      (success) => {
                          console.log('certificate present');
                          resolve('ok');
                      },
                      (err) => {
  
                          console.log('NO certificate present');
                          console.error('isAuthenticated:isAuthenticated failure : ' + err);
                          reject(err);
                      }
                  );
              } else {
                  resolve('ok');
              }
          }
          );
          */
    }

    /**
     * Make http request
     */
    public call(request: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.handleGetRequest(request);
            this.secMobile.secMobilCallRestService(request,
                (success) => {
                    try {
                        resolve(JSON.parse(success));
                    } catch (error) {
                        console.error('fail : ' + error);
                        console.log(JSON.stringify(success));
                        // en cas d objet json vide, on renvoie null, et ça implique qu'on peut recevoir du back que du json
                        resolve(null);
                    }
                },
                (err) => {
                    console.error('secmobile call failure sur la requete ' + request.url + ' : ' + err);
                    reject(err);
                });
        });
    }

    /**
     * Ajoute les données envoyé dans le jsonData dans l'URI de la requête.
     * @param request la requête à envoyer
     */
    handleGetRequest(request: RestRequest) {
        // SecMobile crash lorsqu'on lui passe une requête GET avec un body,
        // on doit donc ajouter les éléments du body dans l'uri avant
        if (request.method === 'GET' && request.jsonData !== undefined) {
            request.url = `${request.url}?${this.jsonToQueryString(request.jsonData)}`;
            request.jsonData = undefined;
        }
    }

    /**
     * Tranforme un json en string représentant les parametres de requête.
     * @param json le json à transformer
     */
    jsonToQueryString(json) {
        return Object.keys(json).map((key) => {
            return key + '=' + json[key];
        }).join('&');
    }

}
