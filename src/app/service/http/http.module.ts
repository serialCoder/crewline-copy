import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { Config } from '../../../configuration/environment-variables/config';
import { SessionService } from '../session/session.service';
import { RestService } from './rest/rest.base.service';
import { RestMobileService } from './rest/rest.mobile.service';
import { RestWebService } from './rest/rest.web.service';
import { SecMobilService } from './secMobil.service';

declare var window: any;

@NgModule({
  imports: [],
  exports: [],
  providers: [
    SecMobilService,
    { provide: RestService, useFactory: createRestService, deps: [HttpClient, SessionService, SecMobilService, Config] },
    HttpClientModule,
  ]
})
export class HttpModule { }

// Check if we are in app mode or in web browser
export function createRestService(
  http: HttpClient,
  sessionService: SessionService,
  secMobilService: SecMobilService,
  config: Config): RestService {
  if (undefined !== window.cordova && 'browser' !== window.cordova.platformId) {
    return new RestMobileService(http, secMobilService, config, sessionService);
  } else {
    return new RestWebService(http, config, sessionService);
  }
}
