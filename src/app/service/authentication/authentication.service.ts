import { Injectable } from '@angular/core';
import { CrewTypeEnum } from 'src/app/enums/crew-type.enum';
import { DivisionEnum } from 'src/app/enums/division.enum';

import { Config } from '../../../configuration/environment-variables/config';
import { User } from '../../models/user';
import { StorageService } from '../../storage/storage.service';
import { RestService } from '../http/rest/rest.base.service';
import { SessionService } from '../session/session.service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

  constructor(
    private restService: RestService,
    private sessionService: SessionService,
    private storageService: StorageService,
    private config: Config
  ) {
  }

  /**
   * Récupère le user connecté à l'application
   * @return une promesse contenant le user connecté
   */
  getAuthenticatedUser(): Promise<User> {
    const user = new User('42615534');
    user.division = DivisionEnum.AFRIQUE;
    user.email = 'mccamara@airfrance.fr';
    user.firstName = 'Mohamed Caso';
    user.lastName = 'CAMARA';
    user.populationType = CrewTypeEnum.PNC;
    user.registrationNumber = '42615534';
    this.storageService.saveUserData(user);
    return Promise.resolve(user);
    /*  return this.restService.get(this.config.backEndAddress).then(user => {
        this.sessionService.authenticatedUser = user;
        // On met à jour les données dans le cache
        this.storageService.saveUserData(user);
        return Promise.resolve(user);
      }).catch(error => {
        // En cas d'erreur, on tente de récupérer les données depuis le cache de l'appli
        return this.storageService.getUserData().then(userInStorage => {
          this.sessionService.authenticatedUser = userInStorage;
          return Promise.resolve(userInStorage);
        });
      });
      */
  }

}
