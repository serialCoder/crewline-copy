import { Injectable } from '@angular/core';
import { CrewTypeEnum } from 'src/app/enums/crew-type.enum';
import { DivisionEnum } from 'src/app/enums/division.enum';

import { User } from '../../models/user';

@Injectable({ providedIn: 'root' })
export class SessionService {
    authenticatedUser: User;
    impersonatedUser: User;

    /**
     * Retourne l'utilisateur "actif". Il s'agit de l'utilisateur connecté, ou de l'utilisateur impersonnifié si celui ci existe.
     * @return l'utilisateur actif
     */
    getActiveUser(): User {
        const user = new User('42615534');
        user.division = DivisionEnum.AFRIQUE;
        user.email = 'mccamara@airfrance.fr';
        user.firstName = 'Mohamed Caso';
        user.lastName = 'CAMARA';
        user.populationType = CrewTypeEnum.PNC;
        user.registrationNumber = '42615534';
        return user;
        //  return this.impersonatedUser ? this.impersonatedUser : this.authenticatedUser;
    }

    /**
     * Vérifie que le pnc consulté est la personne connectée
     * @return vrai si c'est le cas, faux sinon
     */
    isActiveUser(user: User): boolean {
        if (!user) {
            return false;
        }
        return user.registrationNumber === (this.getActiveUser() && this.getActiveUser().registrationNumber);
    }

}
