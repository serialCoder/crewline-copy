import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

declare var window: any;

@Injectable({ providedIn: 'root' })
export class DeviceService {
    constructor(
        public platform: Platform
    ) { }

    /**
     * Détermine si on est sur navigateur ou sur mobile.
     * @return vrai si on est sur navigateur, faux sinon.
     */
    isBrowser(): boolean {
        return window.device && window.device.platform === 'browser' || !this.platform.is('cordova');
    }
}
