import { Injectable } from '@angular/core';

/**
 * Provides access to Cordova plugins.
 * If the application is not running in Cordova it will return mock implementations.
 */
@Injectable({ providedIn: 'root' })
export class PluginsService {
    /**
     * Exposes an API for secmobil authent.
     */
    get secmobil(): CertAuthPlugin {
        return (<any>window).cordova.plugins.CertAuthPlugin;
    }

    /**
     * Exposes an API for BrandKhoros messenger.
     */
    get brand_khoros_native_messenger(): BrandKhorosMessenger {
        return (<any>window).cordova.plugins.BrandKhorosMessenger;
    }
}
