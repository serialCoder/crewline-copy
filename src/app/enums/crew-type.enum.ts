export enum CrewTypeEnum {
    PNT = 'PNT',
    PNC = 'PNC',
    PS = 'PS'
}