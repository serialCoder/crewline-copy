export enum DivisionEnum {
    AFRIQUE = 'AFRIQUE',
    AMERIQUE = 'AMERIQUE',
    ASIE = 'ASIE',
    CILA = 'CILA',
    EUROPE = 'EUROPE',
    FRANCE = 'FRANCE',
    PPT = 'PPT',
    PTP = 'PTP',
    PS = 'PS',
    FLOTTE_320 = 'FLOTTE_320',
    FLOTTE_380 = 'FLOTTE_380',
    FLOTTE_340 = 'FLOTTE_340',
    FLOTTE_777_787 = 'FLOTTE_777_787'
}