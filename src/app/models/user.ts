import { CrewTypeEnum } from '../enums/crew-type.enum';
import { DivisionEnum } from '../enums/division.enum';
import { FunctionEnum } from '../enums/function.enum';

export class User {
    registrationNumber: string;
    firstName: string;
    lastName: string;
    email: string;
    function: FunctionEnum;
    division: DivisionEnum;
    populationType: CrewTypeEnum;
    haulType: string;

    constructor(registrationNumber: string) {
        this.registrationNumber = registrationNumber;
    }
}
