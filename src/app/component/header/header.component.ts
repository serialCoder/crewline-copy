import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() hideBackButton: boolean;

  constructor(public menu: MenuController,
    public location: Location) {
    this.menu.enable(true, 'menu');
  }

  ngOnInit() { }

  goBack(): void {
    this.location.back();
  }

}
