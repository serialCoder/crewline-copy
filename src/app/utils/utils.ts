export class Utils {

    /**
     * Remplace tous les caractéres accentués par leurs pendants non accentués
     * @param text texte à traiter
     * @return la chaine "assainie"
     */
    public static replaceSpecialCaracters(text): string {
        const SpecialCharList = { 'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ø': 'o', 'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ç': 'c', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ÿ': 'y', 'ñ': 'n' };
        const reg = /[àáäâèéêëçìíîïòóôõöøùúûüÿñ]/gi;
        return text.replace(reg,
            function () {
                return SpecialCharList[arguments[0].toLowerCase()];
            }).toLowerCase();
    }


}
