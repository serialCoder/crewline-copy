

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, Config as IonicConfig, MenuController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { Config } from '../configuration/environment-variables/config';
import {
    BrandKhorosMessengerService
} from './service/brand-khoros-messenger/brand-khoros-messenger.service';
import { SecMobilService } from './service/http/secMobil.service';
import { ImpersonateService } from './service/impersonate/impersonate.service';
import { PluginsService } from './service/plugin/plugins.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private translateService: TranslateService,
    private pluginsService: PluginsService,
    private config: Config,
    private ionicConfig: IonicConfig,
    private alertController: AlertController,
    private brandkhorosMessengerService: BrandKhorosMessengerService,
    private secMobilService: SecMobilService,
    private router: Router,
    private splashScreen: SplashScreen,
    private impersonateService: ImpersonateService,
    public menu: MenuController) {
    this.initializeApp();
  }

  private initializeApp() {
    this.translateService.setDefaultLang('en');
    this.translateService.use('fr');
    this.ionicConfig.set('backButtonText', 'Retour');

    this.platform.ready()
      .then(() => {
        if (this.platform.is('cordova')) {
          this.pluginsService.secmobil.initSecmobilHttp(this.config.secmobileEnv);
        }

        this.statusBar.styleDefault();

        this.checkCertificateAndLogon().then(() => {
          this.splashScreen.hide();
        });
      });

    this.platform.resume.subscribe(() => {
      this.checkCertificateAndLogon();
    });
  }

  checkCertificateAndLogon(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.secMobilService.isAuthenticated().then((isLoggedIn: Boolean) => {
        if (!isLoggedIn) {
          this.router.navigate(['/login']);
        }
      }, (error) => {
        this.router.navigate(['/login']);
      });
    });
  }

  logout() {
    this.menu.close('menu');

    this.alertController
      .create({
        header: this.translateService.instant('label.caution'),
        message: this.translateService.instant('message.alert_logout'),
        buttons: [{
          text: this.translateService.instant('button.cancel'),
          role: 'cancel'
        },
        {
          text: this.translateService.instant('button.logout'),
          handler: () => {
            this.secMobilService.secMobilRevokeCertificate();
            this.router.navigate(['/login']);
          }
        }],
        keyboardClose: true,
      })
      .then(alert => alert.present());
  }

  impersonate() {
    this.menu.close('menu');

    this.alertController.create({
      header: this.translateService.instant('impersonate.header'),
      message: this.translateService.instant('impersonate.message'),
      inputs: [{
        name: 'registrationNumber',
        placeholder: this.translateService.instant('impersonate.placeholder')
      }],
      buttons: [{
        text: this.translateService.instant('impersonate.button.submit'),
        role: 'submit',
        handler: (data) => {
          this.impersonateService.impersonate(data.registrationNumber);
          this.brandkhorosMessengerService.destroyBrandKhorosMessenger();
        }
      },
      {
        text: this.translateService.instant('impersonate.button.reset'),
        role: 'reset',
        handler: () => {
          this.impersonateService.clearImpersonate();
          this.brandkhorosMessengerService.destroyBrandKhorosMessenger();
        }
      },
      {
        text: this.translateService.instant('impersonate.button.cancel'),
        role: 'cancel'
      }]
    }).then((alert) => { alert.present(); });
  }

  /**
   * Vérifie qu'on se trouve en prod
   * @return vrai si c'est le cas, faux sinon
   */
  isLiveEnv(): boolean {
    return this.config.env === 'prod';
  }
}
