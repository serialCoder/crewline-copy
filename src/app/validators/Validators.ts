﻿import { FormControl } from '@angular/forms';

export class UsernameValidator {

    private static REQUIRED_USERNAME_LENGTH = 7;

    static isValid(control: FormControl): any {
        const username = control.value;

        if (!username) {
            return {
                'validator.user_input_mandatory': true
            };
        }

        if (username.length !== UsernameValidator.REQUIRED_USERNAME_LENGTH) {
            return {
                'validator.user_input_length': true
            };
        }
        if (isNaN(username.substring(1, UsernameValidator.REQUIRED_USERNAME_LENGTH)) ||
            !username[0].match(/[a-zA-Z]/i)) {
            return {
                'validator.user_input_format': true
            };
        }
        return null;
    }
}

export class TokenValidator {

    private static REQUIRED_TOKEN_LENGTH = 10;

    static isValid(control: FormControl): any {
        const token = control.value;

        if (!token) {
            return {
                'validator.password_input_mandatory': true
            };
        }

        if (token.length !== TokenValidator.REQUIRED_TOKEN_LENGTH) {
            return {
                'validator.password_input_length': true
            };
        }

        if (isNaN(token)) {
            return {
                'validator.password_input_length': true
            };
        }

        // null means valid
        return null;
    }
}
