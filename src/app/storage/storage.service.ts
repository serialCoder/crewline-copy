import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Config } from '../../configuration/environment-variables/config';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class StorageService {

    constructor(
        private storage: Storage,
        private config: Config) {
    }

    /**
     * Persiste les données d'authentification de l'utilisateur courant
     * @param userData les données de l'utilisateur courant
     * @return une promesse qui est résolue quand la persistence est terminée
     */
    saveUserData(userData: User): Promise<any> {
        return this.storage.set(this.config.appName, userData);
    }

    /**
     * Récupère les données utilisateurs du cache
     * @return une promesse contenant les données utilisateurs
     */
    getUserData(): Promise<any> {
        return this.storage.get(this.config.appName);
    }

}

