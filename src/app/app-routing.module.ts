import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: './pages/auth/auth.module#AuthPageModule'
  },
  {
    path: 'conditions-generales',
    loadChildren: './pages/conditions-generales/conditions-generales.module#ConditionsGeneralesPageModule'
  },
  {
    path: 'mentions-legales',
    loadChildren: './pages/mentions-legales/mentions-legales.module#MentionsLegalesPageModule'
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
