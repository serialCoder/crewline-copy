import { Injectable } from '@angular/core';

import { DefaultConfig } from './default';

@Injectable()
export class Config extends DefaultConfig {
    constructor() {
        super();
    }
}
