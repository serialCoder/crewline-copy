import { Injectable } from '@angular/core';

import { DefaultConfig } from './default';

@Injectable()
export class Config extends DefaultConfig {
    constructor() {
        super();
        this.appVersion = '1.0.0';
        this.env = 'acc';
        this.secmobileEnv = 'rct';
        this.backEndAddress = 'https://servlog-api-rct.airfrance.fr/api/rest/resources/cert/user/crewline/v1/me';
        this.integrationId = '5cdbcb9586164a0011075481';
        this.khorosId = '5cb0a584e837a700104de42b';
        this.khorosRegion = 'us';
        this.khorosAdress = 'https://airfrancedev.response.lithium.com';
        this.khorosKeyId = 'app_5cb0a584bc7fd00010cc6bad';
        this.khorosSecret = 'TdmX_97snwI0QUU4X37pwMHnO6gqWLAkx5bq-MqMcqKYJjU00xVrz5Mm-48b9_fp39X25naFytCgDtUyfAJqzg';
    }
}
