export class DefaultConfig {
    public appName = 'CrewLine';
    public appVersion = '1.0.0';
    public env = 'default';
    public secmobileEnv = 'rct';
    public integrationId = '5cdbcb9586164a0011075481';
    public khorosId = '5cb0a584e837a700104de42b';
    public backEndAddress = 'https://servlog-api-rct.airfrance.fr/api/rest/resources/cert/user/crewline/v1/me';
    public khorosRegion = 'us';
    public khorosAdress = 'https://airfrancedev.response.lithium.com';
    public khorosKeyId = 'app_5cb0a584bc7fd00010cc6bad';
    public khorosSecret = 'TdmX_97snwI0QUU4X37pwMHnO6gqWLAkx5bq-MqMcqKYJjU00xVrz5Mm-48b9_fp39X25naFytCgDtUyfAJqzg';

    /**
    * Vérifie qu'on est en local
    * @return  vrai si on est sur l'env localhost, false sinon
    */
    isLocalhost(): boolean {
        return this.env === 'localhost';
    }
}
