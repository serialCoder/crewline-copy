import { Injectable } from '@angular/core';

import { DefaultConfig } from './default';

@Injectable()
export class Config extends DefaultConfig {
    constructor() {
        super();
        this.appVersion = '1.0.0';
        this.env = 'prod';
        this.secmobileEnv = 'prd';
        this.integrationId = '5d2d5130903566001063cb9e';
        this.backEndAddress = 'https://servlog-api.airfrance.fr/api/rest/resources/cert/user/crewline/v1/me';
        this.khorosId = '5cdb842818b31e001072e657';
        this.khorosRegion = 'eu-1';
        this.khorosAdress = 'https://airfrance.response.lithium.com';
        this.khorosKeyId = 'app_5cdb8429cb368b0010617ae9';
        this.khorosSecret = '8M0fyOqx-RqOtwAcIzM4EplR5Z8Mylm1Kz_DbL7-zAGF3QBJLGXvioflA6Wgg8-DoWYmfh0J1t1PAW83ou_unQ';
    }
}
