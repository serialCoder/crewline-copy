(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-auth-auth-module"],{

/***/ "./src/app/pages/auth/auth.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/auth/auth.module.ts ***!
  \*******************************************/
/*! exports provided: AuthPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthPageModule", function() { return AuthPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _auth_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth.page */ "./src/app/pages/auth/auth.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");








var routes = [
    {
        path: '',
        component: _auth_page__WEBPACK_IMPORTED_MODULE_6__["AuthPage"]
    }
];
var AuthPageModule = /** @class */ (function () {
    function AuthPageModule() {
    }
    AuthPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_auth_page__WEBPACK_IMPORTED_MODULE_6__["AuthPage"]]
        })
    ], AuthPageModule);
    return AuthPageModule;
}());



/***/ }),

/***/ "./src/app/pages/auth/auth.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/auth/auth.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\r\n  <h1>{{ \"label.app_name\" | translate }}</h1>\r\n  <img [src]=\"'assets/img/auth/avionenvol.png'\" id=\"imgLogoPlane\" />\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"loginForm\" (ngSubmit)=\"login()\">\r\n    <ion-list>\r\n      <ion-item lines=\"none\">\r\n        <ion-label position=\"stacked\">{{ 'label.identifier' | translate }}</ion-label>\r\n        <ion-input type=\"text\" formControlName=\"username\"></ion-input>\r\n      </ion-item>\r\n\r\n      <ion-item lines=\"none\">\r\n        <ion-label position=\"stacked\">{{ 'label.password' | translate }}</ion-label>\r\n        <ion-input type=\"password\" formControlName=\"password\"></ion-input>\r\n      </ion-item>\r\n\r\n      <ion-item lines=\"none\">\r\n        <ion-row>\r\n          <ion-col id=\"checkbox-col\">\r\n            <ion-checkbox formControlName=\"checked\"></ion-checkbox>\r\n          </ion-col>\r\n          <ion-col>\r\n            <ion-label class=\"ion-text-wrap\">J'ai lu et j'accepte les <a [routerLink]=\"['/mentions-legales']\">mentions\r\n                légales</a> et les <a [routerLink]=\"['/conditions-generales']\">conditions générales</a> de l'application\r\n            </ion-label>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-item>\r\n\r\n      <div padding text-center>\r\n        <ion-button type=\"submit\" color=\"primary\" [disabled]=\"loginForm.invalid && false\">\r\n          {{ 'button.login' | translate }}\r\n        </ion-button>\r\n        <br> <img [src]=\"'assets/img/auth/token_large.png'\" class=\"rsa\">\r\n      </div>\r\n\r\n    </ion-list>\r\n  </form>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/auth/auth.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/auth/auth.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/** Ionic CSS Variables **/\n:root {\n  /** primary **/\n  --ion-color-primary: #051025;\n  --ion-color-primary-rgb: 56, 128, 255;\n  --ion-color-primary-contrast: #ffffff;\n  --ion-color-primary-contrast-rgb: 255, 255, 255;\n  --ion-color-primary-shade: #3171e0;\n  --ion-color-primary-tint: #4c8dff;\n  /** secondary **/\n  --ion-color-secondary: #0cd1e8;\n  --ion-color-secondary-rgb: 12, 209, 232;\n  --ion-color-secondary-contrast: #ffffff;\n  --ion-color-secondary-contrast-rgb: 255, 255, 255;\n  --ion-color-secondary-shade: #0bb8cc;\n  --ion-color-secondary-tint: #24d6ea;\n  /** tertiary **/\n  --ion-color-tertiary: #7044ff;\n  --ion-color-tertiary-rgb: 112, 68, 255;\n  --ion-color-tertiary-contrast: #ffffff;\n  --ion-color-tertiary-contrast-rgb: 255, 255, 255;\n  --ion-color-tertiary-shade: #633ce0;\n  --ion-color-tertiary-tint: #7e57ff;\n  /** success **/\n  --ion-color-success: #10dc60;\n  --ion-color-success-rgb: 16, 220, 96;\n  --ion-color-success-contrast: #ffffff;\n  --ion-color-success-contrast-rgb: 255, 255, 255;\n  --ion-color-success-shade: #0ec254;\n  --ion-color-success-tint: #28e070;\n  /** warning **/\n  --ion-color-warning: #ffce00;\n  --ion-color-warning-rgb: 255, 206, 0;\n  --ion-color-warning-contrast: #ffffff;\n  --ion-color-warning-contrast-rgb: 255, 255, 255;\n  --ion-color-warning-shade: #e0b500;\n  --ion-color-warning-tint: #ffd31a;\n  /** danger **/\n  --ion-color-danger: #f04141;\n  --ion-color-danger-rgb: 245, 61, 61;\n  --ion-color-danger-contrast: #ffffff;\n  --ion-color-danger-contrast-rgb: 255, 255, 255;\n  --ion-color-danger-shade: #d33939;\n  --ion-color-danger-tint: #f25454;\n  /** dark **/\n  --ion-color-dark: #222428;\n  --ion-color-dark-rgb: 34, 34, 34;\n  --ion-color-dark-contrast: #ffffff;\n  --ion-color-dark-contrast-rgb: 255, 255, 255;\n  --ion-color-dark-shade: #1e2023;\n  --ion-color-dark-tint: #383a3e;\n  /** medium **/\n  --ion-color-medium: #989aa2;\n  --ion-color-medium-rgb: 152, 154, 162;\n  --ion-color-medium-contrast: #ffffff;\n  --ion-color-medium-contrast-rgb: 255, 255, 255;\n  --ion-color-medium-shade: #86888f;\n  --ion-color-medium-tint: #a2a4ab;\n  /** light **/\n  --ion-color-light: #fff;\n  --ion-color-light-rgb: 244, 244, 244;\n  --ion-color-light-contrast: #051025;\n  --ion-color-light-contrast-rgb: 0, 0, 0;\n  --ion-color-light-shade: #d7d8da;\n  --ion-color-light-tint: #f5f6f9; }\n/**\r\n * Allows you to use retina images at various pixel densities.\r\n * Examples:\r\n *\r\n *   @include retina(/images/mypic.jpg, 2);\r\n *   @include retina(/images/mypic.jpg, 3, 100px 100px, left top no-repeat transparent);\r\n *\r\n * @param  {Value}  $path               The path to the file name minus extension.\r\n * @param  {Number} $cap:    2          The highest pixel density level images exist for.\r\n * @param  {Value}  $size:   auto auto  The intended width of the rendered image.\r\n * @param  {Value}  $extras: null       Any other `background` values to be added.\r\n */\nion-header h1 {\n  text-align: center;\n  font-size: 3em; }\nion-header #imgLogoPlane {\n  display: block;\n  margin-left: 10%;\n  width: 80%; }\nion-content form {\n  padding-left: 10vw;\n  padding-right: 10vw; }\nion-content form #checkbox-col {\n    display: flex;\n    max-width: 40px;\n    margin: 10px;\n    align-items: center; }\nion-content form #checkbox-col ion-checkbox {\n      margin: 0; }\nion-content form .item-input {\n    margin-bottom: 0.7em; }\nion-content form .item-input ion-input {\n      border: 1px solid #ccc;\n      padding: .2em .5em !important;\n      border-radius: 1em; }\nion-content form .item-input ion-label {\n      font-size: 1em;\n      margin-bottom: .5em !important; }\nion-content form .rsa {\n    height: 50px; }\nion-content form button {\n    text-transform: uppercase;\n    font-weight: bold;\n    font-size: 0.8em;\n    padding: 8px;\n    margin: 5px;\n    border-radius: 4px;\n    letter-spacing: 1px; }\n@media (min-height: 500px) {\n  ion-content form {\n    margin-top: 10vh; } }\n@media (min-height: 650px) {\n  ion-content form {\n    margin-top: 20vh; } }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXV0aC9DOlxcVXNlcnNcXG00MjYxNTVcXERlc2t0b3BcXGNyZXdsaW5lLWNvcHkvc3JjXFx0aGVtZVxcdmFyaWFibGVzLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2F1dGgvYXV0aC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2F1dGgvQzpcXFVzZXJzXFxtNDI2MTU1XFxEZXNrdG9wXFxjcmV3bGluZS1jb3B5L3NyY1xcYXBwXFxwYWdlc1xcYXV0aFxcYXV0aC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0EsMEJBQUE7QUFDQTtFQUNFLGNBQUE7RUFDQSw0QkFBb0I7RUFDcEIscUNBQXdCO0VBQ3hCLHFDQUE2QjtFQUM3QiwrQ0FBaUM7RUFDakMsa0NBQTBCO0VBQzFCLGlDQUF5QjtFQUV6QixnQkFBQTtFQUNBLDhCQUFzQjtFQUN0Qix1Q0FBMEI7RUFDMUIsdUNBQStCO0VBQy9CLGlEQUFtQztFQUNuQyxvQ0FBNEI7RUFDNUIsbUNBQTJCO0VBRTNCLGVBQUE7RUFDQSw2QkFBcUI7RUFDckIsc0NBQXlCO0VBQ3pCLHNDQUE4QjtFQUM5QixnREFBa0M7RUFDbEMsbUNBQTJCO0VBQzNCLGtDQUEwQjtFQUUxQixjQUFBO0VBQ0EsNEJBQW9CO0VBQ3BCLG9DQUF3QjtFQUN4QixxQ0FBNkI7RUFDN0IsK0NBQWlDO0VBQ2pDLGtDQUEwQjtFQUMxQixpQ0FBeUI7RUFFekIsY0FBQTtFQUNBLDRCQUFvQjtFQUNwQixvQ0FBd0I7RUFDeEIscUNBQTZCO0VBQzdCLCtDQUFpQztFQUNqQyxrQ0FBMEI7RUFDMUIsaUNBQXlCO0VBRXpCLGFBQUE7RUFDQSwyQkFBbUI7RUFDbkIsbUNBQXVCO0VBQ3ZCLG9DQUE0QjtFQUM1Qiw4Q0FBZ0M7RUFDaEMsaUNBQXlCO0VBQ3pCLGdDQUF3QjtFQUV4QixXQUFBO0VBQ0EseUJBQWlCO0VBQ2pCLGdDQUFxQjtFQUNyQixrQ0FBMEI7RUFDMUIsNENBQThCO0VBQzlCLCtCQUF1QjtFQUN2Qiw4QkFBc0I7RUFFdEIsYUFBQTtFQUNBLDJCQUFtQjtFQUNuQixxQ0FBdUI7RUFDdkIsb0NBQTRCO0VBQzVCLDhDQUFnQztFQUNoQyxpQ0FBeUI7RUFDekIsZ0NBQXdCO0VBRXhCLFlBQUE7RUFDQSx1QkFBa0I7RUFDbEIsb0NBQXNCO0VBQ3RCLG1DQUEyQjtFQUMzQix1Q0FBK0I7RUFDL0IsZ0NBQXdCO0VBQ3hCLCtCQUF1QixFQUFBO0FBYXpCOzs7Ozs7Ozs7OztFQ1hFO0FDM0VGO0VBR0ksa0JBQWtCO0VBQ2xCLGNBQWMsRUFBQTtBQUpsQjtFQVFJLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsVUFBVSxFQUFBO0FBSWQ7RUFHSSxrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7QUFKdkI7SUFPTSxhQUFhO0lBQ2IsZUFBZTtJQUNmLFlBQVk7SUFDWixtQkFBbUIsRUFBQTtBQVZ6QjtNQWFRLFNBQVMsRUFBQTtBQWJqQjtJQWtCTSxvQkFBb0IsRUFBQTtBQWxCMUI7TUFxQlEsc0JBQXNCO01BQ3RCLDZCQUE2QjtNQUM3QixrQkFBa0IsRUFBQTtBQXZCMUI7TUE0QlEsY0FBYztNQUNkLDhCQUE4QixFQUFBO0FBN0J0QztJQWtDTSxZQUFZLEVBQUE7QUFsQ2xCO0lBc0NNLHlCQUF5QjtJQUV6QixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLG1CQUFtQixFQUFBO0FBS3pCO0VBQ0U7SUFFSSxnQkFBZ0IsRUFBQSxFQUNqQjtBQUlMO0VBQ0U7SUFFSSxnQkFBZ0IsRUFBQSxFQUNqQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2F1dGgvYXV0aC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBJb25pYyBWYXJpYWJsZXMgYW5kIFRoZW1pbmcuIEZvciBtb3JlIGluZm8sIHBsZWFzZSBzZWU6XHJcbi8vIGh0dHA6Ly9pb25pY2ZyYW1ld29yay5jb20vZG9jcy90aGVtaW5nL1xyXG5cclxuLyoqIElvbmljIENTUyBWYXJpYWJsZXMgKiovXHJcbjpyb290IHtcclxuICAvKiogcHJpbWFyeSAqKi9cclxuICAtLWlvbi1jb2xvci1wcmltYXJ5OiAjMDUxMDI1O1xyXG4gIC0taW9uLWNvbG9yLXByaW1hcnktcmdiOiA1NiwgMTI4LCAyNTU7XHJcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdDogI2ZmZmZmZjtcclxuICAtLWlvbi1jb2xvci1wcmltYXJ5LWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcclxuICAtLWlvbi1jb2xvci1wcmltYXJ5LXNoYWRlOiAjMzE3MWUwO1xyXG4gIC0taW9uLWNvbG9yLXByaW1hcnktdGludDogIzRjOGRmZjtcclxuXHJcbiAgLyoqIHNlY29uZGFyeSAqKi9cclxuICAtLWlvbi1jb2xvci1zZWNvbmRhcnk6ICMwY2QxZTg7XHJcbiAgLS1pb24tY29sb3Itc2Vjb25kYXJ5LXJnYjogMTIsIDIwOSwgMjMyO1xyXG4gIC0taW9uLWNvbG9yLXNlY29uZGFyeS1jb250cmFzdDogI2ZmZmZmZjtcclxuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3QtcmdiOiAyNTUsIDI1NSwgMjU1O1xyXG4gIC0taW9uLWNvbG9yLXNlY29uZGFyeS1zaGFkZTogIzBiYjhjYztcclxuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktdGludDogIzI0ZDZlYTtcclxuXHJcbiAgLyoqIHRlcnRpYXJ5ICoqL1xyXG4gIC0taW9uLWNvbG9yLXRlcnRpYXJ5OiAjNzA0NGZmO1xyXG4gIC0taW9uLWNvbG9yLXRlcnRpYXJ5LXJnYjogMTEyLCA2OCwgMjU1O1xyXG4gIC0taW9uLWNvbG9yLXRlcnRpYXJ5LWNvbnRyYXN0OiAjZmZmZmZmO1xyXG4gIC0taW9uLWNvbG9yLXRlcnRpYXJ5LWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcclxuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS1zaGFkZTogIzYzM2NlMDtcclxuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS10aW50OiAjN2U1N2ZmO1xyXG5cclxuICAvKiogc3VjY2VzcyAqKi9cclxuICAtLWlvbi1jb2xvci1zdWNjZXNzOiAjMTBkYzYwO1xyXG4gIC0taW9uLWNvbG9yLXN1Y2Nlc3MtcmdiOiAxNiwgMjIwLCA5NjtcclxuICAtLWlvbi1jb2xvci1zdWNjZXNzLWNvbnRyYXN0OiAjZmZmZmZmO1xyXG4gIC0taW9uLWNvbG9yLXN1Y2Nlc3MtY29udHJhc3QtcmdiOiAyNTUsIDI1NSwgMjU1O1xyXG4gIC0taW9uLWNvbG9yLXN1Y2Nlc3Mtc2hhZGU6ICMwZWMyNTQ7XHJcbiAgLS1pb24tY29sb3Itc3VjY2Vzcy10aW50OiAjMjhlMDcwO1xyXG5cclxuICAvKiogd2FybmluZyAqKi9cclxuICAtLWlvbi1jb2xvci13YXJuaW5nOiAjZmZjZTAwO1xyXG4gIC0taW9uLWNvbG9yLXdhcm5pbmctcmdiOiAyNTUsIDIwNiwgMDtcclxuICAtLWlvbi1jb2xvci13YXJuaW5nLWNvbnRyYXN0OiAjZmZmZmZmO1xyXG4gIC0taW9uLWNvbG9yLXdhcm5pbmctY29udHJhc3QtcmdiOiAyNTUsIDI1NSwgMjU1O1xyXG4gIC0taW9uLWNvbG9yLXdhcm5pbmctc2hhZGU6ICNlMGI1MDA7XHJcbiAgLS1pb24tY29sb3Itd2FybmluZy10aW50OiAjZmZkMzFhO1xyXG5cclxuICAvKiogZGFuZ2VyICoqL1xyXG4gIC0taW9uLWNvbG9yLWRhbmdlcjogI2YwNDE0MTtcclxuICAtLWlvbi1jb2xvci1kYW5nZXItcmdiOiAyNDUsIDYxLCA2MTtcclxuICAtLWlvbi1jb2xvci1kYW5nZXItY29udHJhc3Q6ICNmZmZmZmY7XHJcbiAgLS1pb24tY29sb3ItZGFuZ2VyLWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcclxuICAtLWlvbi1jb2xvci1kYW5nZXItc2hhZGU6ICNkMzM5Mzk7XHJcbiAgLS1pb24tY29sb3ItZGFuZ2VyLXRpbnQ6ICNmMjU0NTQ7XHJcblxyXG4gIC8qKiBkYXJrICoqL1xyXG4gIC0taW9uLWNvbG9yLWRhcms6ICMyMjI0Mjg7XHJcbiAgLS1pb24tY29sb3ItZGFyay1yZ2I6IDM0LCAzNCwgMzQ7XHJcbiAgLS1pb24tY29sb3ItZGFyay1jb250cmFzdDogI2ZmZmZmZjtcclxuICAtLWlvbi1jb2xvci1kYXJrLWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcclxuICAtLWlvbi1jb2xvci1kYXJrLXNoYWRlOiAjMWUyMDIzO1xyXG4gIC0taW9uLWNvbG9yLWRhcmstdGludDogIzM4M2EzZTtcclxuXHJcbiAgLyoqIG1lZGl1bSAqKi9cclxuICAtLWlvbi1jb2xvci1tZWRpdW06ICM5ODlhYTI7XHJcbiAgLS1pb24tY29sb3ItbWVkaXVtLXJnYjogMTUyLCAxNTQsIDE2MjtcclxuICAtLWlvbi1jb2xvci1tZWRpdW0tY29udHJhc3Q6ICNmZmZmZmY7XHJcbiAgLS1pb24tY29sb3ItbWVkaXVtLWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcclxuICAtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGU6ICM4Njg4OGY7XHJcbiAgLS1pb24tY29sb3ItbWVkaXVtLXRpbnQ6ICNhMmE0YWI7XHJcblxyXG4gIC8qKiBsaWdodCAqKi9cclxuICAtLWlvbi1jb2xvci1saWdodDogI2ZmZjtcclxuICAtLWlvbi1jb2xvci1saWdodC1yZ2I6IDI0NCwgMjQ0LCAyNDQ7XHJcbiAgLS1pb24tY29sb3ItbGlnaHQtY29udHJhc3Q6ICMwNTEwMjU7XHJcbiAgLS1pb24tY29sb3ItbGlnaHQtY29udHJhc3QtcmdiOiAwLCAwLCAwO1xyXG4gIC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlOiAjZDdkOGRhO1xyXG4gIC0taW9uLWNvbG9yLWxpZ2h0LXRpbnQ6ICNmNWY2Zjk7XHJcbn1cclxuXHJcbi8vIHJldGluYS5zY3NzXHJcbi8vIEEgaGVscGVyIG1peGluIGZvciBhcHBseWluZyBoaWdoLXJlc29sdXRpb24gYmFja2dyb3VuZCBpbWFnZXMgKGh0dHA6Ly93d3cucmV0aW5hanMuY29tKVxyXG4vLyBTdWJtaXR0ZWQgYnkgTmF0aGFuIENyYW5rXHJcbi8vIG5hdGhhbmNyYW5rLmNvbVxyXG4vLyBVcGRhdGVkIGJ5IEdhYnJpZWwgUi4gU2V6ZWZyZWRvXHJcbi8vIGdhYnJpZWwuc2V6ZWZyZWRvLmNvbS5iclxyXG4vLyBVcGRhdGVkIGJ5IEpvaG4gTmV3bWFuXHJcbi8vIGdpdGh1Yi5jb20vamduZXdtYW5cclxuLy8gaHR0cDovL2F4aWFsLmFnZW5jeVxyXG5cclxuLyoqXHJcbiAqIEFsbG93cyB5b3UgdG8gdXNlIHJldGluYSBpbWFnZXMgYXQgdmFyaW91cyBwaXhlbCBkZW5zaXRpZXMuXHJcbiAqIEV4YW1wbGVzOlxyXG4gKlxyXG4gKiAgIEBpbmNsdWRlIHJldGluYSgvaW1hZ2VzL215cGljLmpwZywgMik7XHJcbiAqICAgQGluY2x1ZGUgcmV0aW5hKC9pbWFnZXMvbXlwaWMuanBnLCAzLCAxMDBweCAxMDBweCwgbGVmdCB0b3Agbm8tcmVwZWF0IHRyYW5zcGFyZW50KTtcclxuICpcclxuICogQHBhcmFtICB7VmFsdWV9ICAkcGF0aCAgICAgICAgICAgICAgIFRoZSBwYXRoIHRvIHRoZSBmaWxlIG5hbWUgbWludXMgZXh0ZW5zaW9uLlxyXG4gKiBAcGFyYW0gIHtOdW1iZXJ9ICRjYXA6ICAgIDIgICAgICAgICAgVGhlIGhpZ2hlc3QgcGl4ZWwgZGVuc2l0eSBsZXZlbCBpbWFnZXMgZXhpc3QgZm9yLlxyXG4gKiBAcGFyYW0gIHtWYWx1ZX0gICRzaXplOiAgIGF1dG8gYXV0byAgVGhlIGludGVuZGVkIHdpZHRoIG9mIHRoZSByZW5kZXJlZCBpbWFnZS5cclxuICogQHBhcmFtICB7VmFsdWV9ICAkZXh0cmFzOiBudWxsICAgICAgIEFueSBvdGhlciBgYmFja2dyb3VuZGAgdmFsdWVzIHRvIGJlIGFkZGVkLlxyXG4gKi9cclxuXHJcbiBAbWl4aW4gcmV0aW5hKCRwYXRoLCAkY2FwOiAyLCAkc2l6ZTogYXV0byBhdXRvLCAkZXh0cmFzOiBudWxsKSB7XHJcbiAgLypcclxuICogU2V0IGEgY291bnRlciBhbmQgZ2V0IHRoZSBsZW5ndGggb2YgdGhlIGltYWdlIHBhdGguXHJcbiAqL1xyXG4gICRwb3NpdGlvbjogLTE7XHJcbiAgJHN0cnBhdGg6ICcjeyRwYXRofSc7XHJcbiAgJGxlbmd0aDogc3RyLWxlbmd0aCgkc3RycGF0aCk7XHJcbiAgLypcclxuICogTG9vcCB2ZXIgdGhlIGltYWdlIHBhdGggYW5kIGZpZ3VyZSBvdXQgdGhlXHJcbiAqIHBvc2l0aW9uIG9mIHRoZSBkb3Qgd2hlcmUgdGhlIGV4dGVuc2lvbiBiZWdpbnMuXHJcbiAqL1xyXG4gIEBmb3IgJGkgZnJvbSAkbGVuZ3RoIHRocm91Z2ggJGxlbmd0aCAtIDEwIHtcclxuICAgICAgQGlmICRwb3NpdGlvbj09LTEge1xyXG4gICAgICAgICAgJGNoYXI6IHN0ci1zbGljZSgkc3RycGF0aCwgJGksICRpKTtcclxuICAgICAgICAgIEBpZiBzdHItaW5kZXgoJGNoYXIsIFwiLlwiKT09MSB7XHJcbiAgICAgICAgICAgICAgJHBvc2l0aW9uOiAkaTtcclxuICAgICAgICAgIH1cclxuICAgICAgfVxyXG4gIH1cclxuICAvKlxyXG4gKiBJZiB3ZSB3ZXJlIGFibGUgdG8gZmlndXJlIG91dCB3aGVyZSB0aGUgZXh0ZW5zaW9uIGlzLFxyXG4gKiBzbGljZSB0aGUgcGF0aCBpbnRvIGEgYmFzZSBhbmQgYW4gZXh0ZW5zaW9uLiBVc2UgdGhhdCB0b1xyXG4gKiBjYWxjdWxhdGUgdXJscyBmb3IgZGlmZmVyZW50IGRlbnNpdHkgZW52aXJvbm1lbnRzLiBTZXRcclxuICogdmFsdWVzIGZvciBkaWZmZXJlbnQgZW52aXJvbm1lbnRzLlxyXG4gKi9cclxuICBAaWYgJHBvc2l0aW9uICE9LTEge1xyXG4gICAgICAkZXh0OiBzdHItc2xpY2UoJHN0cnBhdGgsICRwb3NpdGlvbiArIDEsICRsZW5ndGgpO1xyXG4gICAgICAkYmFzZTogc3RyLXNsaWNlKCRzdHJwYXRoLCAxLCAkcG9zaXRpb24gLSAxKTtcclxuICAgICAgJGF0MXhfcGF0aDogXCIjeyRiYXNlfS4jeyRleHR9XCI7XHJcbiAgICAgICRhdDJ4X3BhdGg6IFwiI3skYmFzZX1AMnguI3skZXh0fVwiO1xyXG4gICAgICAvKlxyXG4gICAqIFNldCBhIGJhc2UgYmFja2dyb3VuZCBmb3IgMXggZW52aXJvbm1lbnRzLlxyXG4gICAqL1xyXG4gICAgICBiYWNrZ3JvdW5kOiB1cmwoXCIjeyRhdDF4X3BhdGh9XCIpICRleHRyYXM7XHJcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogJHNpemU7XHJcbiAgICAgIC8qXHJcbiAgICogQ3JlYXRlIGFuIEAyeC1pc2ggbWVkaWEgcXVlcnkuXHJcbiAgICovXHJcbiAgICAgIEBtZWRpYSBhbGwgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDEuNSksIGFsbCBhbmQgKC1vLW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDMvMiksIGFsbCBhbmQgKG1pbi0tbW96LWRldmljZS1waXhlbC1yYXRpbzogMS41KSwgYWxsIGFuZCAobWluLWRldmljZS1waXhlbC1yYXRpbzogMS41KSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoXCIjeyRhdDJ4X3BhdGh9XCIpICRleHRyYXM7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6ICRzaXplO1xyXG4gICAgICB9XHJcbiAgICAgIC8qXHJcbiAgICogQ3JlYXRlIG1lZGlhIHF1ZXJpZXMgZm9yIGFsbCBlbnZpcm9ubWVudHMgdGhhdCB0aGUgdXNlciBoYXNcclxuICAgKiBwcm92aWRlZCBpbWFnZXMgZm9yLlxyXG4gICAqL1xyXG4gICAgICBAaWYgJGNhcD49MiB7XHJcbiAgICAgICAgICBAZm9yICRlbnYgZnJvbSAyIHRocm91Z2ggJGNhcCB7XHJcbiAgICAgICAgICAgICAgJHN1ZmZpeDogXCJAI3skZW52fXhcIjtcclxuICAgICAgICAgICAgICBAbWVkaWEgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogJGVudiksIChtaW4tcmVzb2x1dGlvbjogJGVudiAqIDk2ZHBpKSB7XHJcbiAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHVybChcIiN7JGJhc2V9I3skc3VmZml4fS4jeyRleHR9XCIpICRleHRyYXM7XHJcbiAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogJHNpemU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC8qXHJcbiAqIElmIGFueXRoaW5nIHdlbnQgd3JvbmcgdHJ5aW5nIHRvIHNlcGFyYXRlIHRoZSBmaWxlIGZyb20gaXRzXHJcbiAqIGV4dGVuc2lvbiwgc2V0IGEgYmFja2dyb3VuZCB2YWx1ZSB3aXRob3V0IGRvaW5nIGFueXRoaW5nIHRvIGl0LlxyXG4gKi9cclxuICB9XHJcbiAgQGVsc2Uge1xyXG4gICAgICBiYWNrZ3JvdW5kOiB1cmwoXCIjeyRwYXRofVwiKSAkZXh0cmFzO1xyXG4gICAgICBiYWNrZ3JvdW5kLXNpemU6ICRzaXplO1xyXG4gIH1cclxufVxyXG5cclxuQG1peGluIHNhbXBsZUljb24oJHBhdGgsICRjYXA6IDIsICRzaXplOiBhdXRvIGF1dG8pIHtcclxuXHJcbiAgY29udGVudDogdXJsKFwiI3skcGF0aH1cIik7XHJcbn1cclxuXHJcbkBtaXhpbiByZXRpbmFJY29uKCRwYXRoLCAkY2FwOiAyLCAkc2l6ZTogYXV0byBhdXRvKSB7XHJcblxyXG4gIC8qXHJcbiAqIFNldCBhIGNvdW50ZXIgYW5kIGdldCB0aGUgbGVuZ3RoIG9mIHRoZSBpbWFnZSBwYXRoLlxyXG4gKi9cclxuICAkcG9zaXRpb246IC0xO1xyXG4gICRzdHJwYXRoOiAnI3skcGF0aH0nO1xyXG4gICRsZW5ndGg6IHN0ci1sZW5ndGgoJHN0cnBhdGgpO1xyXG4gIC8qXHJcbiAqIExvb3AgdmVyIHRoZSBpbWFnZSBwYXRoIGFuZCBmaWd1cmUgb3V0IHRoZVxyXG4gKiBwb3NpdGlvbiBvZiB0aGUgZG90IHdoZXJlIHRoZSBleHRlbnNpb24gYmVnaW5zLlxyXG4gKi9cclxuICBAZm9yICRpIGZyb20gJGxlbmd0aCB0aHJvdWdoICRsZW5ndGggLSAxMCB7XHJcbiAgICAgIEBpZiAkcG9zaXRpb249PS0xIHtcclxuICAgICAgICAgICRjaGFyOiBzdHItc2xpY2UoJHN0cnBhdGgsICRpLCAkaSk7XHJcbiAgICAgICAgICBAaWYgc3RyLWluZGV4KCRjaGFyLCBcIi5cIik9PTEge1xyXG4gICAgICAgICAgICAgICRwb3NpdGlvbjogJGk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgIH1cclxuICB9XHJcbiAgLypcclxuICogSWYgd2Ugd2VyZSBhYmxlIHRvIGZpZ3VyZSBvdXQgd2hlcmUgdGhlIGV4dGVuc2lvbiBpcyxcclxuICogc2xpY2UgdGhlIHBhdGggaW50byBhIGJhc2UgYW5kIGFuIGV4dGVuc2lvbi4gVXNlIHRoYXQgdG9cclxuICogY2FsY3VsYXRlIHVybHMgZm9yIGRpZmZlcmVudCBkZW5zaXR5IGVudmlyb25tZW50cy4gU2V0XHJcbiAqIHZhbHVlcyBmb3IgZGlmZmVyZW50IGVudmlyb25tZW50cy5cclxuICovXHJcbiAgQGlmICRwb3NpdGlvbiAhPS0xIHtcclxuICAgICAgJGV4dDogc3RyLXNsaWNlKCRzdHJwYXRoLCAkcG9zaXRpb24gKyAxLCAkbGVuZ3RoKTtcclxuICAgICAgJGJhc2U6IHN0ci1zbGljZSgkc3RycGF0aCwgMSwgJHBvc2l0aW9uIC0gMSk7XHJcbiAgICAgICRhdDF4X3BhdGg6IFwiI3skYmFzZX0uI3skZXh0fVwiO1xyXG4gICAgICAkYXQyeF9wYXRoOiBcIiN7JGJhc2V9QDJ4LiN7JGV4dH1cIjtcclxuICAgICAgLypcclxuICAgKiBTZXQgYSBiYXNlIGJhY2tncm91bmQgZm9yIDF4IGVudmlyb25tZW50cy5cclxuICAgKi9cclxuICAgICAgY29udGVudDogdXJsKFwiI3skYXQxeF9wYXRofVwiKTtcclxuICAgICAgLy8gYmFja2dyb3VuZC1zaXplOiAkc2l6ZTtcclxuICAgICAgLypcclxuICAgKiBDcmVhdGUgYW4gQDJ4LWlzaCBtZWRpYSBxdWVyeS5cclxuICAgKi9cclxuICAgICAgQG1lZGlhIGFsbCBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMS41KSwgYWxsIGFuZCAoLW8tbWluLWRldmljZS1waXhlbC1yYXRpbzogMy8yKSwgYWxsIGFuZCAobWluLS1tb3otZGV2aWNlLXBpeGVsLXJhdGlvOiAxLjUpLCBhbGwgYW5kIChtaW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAxLjUpIHtcclxuICAgICAgICAgIGNvbnRlbnQ6IHVybChcIiN7JGF0MnhfcGF0aH1cIik7XHJcbiAgICAgICAgICAvLyBiYWNrZ3JvdW5kLXNpemU6ICRzaXplO1xyXG4gICAgICB9XHJcbiAgICAgIC8qXHJcbiAgICogQ3JlYXRlIG1lZGlhIHF1ZXJpZXMgZm9yIGFsbCBlbnZpcm9ubWVudHMgdGhhdCB0aGUgdXNlciBoYXNcclxuICAgKiBwcm92aWRlZCBpbWFnZXMgZm9yLlxyXG4gICAqL1xyXG4gICAgICBAaWYgJGNhcD49MiB7XHJcbiAgICAgICAgICBAZm9yICRlbnYgZnJvbSAyIHRocm91Z2ggJGNhcCB7XHJcbiAgICAgICAgICAgICAgJHN1ZmZpeDogXCJAI3skZW52fXhcIjtcclxuICAgICAgICAgICAgICBAbWVkaWEgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogJGVudiksIChtaW4tcmVzb2x1dGlvbjogJGVudiAqIDk2ZHBpKSB7XHJcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IHVybChcIiN7JGJhc2V9I3skc3VmZml4fS4jeyRleHR9XCIpO1xyXG4gICAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLXNpemU6ICRzaXplO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAvKlxyXG4gKiBJZiBhbnl0aGluZyB3ZW50IHdyb25nIHRyeWluZyB0byBzZXBhcmF0ZSB0aGUgZmlsZSBmcm9tIGl0c1xyXG4gKiBleHRlbnNpb24sIHNldCBhIGJhY2tncm91bmQgdmFsdWUgd2l0aG91dCBkb2luZyBhbnl0aGluZyB0byBpdC5cclxuICovXHJcbiAgfVxyXG4gIEBlbHNlIHtcclxuICAgICAgY29udGVudDogdXJsKFwiI3skcGF0aH1cIik7XHJcbiAgICAgIC8vIGJhY2tncm91bmQtc2l6ZTogJHNpemU7XHJcbiAgfVxyXG59XHJcblxyXG4vLyBtaXhpbiB1c2VkIGluIGNyZWF0ZS1kZGEtcmVzdC1wbmMgdG8gZ2VuZXJhdGUgY2xhc3NcclxuQG1peGluIHNlbGVjdGVkKCRwb3NpdGlvbi10b3AsICRhY3RpdmUpIHtcclxuICAmOmFmdGVyIHtcclxuICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgYm9yZGVyLWJvdHRvbTogMTVweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgYm9yZGVyLWxlZnQ6IDE1cHggc29saWQgI2ZmZjtcclxuICAgICAgYm9yZGVyLXRvcDogMTVweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB0b3A6ICRwb3NpdGlvbi10b3A7XHJcbiAgICAgIHJpZ2h0OiAtMTFweDtcclxuICAgICAgei1pbmRleDogMTA7XHJcbiAgICAgIGJvcmRlci1sZWZ0OiAxNXB4IHNvbGlkICNmZmY7XHJcbiAgfVxyXG4gICY6YmVmb3JlIHtcclxuICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgYm9yZGVyLWJvdHRvbTogMTVweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgYm9yZGVyLXRvcDogMTVweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB0b3A6ICRwb3NpdGlvbi10b3A7XHJcbiAgICAgIHJpZ2h0OiAtMTRweDtcclxuICAgICAgei1pbmRleDogMTA7XHJcbiAgICAgIEBpZiAkYWN0aXZlIHtcclxuICAgICAgICAgIGJvcmRlci1sZWZ0OiAxNXB4IHNvbGlkICRhY2NlbnQtY29sb3I7XHJcbiAgICAgIH1AZWxzZSB7XHJcbiAgICAgICAgICBib3JkZXItbGVmdDogMTVweCBzb2xpZCAjZGVkZWRlO1xyXG4gICAgICB9XHJcbiAgfVxyXG59XHJcbiIsIi8qKiBJb25pYyBDU1MgVmFyaWFibGVzICoqL1xuOnJvb3Qge1xuICAvKiogcHJpbWFyeSAqKi9cbiAgLS1pb24tY29sb3ItcHJpbWFyeTogIzA1MTAyNTtcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1yZ2I6IDU2LCAxMjgsIDI1NTtcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdDogI2ZmZmZmZjtcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XG4gIC0taW9uLWNvbG9yLXByaW1hcnktc2hhZGU6ICMzMTcxZTA7XG4gIC0taW9uLWNvbG9yLXByaW1hcnktdGludDogIzRjOGRmZjtcbiAgLyoqIHNlY29uZGFyeSAqKi9cbiAgLS1pb24tY29sb3Itc2Vjb25kYXJ5OiAjMGNkMWU4O1xuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktcmdiOiAxMiwgMjA5LCAyMzI7XG4gIC0taW9uLWNvbG9yLXNlY29uZGFyeS1jb250cmFzdDogI2ZmZmZmZjtcbiAgLS1pb24tY29sb3Itc2Vjb25kYXJ5LWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcbiAgLS1pb24tY29sb3Itc2Vjb25kYXJ5LXNoYWRlOiAjMGJiOGNjO1xuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktdGludDogIzI0ZDZlYTtcbiAgLyoqIHRlcnRpYXJ5ICoqL1xuICAtLWlvbi1jb2xvci10ZXJ0aWFyeTogIzcwNDRmZjtcbiAgLS1pb24tY29sb3ItdGVydGlhcnktcmdiOiAxMTIsIDY4LCAyNTU7XG4gIC0taW9uLWNvbG9yLXRlcnRpYXJ5LWNvbnRyYXN0OiAjZmZmZmZmO1xuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XG4gIC0taW9uLWNvbG9yLXRlcnRpYXJ5LXNoYWRlOiAjNjMzY2UwO1xuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS10aW50OiAjN2U1N2ZmO1xuICAvKiogc3VjY2VzcyAqKi9cbiAgLS1pb24tY29sb3Itc3VjY2VzczogIzEwZGM2MDtcbiAgLS1pb24tY29sb3Itc3VjY2Vzcy1yZ2I6IDE2LCAyMjAsIDk2O1xuICAtLWlvbi1jb2xvci1zdWNjZXNzLWNvbnRyYXN0OiAjZmZmZmZmO1xuICAtLWlvbi1jb2xvci1zdWNjZXNzLWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcbiAgLS1pb24tY29sb3Itc3VjY2Vzcy1zaGFkZTogIzBlYzI1NDtcbiAgLS1pb24tY29sb3Itc3VjY2Vzcy10aW50OiAjMjhlMDcwO1xuICAvKiogd2FybmluZyAqKi9cbiAgLS1pb24tY29sb3Itd2FybmluZzogI2ZmY2UwMDtcbiAgLS1pb24tY29sb3Itd2FybmluZy1yZ2I6IDI1NSwgMjA2LCAwO1xuICAtLWlvbi1jb2xvci13YXJuaW5nLWNvbnRyYXN0OiAjZmZmZmZmO1xuICAtLWlvbi1jb2xvci13YXJuaW5nLWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcbiAgLS1pb24tY29sb3Itd2FybmluZy1zaGFkZTogI2UwYjUwMDtcbiAgLS1pb24tY29sb3Itd2FybmluZy10aW50OiAjZmZkMzFhO1xuICAvKiogZGFuZ2VyICoqL1xuICAtLWlvbi1jb2xvci1kYW5nZXI6ICNmMDQxNDE7XG4gIC0taW9uLWNvbG9yLWRhbmdlci1yZ2I6IDI0NSwgNjEsIDYxO1xuICAtLWlvbi1jb2xvci1kYW5nZXItY29udHJhc3Q6ICNmZmZmZmY7XG4gIC0taW9uLWNvbG9yLWRhbmdlci1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XG4gIC0taW9uLWNvbG9yLWRhbmdlci1zaGFkZTogI2QzMzkzOTtcbiAgLS1pb24tY29sb3ItZGFuZ2VyLXRpbnQ6ICNmMjU0NTQ7XG4gIC8qKiBkYXJrICoqL1xuICAtLWlvbi1jb2xvci1kYXJrOiAjMjIyNDI4O1xuICAtLWlvbi1jb2xvci1kYXJrLXJnYjogMzQsIDM0LCAzNDtcbiAgLS1pb24tY29sb3ItZGFyay1jb250cmFzdDogI2ZmZmZmZjtcbiAgLS1pb24tY29sb3ItZGFyay1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XG4gIC0taW9uLWNvbG9yLWRhcmstc2hhZGU6ICMxZTIwMjM7XG4gIC0taW9uLWNvbG9yLWRhcmstdGludDogIzM4M2EzZTtcbiAgLyoqIG1lZGl1bSAqKi9cbiAgLS1pb24tY29sb3ItbWVkaXVtOiAjOTg5YWEyO1xuICAtLWlvbi1jb2xvci1tZWRpdW0tcmdiOiAxNTIsIDE1NCwgMTYyO1xuICAtLWlvbi1jb2xvci1tZWRpdW0tY29udHJhc3Q6ICNmZmZmZmY7XG4gIC0taW9uLWNvbG9yLW1lZGl1bS1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XG4gIC0taW9uLWNvbG9yLW1lZGl1bS1zaGFkZTogIzg2ODg4ZjtcbiAgLS1pb24tY29sb3ItbWVkaXVtLXRpbnQ6ICNhMmE0YWI7XG4gIC8qKiBsaWdodCAqKi9cbiAgLS1pb24tY29sb3ItbGlnaHQ6ICNmZmY7XG4gIC0taW9uLWNvbG9yLWxpZ2h0LXJnYjogMjQ0LCAyNDQsIDI0NDtcbiAgLS1pb24tY29sb3ItbGlnaHQtY29udHJhc3Q6ICMwNTEwMjU7XG4gIC0taW9uLWNvbG9yLWxpZ2h0LWNvbnRyYXN0LXJnYjogMCwgMCwgMDtcbiAgLS1pb24tY29sb3ItbGlnaHQtc2hhZGU6ICNkN2Q4ZGE7XG4gIC0taW9uLWNvbG9yLWxpZ2h0LXRpbnQ6ICNmNWY2Zjk7IH1cblxuLyoqXHJcbiAqIEFsbG93cyB5b3UgdG8gdXNlIHJldGluYSBpbWFnZXMgYXQgdmFyaW91cyBwaXhlbCBkZW5zaXRpZXMuXHJcbiAqIEV4YW1wbGVzOlxyXG4gKlxyXG4gKiAgIEBpbmNsdWRlIHJldGluYSgvaW1hZ2VzL215cGljLmpwZywgMik7XHJcbiAqICAgQGluY2x1ZGUgcmV0aW5hKC9pbWFnZXMvbXlwaWMuanBnLCAzLCAxMDBweCAxMDBweCwgbGVmdCB0b3Agbm8tcmVwZWF0IHRyYW5zcGFyZW50KTtcclxuICpcclxuICogQHBhcmFtICB7VmFsdWV9ICAkcGF0aCAgICAgICAgICAgICAgIFRoZSBwYXRoIHRvIHRoZSBmaWxlIG5hbWUgbWludXMgZXh0ZW5zaW9uLlxyXG4gKiBAcGFyYW0gIHtOdW1iZXJ9ICRjYXA6ICAgIDIgICAgICAgICAgVGhlIGhpZ2hlc3QgcGl4ZWwgZGVuc2l0eSBsZXZlbCBpbWFnZXMgZXhpc3QgZm9yLlxyXG4gKiBAcGFyYW0gIHtWYWx1ZX0gICRzaXplOiAgIGF1dG8gYXV0byAgVGhlIGludGVuZGVkIHdpZHRoIG9mIHRoZSByZW5kZXJlZCBpbWFnZS5cclxuICogQHBhcmFtICB7VmFsdWV9ICAkZXh0cmFzOiBudWxsICAgICAgIEFueSBvdGhlciBgYmFja2dyb3VuZGAgdmFsdWVzIHRvIGJlIGFkZGVkLlxyXG4gKi9cbmlvbi1oZWFkZXIgaDEge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogM2VtOyB9XG5cbmlvbi1oZWFkZXIgI2ltZ0xvZ29QbGFuZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tbGVmdDogMTAlO1xuICB3aWR0aDogODAlOyB9XG5cbmlvbi1jb250ZW50IGZvcm0ge1xuICBwYWRkaW5nLWxlZnQ6IDEwdnc7XG4gIHBhZGRpbmctcmlnaHQ6IDEwdnc7IH1cbiAgaW9uLWNvbnRlbnQgZm9ybSAjY2hlY2tib3gtY29sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG1heC13aWR0aDogNDBweDtcbiAgICBtYXJnaW46IDEwcHg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgfVxuICAgIGlvbi1jb250ZW50IGZvcm0gI2NoZWNrYm94LWNvbCBpb24tY2hlY2tib3gge1xuICAgICAgbWFyZ2luOiAwOyB9XG4gIGlvbi1jb250ZW50IGZvcm0gLml0ZW0taW5wdXQge1xuICAgIG1hcmdpbi1ib3R0b206IDAuN2VtOyB9XG4gICAgaW9uLWNvbnRlbnQgZm9ybSAuaXRlbS1pbnB1dCBpb24taW5wdXQge1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgICAgIHBhZGRpbmc6IC4yZW0gLjVlbSAhaW1wb3J0YW50O1xuICAgICAgYm9yZGVyLXJhZGl1czogMWVtOyB9XG4gICAgaW9uLWNvbnRlbnQgZm9ybSAuaXRlbS1pbnB1dCBpb24tbGFiZWwge1xuICAgICAgZm9udC1zaXplOiAxZW07XG4gICAgICBtYXJnaW4tYm90dG9tOiAuNWVtICFpbXBvcnRhbnQ7IH1cbiAgaW9uLWNvbnRlbnQgZm9ybSAucnNhIHtcbiAgICBoZWlnaHQ6IDUwcHg7IH1cbiAgaW9uLWNvbnRlbnQgZm9ybSBidXR0b24ge1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAwLjhlbTtcbiAgICBwYWRkaW5nOiA4cHg7XG4gICAgbWFyZ2luOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7IH1cblxuQG1lZGlhIChtaW4taGVpZ2h0OiA1MDBweCkge1xuICBpb24tY29udGVudCBmb3JtIHtcbiAgICBtYXJnaW4tdG9wOiAxMHZoOyB9IH1cblxuQG1lZGlhIChtaW4taGVpZ2h0OiA2NTBweCkge1xuICBpb24tY29udGVudCBmb3JtIHtcbiAgICBtYXJnaW4tdG9wOiAyMHZoOyB9IH1cbiIsIkBpbXBvcnQgXCIuLi8uLi8uLi90aGVtZS92YXJpYWJsZXMuc2Nzc1wiO1xyXG5cclxuaW9uLWhlYWRlciB7XHJcblxyXG4gIGgxIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogM2VtO1xyXG4gIH1cclxuXHJcbiAgI2ltZ0xvZ29QbGFuZSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gIH1cclxufVxyXG5cclxuaW9uLWNvbnRlbnQge1xyXG5cclxuICBmb3JtIHtcclxuICAgIHBhZGRpbmctbGVmdDogMTB2dztcclxuICAgIHBhZGRpbmctcmlnaHQ6IDEwdnc7XHJcblxyXG4gICAgI2NoZWNrYm94LWNvbCB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIG1heC13aWR0aDogNDBweDtcclxuICAgICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICAgICAgaW9uLWNoZWNrYm94IHtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuaXRlbS1pbnB1dCB7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDAuN2VtO1xyXG5cclxuICAgICAgaW9uLWlucHV0IHtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgIHBhZGRpbmc6IC4yZW0gLjVlbSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDFlbTtcclxuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLjVlbSAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnJzYSB7XHJcbiAgICAgIGhlaWdodDogNTBweDtcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24ge1xyXG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG5cclxuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICAgIHBhZGRpbmc6IDhweDtcclxuICAgICAgbWFyZ2luOiA1cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLWhlaWdodDogNTAwcHgpe1xyXG4gIGlvbi1jb250ZW50IHtcclxuICAgIGZvcm0ge1xyXG4gICAgICBtYXJnaW4tdG9wOiAxMHZoO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4taGVpZ2h0OiA2NTBweCl7XHJcbiAgaW9uLWNvbnRlbnQge1xyXG4gICAgZm9ybSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDIwdmg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/auth/auth.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/auth/auth.page.ts ***!
  \*****************************************/
/*! exports provided: AuthPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthPage", function() { return AuthPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_http_secMobil_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/http/secMobil.service */ "./src/app/service/http/secMobil.service.ts");
/* harmony import */ var _validators_Validators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../validators/Validators */ "./src/app/validators/Validators.ts");








var AuthPage = /** @class */ (function () {
    function AuthPage(formBuilder, navCtrl, zone, secMobilService, translateService, router, loadingController, toastController) {
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.zone = zone;
        this.secMobilService = secMobilService;
        this.translateService = translateService;
        this.router = router;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _validators_Validators__WEBPACK_IMPORTED_MODULE_7__["UsernameValidator"].isValid])],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _validators_Validators__WEBPACK_IMPORTED_MODULE_7__["TokenValidator"].isValid])],
            checked: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].requiredTrue]
        });
    }
    AuthPage.prototype.login = function () {
        var _this = this;
        this.loadingController.create({
            message: 'Connexion en cours...'
        }).then(function (createdLoader) {
            createdLoader.present();
            _this.user = {
                username: _this.loginForm.value.username.toLowerCase(),
                password: _this.loginForm.value.password
            };
            _this.secMobilService.authenticate(_this.user.username, _this.user.password)
                .then(function () {
                createdLoader.dismiss();
                _this.resetUI();
                _this.router.navigate(['/home'], { replaceUrl: true });
            }).catch(function (error) {
                // Screen reset
                createdLoader.dismiss();
                _this.resetUI();
                // Determine the error message
                var errorMsg = '';
                if (error === 'secmobil.incorrect.credentials') {
                    errorMsg = _this.translateService.instant('secmobil.authent.error-incorrect-credentials');
                }
                else if (error === 'secmobil.unknown.error : errSecDefault') {
                    errorMsg = _this.translateService.instant('secmobil.authent.error-contact-server');
                }
                else {
                    errorMsg = _this.translateService.instant('secmobil.authent.error-contact-server');
                }
                // Toast the error message
                _this.toastController.create({
                    message: errorMsg,
                    duration: 3000,
                    position: 'bottom'
                }).then(function (createdToast) {
                    createdToast.present();
                });
            });
        });
    };
    AuthPage.prototype.resetUI = function () {
        this.loginForm.reset();
    };
    AuthPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-auth',
            template: __webpack_require__(/*! ./auth.page.html */ "./src/app/pages/auth/auth.page.html"),
            styles: [__webpack_require__(/*! ./auth.page.scss */ "./src/app/pages/auth/auth.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            _service_http_secMobil_service__WEBPACK_IMPORTED_MODULE_6__["SecMobilService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]])
    ], AuthPage);
    return AuthPage;
}());



/***/ }),

/***/ "./src/app/validators/Validators.ts":
/*!******************************************!*\
  !*** ./src/app/validators/Validators.ts ***!
  \******************************************/
/*! exports provided: UsernameValidator, TokenValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsernameValidator", function() { return UsernameValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenValidator", function() { return TokenValidator; });
var UsernameValidator = /** @class */ (function () {
    function UsernameValidator() {
    }
    UsernameValidator.isValid = function (control) {
        var username = control.value;
        if (!username) {
            return {
                'validator.user_input_mandatory': true
            };
        }
        if (username.length !== UsernameValidator.REQUIRED_USERNAME_LENGTH) {
            return {
                'validator.user_input_length': true
            };
        }
        if (isNaN(username.substring(1, UsernameValidator.REQUIRED_USERNAME_LENGTH)) ||
            !username[0].match(/[a-zA-Z]/i)) {
            return {
                'validator.user_input_format': true
            };
        }
        return null;
    };
    UsernameValidator.REQUIRED_USERNAME_LENGTH = 7;
    return UsernameValidator;
}());

var TokenValidator = /** @class */ (function () {
    function TokenValidator() {
    }
    TokenValidator.isValid = function (control) {
        var token = control.value;
        if (!token) {
            return {
                'validator.password_input_mandatory': true
            };
        }
        if (token.length !== TokenValidator.REQUIRED_TOKEN_LENGTH) {
            return {
                'validator.password_input_length': true
            };
        }
        if (isNaN(token)) {
            return {
                'validator.password_input_length': true
            };
        }
        // null means valid
        return null;
    };
    TokenValidator.REQUIRED_TOKEN_LENGTH = 10;
    return TokenValidator;
}());



/***/ })

}]);
//# sourceMappingURL=pages-auth-auth-module.js.map